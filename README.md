
# Geometric modeling
Study project, in geometric graphic and industrial development (Master 2 GIG), at the University of Luminy, 13005 Marseille.  
This project contains different tools:  
* Mesh Analysis  
* Meshing generation and modelisation  
* And others…

![Preview](http://libs.u4a.at/ModelesGeometriques/preview.png)

## Modern ! 
Written in C++ 14 ( Const-Correctness, Move-Semantics, constexpr, etc... ).
Use Qt and OpenGL 330.

## Documentation
* <http://libs.u4a.at/ModelesGeometriques/html/>
* <https://ametice.univ-amu.fr/mod/wiki/view.php?pageid=4685&group=0>

## Releases
* 32bits : <http://libs.u4a.at/ModelesGeometriques/Release-DW2_32bits-MinGW_720.zip>
* 64bits : <http://libs.u4a.at/ModelesGeometriques/Release-SEH_64bits-MinGW_720.zip>

##  Dependent library :
* <https://qt.io> (Need LGPL binaries License)
* <https://github.com/ocornut/imgui> (Need Header only MIT License)
* <http://www.qcustomplot.com/> (Already included in the project with GPL License)

## Author 
* C++ & WebGL : Walid Birjam [DoubleVV]
* C++ & WebGL : Alexandre D'Ambra [zerdnaxel]
* C++ : Christophe-Alexandre Sonntag [Hip] (http://u4a.at)

## License
Copyright (C) 2018, Luminy GIG Team, All rights reserved.

Distributed under the terms of the  :
GNU GENERAL PUBLIC LICENSE v3 ( GPL v3 )
<https://www.gnu.org/licenses/gpl-3.0.en.html>

## Prerequisites
This project can be builded by two way : 
* Qt + QMake : See "[ModelesGeometriques.pro](https://gitlab.com/luminy-tiptop/modeles_geometriques/blob/master/ModelesGeometriques.pro)" file
* Qt + CMake : See "[CMakeLists.txt](https://gitlab.com/luminy-tiptop/modeles_geometriques/blob/master/CMakeLists.txt)" file, need "QT_DIR" CMake variable, targeting to Qt build directory, to work.

## Modelisation & Analisys
* [Site of the M. Polette supervisor](http://arnaud.polette.perso.luminy.univ-amu.fr/M2GIG.php)
* [Site of the M. Bac supervisor](http://alexandra.bac.perso.luminy.univ-amu.fr/enseignement/styled-16/styled-17/index.html)

### Search about "City generation" : 
https://arrow.dit.ie/cgi/viewcontent.cgi?referer=https://www.google.fr/&httpsredir=1&article=1097&context=itbj
https://josauder.github.io/procedural_city_generation/#the-growth-rules
http://martindevans.me/game-development/2016/05/07/Procedural-Generation-For-Dummies-Footprints/
https://www.reddit.com/r/proceduralgeneration/comments/6kgs8k/procedural_generation_of_city_road_maps/
https://medium.freecodecamp.org/how-to-make-your-own-procedural-dungeon-map-generator-using-the-random-walk-algorithm-e0085c8aa9a

