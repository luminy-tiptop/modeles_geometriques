
/**
 * @author https://rosettacode.org/wiki/Voronoi_diagram
 * @compile gcc -O3 voronoi.cpp -o voronoi.exe -lgdi32 -std=c++11 -lstdc++
 */

#include <windows.h>
#include <vector>
#include <string>
#include <map>

using namespace std;

//////////////////////////////////////////////////////
struct Point
{
  int x, y;
};

//////////////////////////////////////////////////////
class MyBitmap
{
 public:
  MyBitmap() : pen_( nullptr ) {}
  ~MyBitmap()
  {
    DeleteObject( pen_ );
    DeleteDC( hdc_ );
    DeleteObject( bmp_ );
  }

  bool Create( int w, int h )
  {
    BITMAPINFO bi;
    ZeroMemory( &bi, sizeof( bi ) );

    bi.bmiHeader.biSize = sizeof( bi.bmiHeader );
    bi.bmiHeader.biBitCount = sizeof( DWORD ) * 8;
    bi.bmiHeader.biCompression = BI_RGB;
    bi.bmiHeader.biPlanes = 1;
    bi.bmiHeader.biWidth = w;
    bi.bmiHeader.biHeight = -h;

    void* bits_ptr = nullptr;
    HDC dc = GetDC( GetConsoleWindow() );
    bmp_ = CreateDIBSection( dc, &bi, DIB_RGB_COLORS, &bits_ptr, nullptr, 0 );
    if( !bmp_ ) return false;

    hdc_ = CreateCompatibleDC( dc );
    SelectObject( hdc_, bmp_ );
    ReleaseDC( GetConsoleWindow(), dc );

    width_ = w;
    height_ = h;

    return true;
  }

  void SetPenColor( DWORD clr )
  {
    if( pen_ ) DeleteObject( pen_ );
    pen_ = CreatePen( PS_SOLID, 1, clr );
    SelectObject( hdc_, pen_ );
  }

  bool SaveBitmap( const char* path )
  {
    HANDLE file = CreateFile( path, GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr );
    if( file == INVALID_HANDLE_VALUE )
    {
      return false;
    }

    BITMAPFILEHEADER fileheader;
    BITMAPINFO infoheader;
    BITMAP bitmap;
    GetObject( bmp_, sizeof( bitmap ), &bitmap );

    DWORD* dwp_bits = new DWORD[bitmap.bmWidth * bitmap.bmHeight];
    ZeroMemory( dwp_bits, bitmap.bmWidth * bitmap.bmHeight * sizeof( DWORD ) );
    ZeroMemory( &infoheader, sizeof( BITMAPINFO ) );
    ZeroMemory( &fileheader, sizeof( BITMAPFILEHEADER ) );

    infoheader.bmiHeader.biBitCount = sizeof( DWORD ) * 8;
    infoheader.bmiHeader.biCompression = BI_RGB;
    infoheader.bmiHeader.biPlanes = 1;
    infoheader.bmiHeader.biSize = sizeof( infoheader.bmiHeader );
    infoheader.bmiHeader.biHeight = bitmap.bmHeight;
    infoheader.bmiHeader.biWidth = bitmap.bmWidth;
    infoheader.bmiHeader.biSizeImage = bitmap.bmWidth * bitmap.bmHeight * sizeof( DWORD );

    fileheader.bfType = 0x4D42;
    fileheader.bfOffBits = sizeof( infoheader.bmiHeader ) + sizeof( BITMAPFILEHEADER );
    fileheader.bfSize = fileheader.bfOffBits + infoheader.bmiHeader.biSizeImage;

    GetDIBits( hdc_, bmp_, 0, height_, (LPVOID)dwp_bits, &infoheader, DIB_RGB_COLORS );

    DWORD wb;
    WriteFile( file, &fileheader, sizeof( BITMAPFILEHEADER ), &wb, nullptr );
    WriteFile( file, &infoheader.bmiHeader, sizeof( infoheader.bmiHeader ), &wb, nullptr );
    WriteFile( file, dwp_bits, bitmap.bmWidth * bitmap.bmHeight * 4, &wb, nullptr );
    CloseHandle( file );

    delete[] dwp_bits;
    return true;
  }

  HDC hdc() { return hdc_; }
  int width() { return width_; }
  int height() { return height_; }

 private:
  HBITMAP bmp_;
  HDC hdc_;
  HPEN pen_;
  int width_, height_;
};

static int DistanceSqrd( const Point& point, int x, int y )
{
  int xd = x - point.x;
  int yd = y - point.y;
  return ( xd * xd ) + ( yd * yd );
}

//////////////////////////////////////////////////////
class Voronoi
{
 public:
  int width = 0;
  int height = 0;

 public:
  void Make( MyBitmap* bmp, int count )
  {
    width = bmp->width();
    height = bmp->height();
    bmp_ = bmp;
    //
    CreatePoints( count );
    CreateColors();
    CreateSites();
    SetSitesPoints();
  }

 private:
  inline int closest_point_from( int x, int y )
  {
    int ind = -1, dist = INT_MAX;
    for( size_t it = 0; it < points_.size(); it++ )
    {
      const Point& p = points_[it];
      const int d = DistanceSqrd( p, x, y );
      if( d < dist )
      {
        dist = d;
        ind = it;
      }
    }
    return ind;
  }

  inline int closest_point_from_2( int x, int y )
  {
    using it_type = const PointsPlanMapped::const_iterator;

    // const auto static best
    const auto static valid_x_it([ = ]( const it_type& it ) { return it != points_x_mapped_.end(); } );
    const auto static valid_y_it([ = ]( const it_type& it ) { return it != points_y_mapped_.end(); } );

    //
    const it_type x_equal_it( points_x_mapped_.find( x ) );
    const it_type y_equal_it( points_y_mapped_.find( y ) );
    if( valid_x_it( x_equal_it ) && valid_y_it( y_equal_it ) )
      if( x_equal_it->second == y_equal_it->second )
        return x_equal_it->second;

    //
    const it_type x_low_it( points_x_mapped_.lower_bound( x ) );
    const it_type x_up_it( points_x_mapped_.upper_bound( x ) );
    const it_type y_low_it( points_y_mapped_.lower_bound( y ) );
    const it_type y_up_it( points_y_mapped_.upper_bound( y ) );

    //
    if( !valid_x_it( x_low_it ) || !valid_x_it( x_up_it ) || !valid_x_it( y_low_it ) || !valid_x_it( y_up_it ) )
      return -1;

    //
    static const auto get_closest_it(
      []( const int v1, const int v2, const it_type& it1, const it_type& it2 ) {
        return ( std::abs( it1->first - v1 ) <= std::abs( it2->first - v2 ) ) ?
        it1 : it2;
      } );

    // int closest_x( ( std::abs( x_low_it->first - x ) <= std::abs( x_up_it->first - x ) ) ? x_low_it->second : )
    const auto& closest_x_it( get_closest_it( x, x, x_low_it, x_up_it ) );
    const auto& closest_y_it( get_closest_it( y, y, y_low_it, y_up_it ) );

    //
    if( closest_x_it->second == closest_y_it->second )
      return closest_x_it->second;
    else
    {
      const it_type& closest_it( get_closest_it( x, y, closest_x_it, closest_y_it ) );
      return closest_it->second;
    }

  }

 private:
  void CreateSites()
  {
    int w = bmp_->width(), h = bmp_->height();
    for( int hh = 0; hh < h; hh++ ) {
      for( int ww = 0; ww < w; ww++ ) {
        const int ind( closest_point_from( ww, hh ) );
        if( ind > -1 )
          SetPixel( bmp_->hdc(), ww, hh, colors_[ind] );
      }
    }
  }

 private:
  inline void set_point( const Point& p, int size = 3, COLORREF c = 0 )
  {
    const int half( size / 2 );
    Point d;
    for( d.x = ( p.x - half ); d.x < ( p.x + half ); d.x++ )
    {
      for( d.y = ( p.y - half ); d.y < ( p.y + half ); d.y++ )
      {
        if( ( d.x >= 0 ) && ( d.y >=0 ) )
          if( ( d.x < width ) && ( d.y < height ) )
            SetPixel( bmp_->hdc(), d.x, d.y, c );
      }
    }
  }

  void SetSitesPoints()
  {
    for( uint32_t i( 0 ); i < points_.size(); ++i ) {
      const auto& point( points_[i] );
      set_point( point, 5, 0 );
      //set_point( point, 1, colors_[i] );
      set_point( point, 3, colors_[i] );
    }
  }

  void CreatePoints( int count )
  {
    const int w = bmp_->width() - 20, h = bmp_->height() - 20;
    for( int i = 0; i < count; i++ )
    {
      Point p{ rand() % w + 10, rand() % h + 10 };
      points_.push_back( p );
      points_x_mapped_[p.x] = i;
      points_y_mapped_[p.y] = i;
    }
  }

  void CreateColors()
  {
    for( size_t i = 0; i < points_.size(); i++ ) {
      DWORD c = RGB( rand() % 200 + 50, rand() % 200 + 55, rand() % 200 + 50 );
      colors_.push_back( c );
    }
  }

  vector<Point> points_;

  using PointNum = int;
  using PointsPlanMapped = std::map<int, PointNum>;
  PointsPlanMapped points_x_mapped_;
  PointsPlanMapped points_y_mapped_;

  vector<DWORD> colors_;
  MyBitmap* bmp_;
};

//////////////////////////////////////////////////////
int main( int argc, char* argv[] )
{
  ShowWindow( GetConsoleWindow(), SW_MAXIMIZE );
  srand( GetTickCount() );

  uint32_t w( 512 );

  MyBitmap bmp;
  bmp.Create( w, w );
  bmp.SetPenColor( 0 );

  Voronoi v;
  v.Make( &bmp, 10 );

  BitBlt(
    GetDC( GetConsoleWindow() ),
    20, 20, 512, 512,
    bmp.hdc(), 0, 0, SRCCOPY
  );
  bmp.SaveBitmap( "v.bmp" );

  // system("pause");

  return 0;
}

