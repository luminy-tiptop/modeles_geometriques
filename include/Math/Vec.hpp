#pragma once

#include <Math/Number.hpp>

#include <OpenMesh/Core/Geometry/Vector11T.hh>

/**
 * @group Math
 * @{
 */

namespace Math {

  /**
   * @group Vec
   * @{
   */

  template<typename T> using Vec2 = OpenMesh::VectorT<T, 2>;
  template<typename T> using Vec3 = OpenMesh::VectorT<T, 3>;

  using Vec2f =  Vec2<float>;
  using Vec2d =  Vec2<double>;
  using Vec2ui = Vec2<uint>;

  using Vec3f =  Vec3<float>;
  using Vec3d =  Vec3<double>;
  using Vec3ui = Vec3<uint>;

  using Vec2CN = Vec2<CN>;
  using Vec2DN = Vec2<DN>;
  using Vec3CN = Vec3<CN>;
  using Vec3DN = Vec3<DN>;

  using Point2D = Vec3CN;
  using Point3D = Vec3CN;

  /**
   * @}
   * @group Vec operation
   * @{
   */


  template<typename T>
  inline T length( const Vec2<T>& p )
  { return std::sqrt( ( p[0] * p[0] ) + ( p[1] * p[1] ) ); }

  template<typename T>
  inline T length( const Vec3<T>& p )
  { return std::sqrt( ( p[0] * p[0] ) + ( p[1] * p[1] ) + ( p[2] * p[2] ) ); }

  template<typename T>
  inline T distance( const Vec2<T>& p1, const Vec2<T>& p2 )
  {
    const T x( p2[0] - p1[0] ), y( p2[1] - p1[1] );
    return std::sqrt( ( x * x ) + ( y * y ) );
  }

  template<typename T>
  inline T distance( const Vec3<T>& p1, const Vec3<T>& p2 )
  {
    const T x( p2[0] - p1[0] ), y( p2[1] - p1[1] ), z( p2[2] - p1[2] );
    return std::sqrt( ( x * x ) + ( y * y ) + ( z * z ) );
  }

  /**
   * @brief crossProduct : produit vectoriel
   */
  template<typename T>
  inline Vec3<T> crossProduct( const Vec3<T>& p1, const Vec3<T>& p2 )
  {
    return Vec3<T>(
      p1[1] * p2[2] - p1[2] * p2[1],
      p1[0] * p2[2] - p1[2] * p2[0],
      p1[0] * p2[1] - p1[1] * p2[0]
    );
  }

  /**
   * @brief dotProduct : produit scalaire
   */
  template<typename T>
  inline T dotProduct( const Vec3<T>& p1, const Vec3<T>& p2 )
  { return ( p1[0] * p2[0] ) + ( p1[1] * p2[1] ) + ( p1[2] * p2[2] ); }

  template<typename T>
  inline T triangleArea( const Vec3<T>& p1, const Vec3<T>& p2, const Vec3<T>& p3 )
  {
    const T p2VecFromP1( distance( p2, p1 ) );
    const T p3VecFromP1( distance( p3, p1 ) );
    return ( p2VecFromP1 * p3VecFromP1 ) / 2.; // like half square
  }

  /**
   * @see https://calculis.net/triangle
   */
  template<typename T>
  inline T triangleAngle( const Vec3<T>& pFrom, const Vec3<T>& pSide1, const Vec3<T>& pSide2 )
  {
    const T side1( distance( pSide1, pFrom ) );
    const T side2( distance( pSide2, pFrom ) );
    const T oppositeSide( distance( pSide1, pSide2 ) );
    return std::acos( ( side1 * side1 + side2 * side2 - oppositeSide * oppositeSide ) / ( 2.f * side1 * side2 ) );
  }

  /**
   * @see https://stackoverflow.com/a/34069505
   */
  template<typename T>
  inline float triangleAngle2( const Vec3<T>& pFrom, const Vec3<T>& pSide1, const Vec3<T>& pSide2 )
  {
    const Vec3<T> side1( pSide1 - pFrom );
    const Vec3<T> side2( pSide2 - pFrom );
    const T side1Vec( std::sqrt( side1[0] * side1[0] + side1[1] * side1[1] + side1[2] * side1[2] ) );
    const T side2Vec( std::sqrt( side2[0] * side2[0] + side2[1] * side2[1] + side2[2] * side2[2] ) );
    const Vec3<T> side1Norm( side1 / side1Vec );
    const Vec3<T> side2Norm( side2 / side2Vec );
    return std::acos( dotProduct( side1Norm, side2Norm ) );
  }

  /** @todo https://math.stackexchange.com/a/128999 */
  /** @todo https://math.stackexchange.com/a/1483219 */

  /** }@ */

}

/** }@ */

