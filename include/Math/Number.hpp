#pragma once

#include <cmath>
#include <limits>

/**
 * @group Math
 * @{
 */
namespace Math {

  /**
   * @group ContinuousNumber
   * @{
   */

  using ContinuousNumber = double;
  using CN = ContinuousNumber;
  using CTN = ContinuousNumber;
  using DUR = ContinuousNumber;

  /**
   * @}
   * @group DiscreteNumber
   * @{
   */

  using DiscreteNumber = long int;
  using DN = long int;

  /**
   * @}
   * @group Limits comparaison
   * @{
   */

  template<typename T>
  inline bool equal( const T& lhs, const T& rhs )
  {
    static constexpr T epsilon( std::numeric_limits<T>::epsilon() );
    return std::fabs( lhs - rhs ) <= epsilon;
  }

  template<typename T>
  inline bool not_equal( const T& lhs, const T& rhs )
  {
    static constexpr T epsilon( std::numeric_limits<T>::epsilon() );
    return std::fabs( lhs - rhs ) > epsilon;
  }

  /** }@  */

}

  /** }@  */
