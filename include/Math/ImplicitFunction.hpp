#pragma once

#include <Math/Math.hpp>

/**
 * @group Math
 * @{
 */
namespace Math {
  /**
   * @group ImplicitFunction
   * @{
   */
  namespace ImplicitFunction {

    /** implicit surface function */
    typedef CN (* func_pf_t)( const Point3D& );

    // ---- ----

    /**
     * @group BasicFonctions
     * @{
     */

    /** torus: a torus with major, minor radii = 0.5, 0.1, try size = .05 */
    inline CN torus( const Point3D& p )
    {
      const CN x2( p[0] * p[0] ), y2( p[1] * p[1] ), z2( p[2] * p[2] );
      const CN a( x2 + y2 + z2 + CN( 0.5 * 0.5 ) - CN( 0.1 * 0.1 ) );
      return ( a * a ) - CN( 4. ) * CN( 0.5 * 0.5 ) * ( y2 + z2 );
    }

    /** sphere */
    inline CN sphere( const Point3D& p )
    {
      static constexpr Point3D C( 0., 0., 0. );
      CN r1( 1. );
      return ( p - C ).sqrnorm() - r1 * r1;
    }

    /** positiveSphere: an inverse square function (always positive) */
    inline CN positiveSphere( const Point3D& p )
    {
      const CN rsq( p[0] * p[0] + p[1] * p[1] + p[2] * p[2] );
      return CN( 1.0 ) / ( ( rsq < CN( 0.00001 ) ) ? CN( 0.00001 ) : rsq );
    }

    /** blob: a three-pole blend function, try size = .1 */
    inline CN blob( const Point3D& p )
    {
      return CN( 4.0 )
             - positiveSphere( { p[0] + CN( 1.0 ), p[1], p[2] } )
             - positiveSphere( { p[0], p[1] + CN( 1.0 ), p[2] } )
             - positiveSphere( { p[0], p[1], p[2] + CN( 1.0 ) } );
    }

    /** wiffle_cube: with a = 1/2.3 and b = 1/2. */
    inline CN wiffle_cube( const Point3D& p )
    {
      const CN a( 1. / 2.3 ), b( 1. / 2. );
      return 1.
             - std::pow( ( a * a ) * ( p[0] * p[0] ) + ( a * a ) * ( p[1] * p[1] ) + ( a * a ) * ( p[2] * p[2] ) , -6 )
             - std::pow( std::pow( b, 8 ) * std::pow( p[0], 8 ) + std::pow( b, 8 ) * std::pow( p[1], 8 ) + std::pow( b, 8 ) * std::pow( p[2], 8 ), 6 );
    }

    /** }@ */

    // ---- ----

  }
  /** }@ */
}
/** }@ */
