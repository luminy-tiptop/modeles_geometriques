#pragma once

#include <ShaderProgram/AbstractShaderProgram.hpp>
#include <Mesh.hpp>
#include <QOpenGLBuffer>
#include <QVector3D>
#include <bits/move.h>

namespace ShaderProgram {

  struct VertexColorPack : public AbstractShaderProgram
  {
   public:
    struct Format
    {
      QVector3D vertex;
      QVector3D color;

      inline Format( QVector3D && _vertex, QVector3D && _color ) : vertex( std::move( _vertex ) ), color( std::move( _color ) ) {}
      inline Format( QVector3D _vertex, QVector3D _color ) : vertex( std::move( _vertex ) ), color( std::move( _color ) ) {}

      inline Format( const OpenMesh::Vec3f & openmesh_vertex, const Mesh::Color & openmesh_color ) :
        vertex( openmesh_vertex[0], openmesh_vertex[1], openmesh_vertex[2] ),
        color( float( openmesh_color[0] ) / 255.f, float( openmesh_color[1] ) / 255.f, float( openmesh_color[2] ) / 255.f ) {}
    };

   public:
    QOpenGLBuffer vertexColor;

   public:
    virtual ~VertexColorPack() override = default;

   public:
    virtual inline void initialise( QOpenGLFunctions_3_3_Core & gl ) override {
      buildProgram(
        ":/glsl/in_vec3_color__out_color.vert",
        ":/glsl/in_color__out_color.frag"
      );
      vertexColor.create();
      vao.create();

      vao.bind();
      gl.glEnableVertexAttribArray( 0 );
      gl.glEnableVertexAttribArray( 1 );
      vertexColor.bind();

      const int buffer_stride( sizeof( Format ) );
      const void * const vertex_offset( reinterpret_cast<void *>( offsetof( Format, vertex ) ) );
      const void * const color_offset( reinterpret_cast<void *>( offsetof( Format, color ) ) );

      // glsl : layout( location = 0 ) in vec3 vertex_from_buffer:
      gl.glVertexAttribPointer( 0, 3, GL_FLOAT, false, buffer_stride, vertex_offset );
      // glsl : layout( location = 1 ) in vec3 color_from_buffer:
      gl.glVertexAttribPointer( 1, 3, GL_FLOAT, false, buffer_stride, color_offset );

      vao.release();
      // After vao release :
      vertexColor.release();
      gl.glDisableVertexAttribArray( 0 );
      gl.glDisableVertexAttribArray( 1 );
    }

   public:
    inline void send( const std::vector<Format> & vec ) {
      count = GLuint( vec.size() );
      const GLuint vertex_sizeof( count * sizeof( Format ) );
      vertexColor.bind();
      vertexColor.allocate( vec.data(), int( vertex_sizeof ) );
      vertexColor.release();
    }


  };


}
