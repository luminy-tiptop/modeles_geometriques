#pragma once

#include <ShaderProgram/AbstractShaderProgram.hpp>
#include <Math/Math.hpp>

#include <QOpenGLBuffer>
#include <OpenMesh/Core/Geometry/Vector11T.hh>

#include <Utils/GLTypes.hpp>

#include <vector>

namespace ShaderProgram {

  struct IndexedVertexValueUv : public AbstractShaderProgram
  {
   public:
    struct VertexFormat
    {
      Math::Point3D position;
      Math::Point3D normal;

      inline VertexFormat( Math::Point3D _position, Math::Point3D _normal ) :
        position( std::move( _position ) ),
        normal( std::move( _normal ) )
      {}

      inline VertexFormat( const QVector3D& _position, const QVector3D& _normal ) :
        position( _position[0], _position[1], _position[2] ),
        normal( _normal[0], _normal[1], _normal[2] )
      {}
    };

    struct TriangleIndicesFormat
    {
     public:
      uint i = 0;
      uint j = 0;
      uint k = 0;

     public:
      TriangleIndicesFormat() = default;
      inline TriangleIndicesFormat( uint _i, uint _j, uint _k ) :
        i( std::move( _i ) ), j( std::move( _j ) ), k( std::move( _k ) )
      {}
    };

   public:
    QOpenGLBuffer vertex{ QOpenGLBuffer::Type::VertexBuffer };
    QOpenGLBuffer indices{ QOpenGLBuffer::Type::IndexBuffer };
    uint indicesCount = 0;
    uint triangleCount = 0;

   public:
    virtual ~IndexedVertexValueUv() override = default;

   public:
    virtual inline void initialise( QOpenGLFunctions_3_3_Core& gl ) override
    {
      buildProgram(
        ":/glsl/in_vec3_norm__out_vec3_norm.vert",
        ":/glsl/in_vec3_norm__out_color.frag"
      );
      vertex.create();
      vertex.setUsagePattern( QOpenGLBuffer::UsagePattern::DynamicDraw );
      indices.create();
      indices.setUsagePattern( QOpenGLBuffer::UsagePattern::DynamicDraw );

      vao.create();

      vao.bind();
      gl.glEnableVertexAttribArray( 0 );
      gl.glEnableVertexAttribArray( 1 );
      vertex.bind();

      //
      const GLuint formatType( Utils::GLTypes::getTypeForGL<Math::ContinuousNumber>() );
      const int buffer_stride( sizeof( VertexFormat ) );
      const void* const position_offset( reinterpret_cast<void*>( offsetof( VertexFormat, position ) ) );
      const void* const normal_offset( reinterpret_cast<void*>( offsetof( VertexFormat, normal ) ) );

      // glsl : layout( location = 0 ) in vec3 vertex_from_buffer :
      gl.glVertexAttribPointer( 0,  3, formatType, false, buffer_stride, position_offset );

      // glsl : layout( location = 1 ) in vec3 normal_from_buffer:
      gl.glVertexAttribPointer( 1, 3, formatType, false, buffer_stride, normal_offset );

      indices.bind();

      vao.release();
      // After vao release :
      vertex.release();
      gl.glDisableVertexAttribArray( 0 );
      gl.glDisableVertexAttribArray( 1 );
    }

   protected:
    template<typename TVec>
    inline void internalSend( const void* data, const size_t size )
    {
      count = GLuint( size );
      const GLuint vertex_sizeof( count * sizeof( TVec ) );
      vertex.bind();
      vertex.allocate( data, int(vertex_sizeof) );
      vertex.release();
    }

   public:
    inline void sendVertices( const std::vector<VertexFormat>& vec )
    {
      count = GLuint( vec.size() );
      const GLuint vertex_sizeof( count * sizeof( VertexFormat ) );
      vertex.bind();
      vertex.allocate( vec.data(), int(vertex_sizeof) );
      vertex.release();
    }

   public:
    inline void sendTriangles( const std::vector<TriangleIndicesFormat>& ids )
    {
      triangleCount = uint( ids.size() );
      indicesCount = triangleCount * 3;
      const GLuint indices_sizeof( triangleCount * sizeof( TriangleIndicesFormat ) );
      indices.bind();
      indices.allocate( ids.data(), int(indices_sizeof) );
      indices.release();
    }

  };


}
