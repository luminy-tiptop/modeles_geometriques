#pragma once

#include <ShaderProgram/AbstractShaderProgram.hpp>
#include <Math/Vec.hpp>

#include <QOpenGLBuffer>
#include <vector>

namespace ShaderProgram {

  struct Vertex : public AbstractShaderProgram
  {
   public:
    QOpenGLBuffer vertex;

   public:
    virtual ~Vertex() override = default;

   public:
    virtual inline void initialise( QOpenGLFunctions_3_3_Core& gl ) override
    {
      buildProgram(
        ":/glsl/in_vec3.vert",
        ":/glsl/uniform_color__out_color.frag"
      );
      vertex.create();
      vao.create();

      vao.bind();
      gl.glEnableVertexAttribArray( 0 );
      vertex.bind();

      // glsl : layout( location = 0 ) in vec3 vertex_from_buffer :
      gl.glVertexAttribPointer( 0, 3, GL_FLOAT, false, 0, nullptr );

      vao.release();
      // After vao release :
      vertex.release();
      gl.glDisableVertexAttribArray( 0 );
    }

   protected:
    template<typename TVec>
    inline void internalSend( const void* data, const size_t size )
    {
      count = GLuint( size );
      const GLuint vertex_sizeof( count * sizeof( TVec ) );
      vertex.bind();
      vertex.allocate( data, int(vertex_sizeof) );
      vertex.release();
    }

   public:
    inline void send( const std::vector<QVector3D>& vec )
    {  internalSend<QVector3D>( vec.data(), vec.size() ); }

    inline void send( const std::vector<Math::Vec3f>& vec )
    {  internalSend<Math::Vec3f>( vec.data(), vec.size() ); }

    inline void send( const Math::Vec3f* vec, size_t nb )
    {  internalSend<Math::Vec3f>( vec, nb ); }

   public:
    void paint( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp, const QVector3D& color = { 1.f, 0., 0.f }, bool wireframe = false )
    {
      bind();
      program.setUniformValue( "uniform_mvp", mvp );
      program.setUniformValue( "uniform_color", color );
      program.setUniformValue( "uniform_mode_wireframe", int(wireframe) );
      gl.glDrawArrays( GL_LINES, 0, GLint( count ) );
      release();
    }

  };


}
