#pragma once

#include <ShaderProgram/AbstractShaderProgram.hpp>
#include <Mesh.hpp>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QVector3D>
#include <QVector2D>
#include <QImage>
#include <memory>

namespace ShaderProgram {

  struct VertexUvPack : public AbstractShaderProgram
  {
   public:
    struct Format
    {
      QVector3D vertex;
      QVector2D uv;

      inline Format( QVector3D&& _vertex, QVector2D&& _uv ) : vertex( std::move( _vertex ) ), uv( std::move( _uv ) ) {}
      inline Format( QVector3D _vertex, QVector2D _uv ) : vertex( std::move( _vertex ) ), uv( std::move( _uv ) ) {}

      inline Format( const OpenMesh::Vec3f& openmesh_vertex, const OpenMesh::Vec2f& openmesh_uv ) :
        vertex( openmesh_vertex[0], openmesh_vertex[1], openmesh_vertex[2] ),
        uv( openmesh_uv[0], openmesh_uv[1] ) {}
    };

   public:
    QOpenGLBuffer vertexUv;
    std::unique_ptr<QOpenGLTexture> texture_up;

   public:
    virtual ~VertexUvPack() override = default;

   public:
    inline virtual void bind() override
    {
      AbstractShaderProgram::bind();
      if( texture_up )
      {
        texture_up->bind( 0 );
        program.setUniformValue( "uniform_sampler2d_texture", 0 );
      }
    }
    inline virtual void release() override
    {
      if( texture_up )
        texture_up->release();
      AbstractShaderProgram::release();
    }

   public:
    virtual inline void initialise( QOpenGLFunctions_3_3_Core& gl ) override
    {
      buildProgram(
        ":/glsl/in_vec3_uv__out_uv.vert",
        ":/glsl/in_uv__out_color.frag"
      );

      vertexUv.create();
      vao.create();

      vao.bind();
      gl.glEnableVertexAttribArray( 0 );
      gl.glEnableVertexAttribArray( 1 );
      vertexUv.bind();

      const int buffer_stride( sizeof( Format ) );
      const void* const vertex_offset( reinterpret_cast<void*>( offsetof( Format, vertex ) ) );
      const void* const uv_offset( reinterpret_cast<void*>( offsetof( Format, uv ) ) );

      // glsl : layout( location = 0 ) in vec3 vertex_from_buffer:
      gl.glVertexAttribPointer( 0, 3, GL_FLOAT, false, buffer_stride, vertex_offset );
      // glsl : layout( location = 1 ) in vec2 uv_from_buffer:
      gl.glVertexAttribPointer( 1, 2, GL_FLOAT, false, buffer_stride, uv_offset );

      vao.release();
      // After vao release :
      vertexUv.release();
      gl.glDisableVertexAttribArray( 0 );
      gl.glDisableVertexAttribArray( 1 );
    }

   public:
    inline void send( const std::vector<Format>& vec, bool resetTexture = true )
    {
      // Buffer :
      count = GLuint( vec.size() );
      const GLuint vertex_uv_sizeof( count * sizeof( Format ) );
      vertexUv.bind();
      vertexUv.allocate( vec.data(), int(vertex_uv_sizeof) );
      vertexUv.release();

      // Texture :
      if( resetTexture )
        texture_up.reset();
    }

   public:
    inline void sendTexture( QImage& image )
    {
      texture_up =
        std::make_unique<QOpenGLTexture>(
          image.mirrored()
        );
      texture_up->setMinificationFilter( QOpenGLTexture::Nearest );
      texture_up->setMagnificationFilter( QOpenGLTexture::Linear );
      texture_up->setWrapMode( QOpenGLTexture::MirroredRepeat );
    }


  };


}
