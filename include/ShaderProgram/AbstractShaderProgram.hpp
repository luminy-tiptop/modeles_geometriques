#pragma once

#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>

#include <QOpenGLFunctions_3_3_Core>

namespace ShaderProgram {

  struct AbstractShaderProgram
  {
   public:
    QOpenGLShaderProgram program;
    QOpenGLVertexArrayObject vao;
    uint count = 0;

   public:
    virtual ~AbstractShaderProgram() = default;

   protected:
    void buildProgram( const QString & vert_path, const QString & frag_path );

   public:
    virtual void bind();
    virtual void release();

   public:
    virtual void initialise( QOpenGLFunctions_3_3_Core & gl ) = 0;
    //virtual void paint() { /** @todo pass it to abstract */ };

  };


}
