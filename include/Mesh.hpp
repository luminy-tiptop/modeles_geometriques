#pragma once

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <vector>

struct MeshTraits : public OpenMesh::DefaultTraits
{
  VertexAttributes( OpenMesh::Attributes::TexCoord2D );
  HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
};

struct Mesh : public OpenMesh::TriMesh_ArrayKernelT<MeshTraits>
{
 //public:
 // const Math::Vec3f* vertices_data() const { return points(); }
 // size_t vertices_count() const { return n_vertices(); }

 public:
  using TriMesh_ArrayKernelT::TriMesh_ArrayKernelT;   // Constructor Inheritance

 public:
  inline void add_face_quad( VertexHandle _vh0, VertexHandle _vh1, VertexHandle _vh2, VertexHandle _vh3 )
  {
    add_face( _vh0, _vh1, _vh2 );
    add_face( _vh2, _vh3, _vh0 );
  }

};

// ---- ---- ---- ----
//
// struct VectorMesh
// {
// public:
//   std::vector<> vertices;
//
// public:
//   VectorMesh();
// };
