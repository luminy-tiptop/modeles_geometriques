#pragma once

#include <QVector2D>
#include <QVector3D>
#include <QMatrix4x4>
#include <QPoint>

#include <OpenMesh/Core/Geometry/VectorT.hh>

#include <iostream>
#include <iomanip>
#include <sstream>

#define USE_LOGGER 1

#define OUTPUT_VAR( var ) " " #var "(" << var << ") "
#define OV( var ) OUTPUT_VAR( var )

#define debug_cout std::cout


#if USE_LOGGER==1
extern std::stringstream logger_stream;
#define logger logger_stream
#else
#define logger std::cout
#endif


inline std::ostream& operator <<( std::ostream& os, const QVector2D& v2 )
{
  os << std::setprecision( 2 );
  os << std::setw( 5 ) << v2.x() << "," << std::setw( 5 ) << v2.y();
  return os;
}

inline std::ostream& operator <<( std::ostream& os, const QSize& size )
{
  os << std::setprecision( 2 );
  os << std::setw( 5 ) << size.width() << "," << std::setw( 5 ) << size.height();
  return os;
}

inline std::ostream& operator <<( std::ostream& os, const QVector3D& v3 )
{
  os << std::setprecision( 2 );
  os << std::setw( 5 ) << v3.x() << "," << std::setw( 5 ) << v3.y() << "," << std::setw( 5 ) << v3.z();
  return os;
}

inline std::ostream& operator <<( std::ostream& os, const QPoint& p )
{
  os << std::setprecision( 2 );
  os << std::setw( 5 ) << p.x() << "," << std::setw( 5 ) << p.y();
  return os;
}

//inline std::ostream& operator <<( std::ostream& os, const QVector2D& v )
//{ os << v.x() << "," << v.y() << ")" << std::endl; return os; }

//inline std::ostream& operator <<( std::ostream& os, const QVector3D& v )
//{ os << v.x() << "," << v.y() << "," << v.z() << ")" << std::endl; return os; }

inline std::ostream& operator <<( std::ostream& os, const QQuaternion& q )
{ os << q.x() << "," << q.y() << "," << q.z() << "," << q.scalar() << std::endl; return os; }

inline std::ostream& operator <<( std::ostream& os, const QMatrix4x4& m )
{
  os << " [" << std::endl
     << "  [" << m( 0, 0 ) << "," << m( 0, 1 ) << "," << m( 0, 2 ) << "," << m( 0, 3 ) << "]" << std::endl
     << "  [" << m( 1, 0 ) << "," << m( 1, 1 ) << "," << m( 1, 2 ) << "," << m( 1, 3 ) << "]" << std::endl
     << "  [" << m( 2, 0 ) << "," << m( 2, 1 ) << "," << m( 2, 2 ) << "," << m( 2, 3 ) << "]" << std::endl
     << "  [" << m( 3, 0 ) << "," << m( 3, 1 ) << "," << m( 3, 2 ) << "," << m( 3, 3 ) << "]" << std::endl
     << " ]" << std::endl;
  return os;
}

template<typename T>
inline std::ostream& operator <<( std::ostream& os, const OpenMesh::VectorT<T, 2>& v )
{ os << v[0] << ", " << v[1]; return os; }

template<typename T>
inline std::ostream& operator <<( std::ostream& os, const OpenMesh::VectorT<T, 3>& v )
{ os << v[0] << ", " << v[1] << ", " << v[2]; return os; }

//template<typename T>
//inline std::ostream& operator <<( std::ostream& os, const OpenMesh::VectorT<T, 2>& v )
//{ os << v[0] << "," << v[1] << ")"; return os; }

//template<typename T>
//inline std::ostream& operator <<( std::ostream& os, const OpenMesh::VectorT<T, 3>& v )
//{ os << v[0] << "," << v[1] << "," << v[2] << ")" << std::endl; return os; }



