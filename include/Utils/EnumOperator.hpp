#pragma once

#define Utils_EnumOperator(enum_name,enum_type) \
  inline enum_name operator|( enum_name a, enum_name b ) {return enum_name( enum_type( a ) | enum_type( b ) );}        \
  inline enum_name operator&( enum_name a, enum_name b ) {return enum_name( enum_type( a ) & enum_type( b ) );}        \
  inline enum_name operator-( enum_name a, enum_name b ) {return enum_name( enum_type( a ) & ( ~enum_type( b ) ) );}   \
  inline enum_name & operator|=( enum_name & a, enum_name b ) {return ( a = a | b );}                                  \
  inline enum_name & operator&=( enum_name & a, enum_name b ) {return ( a = a & b );}                                  \
  inline enum_name & operator-=( enum_name & a, enum_name b ) {return ( a = a - b );}                                  \
  inline bool operator==( enum_name a, enum_name b ) { return enum_type( a ) == enum_type( b ); }                      \
  inline bool operator!=( enum_name a, enum_name b ) { return enum_type( a ) != enum_type( b ); }                      \
  inline bool operator!( enum_name a ) {return enum_type( a ) == 0;}                                                   \
  inline bool any( enum_name a ) { return enum_type( a ) > 0; }

