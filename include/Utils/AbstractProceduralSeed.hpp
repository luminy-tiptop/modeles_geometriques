#pragma once

#include <Math/Vec.hpp>

#include <types.hpp>

#include <random>
#include <chrono>

namespace Utils {

  class AbstractProceduralSeed
  {
   public:
    using Clock = std::chrono::system_clock;
    using Generator = std::ranlux48_base;
    using SeedType = Generator::result_type;


   public:
    static std::uniform_real_distribution<float> positiveNormalValueDistribution;
    static std::uniform_real_distribution<float> normalPositionDistribution;

   public:
    static inline Clock::duration::rep now() { return Clock::now().time_since_epoch().count(); }


   protected:
    Generator mGenerator;

   public:
    inline AbstractProceduralSeed() : mGenerator( SeedType( now() ) ) {  }
    inline AbstractProceduralSeed( SeedType _v ) : mGenerator( std::move( _v ) ) {}
    virtual ~AbstractProceduralSeed() = default;

   public:
    inline float randPositiveNormalValue( const float coef = 1.f  )
    { return positiveNormalValueDistribution( mGenerator ) * coef; }

    inline Math::Vec3f randNormalPosition( const float coef = 1.f  )
    { return coef * Math::Vec3f( normalPositionDistribution( mGenerator ), normalPositionDistribution( mGenerator ), normalPositionDistribution( mGenerator ) ); }

    inline uint randPositiveValue( const uint min = 0, const uint max = 100 )
    { return std::uniform_int_distribution<uint>( min, max ) ( mGenerator ); }

    inline int randValue( const int min = 0, const int max = 100 )
    { return std::uniform_int_distribution<int>( min, max ) ( mGenerator ); }

    inline float randValue( const float min = 0.f, const float max = 100.f )
    { return std::uniform_real_distribution<float>( min, max ) ( mGenerator ); }

   public:
    template<typename TEnum>
    inline TEnum randEnum( const int min = 0, const int max = 100 ) { return TEnum( randValue( min, max ) ); }

    template<typename TEnum>
    inline TEnum randEnum( const TEnum min, const TEnum max ) { return TEnum( randValue( int(min), int(max) ) ); }

   public:
    inline void changeSeed()
    { mGenerator = Generator( SeedType( now() ) ); }

    inline void setSeed( SeedType v )
    { mGenerator = Generator( v ); }

   public:
    virtual void refresh() = 0;

  };


}
