#pragma once

#include <QOpenGLFunctions_3_3_Core>

namespace Utils {
  namespace GLTypes {

    template<typename T> inline GLuint getTypeForGL() { throw std::runtime_error( "Unknown type" ); }
    template<> inline GLuint getTypeForGL<double>() { return GL_DOUBLE; }
    template<> inline GLuint getTypeForGL<float>()  { return GL_FLOAT; }

  }
}
