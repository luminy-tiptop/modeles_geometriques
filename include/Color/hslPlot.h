#pragma once

#include <OpenMesh/Core/Geometry/Vector11T.hh>

#include <vector>
#include <cmath>

using uint = unsigned int;
using uchar = unsigned char;

namespace Color {


  /**
   * @author adaptation from https://www.programmingalgorithms.com/algorithm/hsl-to-rgb?lang=C%2B%2B
   */
  inline float hslHue2Rgb( float v1, float v2, float vH )
  {
    if ( vH < 0.f )           vH += 1.f;
    if ( vH > 1.f )           vH -= 1.f;
    if ( ( 6.f * vH ) < 1.f ) return ( v1 + ( v2 - v1 ) * 6.f * vH );
    if ( ( 2.f * vH ) < 1.f ) return v2;
    if ( ( 3.f * vH ) < 2.f ) return ( v1 + ( v2 - v1 ) * ( ( 2.f / 3.f ) - vH ) * 6.f );
    return v1;
  }

  template<typename TColor>
  inline TColor hslPlotColorFormat( float r, float g, float b )
  { return  TColor( r, g, b );}

  template<>
  inline OpenMesh::Vec3uc hslPlotColorFormat<OpenMesh::Vec3uc>( float r, float g, float b )
  { return  OpenMesh::Vec3uc( uchar( 255.f * r ), uchar( 255.f * g ), uchar( 255.f * b ) );}

  /**
   * @see adaptation from https://www.programmingalgorithms.com/algorithm/hsl-to-rgb?lang=C%2B%2B
   */
  template<typename TColor>
  inline std::vector<TColor> hslPlot( uint size, float hsl_over = 9.999f, float hsl_start = 0.f, float hsl_end = 360.f )
  {
    static const float hsl_saturation( 1.f ), hsl_lightness( 0.7f );

    //
    std::vector<TColor> plot( size );
    const float hue_padding( ( hsl_end - hsl_start ) / float( size ) );

    //
    for ( uint i( 0 ); i < size; ++i )
    {
      float hue( std::fmod( ( float( i ) * hue_padding * hsl_over ) / 360.f, 1.f ) );

      const float v2(
        ( hsl_lightness < 0.5f ) ?
        ( hsl_lightness * ( 1.f + hsl_saturation ) ) :
        ( ( hsl_lightness + hsl_saturation ) - ( hsl_lightness * hsl_saturation ) )
      );
      const float v1( 2.f * hsl_lightness - v2 );
      plot[i] = hslPlotColorFormat<TColor>(
                  hslHue2Rgb( v1, v2, hue + ( 1.0f / 3.f ) ),  // R
                  hslHue2Rgb( v1, v2, hue ),                   // G
                  hslHue2Rgb( v1, v2, hue - ( 1.0f / 3.f ) )   // B
                );
    }
    return plot;
  }

}
