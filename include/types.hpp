#pragma once

#include <cstdint>

using uchar = unsigned char;
using ushort = unsigned short;
using uint = unsigned int;
