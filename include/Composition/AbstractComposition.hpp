#pragma once

#include <Mesh.hpp>

#include <QString>
#include <QWidget>
#include <QMenu>

#include <QMatrix4x4>

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>

#include <Composition/CompositionReceiver.hpp>

#include <memory>

namespace Composition {

  class AbstractComposition
  {
   public:
    const std::string name;

   protected:
    //Gui::MeshViewerWidget & glWidget;
    Composition::CompositionReceiver& mReceiver;

   public:
    //inline AbstractComposition( std::string _name, Gui::MeshViewerWidget & _glWidget );
    inline AbstractComposition( std::string _name, Composition::CompositionReceiver& receiver ) :
      name( std::move( _name ) ), mReceiver( receiver ) { }

   public:
    virtual ~AbstractComposition() = default;

   public:
    inline virtual QWidget* getAttachedWidget() { return nullptr; /* default */ }
    //inline virtual QWidget * getAttachedSubWidget() { return nullptr; /* default */ }
    inline virtual QMenu* getAttachedMenu() { return nullptr; /* default */ }

   public:
    inline virtual Mesh* getAttachedOpenMesh() { return nullptr; /* default */ }

   public:
    virtual void initialiseGL( QOpenGLFunctions_3_3_Core& gl ) = 0;
    virtual void paintGL( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp ) = 0;

   public:
    virtual bool isInitializedGL() const = 0;

   protected:
    bool mDoPaint = true;

   public:
    virtual bool doPaint() const { return mDoPaint; }
    virtual void setPaint( bool v ) { mDoPaint = v; }

   public:
    inline void throwIfNotInitializedGL() const
    {
      if( !isInitializedGL() )
        throw std::runtime_error( "[AbstractComposition][" + name + "] Is not initialized" );
    }
  };

  using Composition_up = std::unique_ptr<AbstractComposition>;

}

