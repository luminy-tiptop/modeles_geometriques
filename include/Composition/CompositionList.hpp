#pragma once

#include <Composition/AbstractComposition.hpp>

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>

#include <list>
#include <memory>

namespace Gui { class Panel_CompositionManager; }

namespace Composition {

  using Composition_up_t = std::unique_ptr<AbstractComposition>;
  using Compositions_t = std::list<Composition_up_t>;

  class CompositionList : public Compositions_t
  {
   protected:
    static std::unique_ptr<CompositionList> mSingleton;

   public:
    static CompositionList& get();

   protected:
    CompositionList() = default;

   protected:
    Composition::CompositionReceiver* mReceiver_p = nullptr;

   public:
    inline void repaint() { if( mReceiver_p ) mReceiver_p->repaint(); }

   public:
    inline void setDefaultReceiver( Composition::CompositionReceiver& receiver )
    { mReceiver_p = &receiver; }

   protected:
    friend Gui::Panel_CompositionManager;
    //compositions_t compositions;

   protected:
    QOpenGLWidget* widget = nullptr;
    QOpenGLFunctions_3_3_Core* gl = nullptr;

   public:
    /**
     *  @brief addComposition : emplace an composition type T and return his reference
     *  Can be initialiseGL if this composition list is already initialised
     */
    template<typename T>
    inline T& addComposition( std::unique_ptr<T> composition_up )
    {
      T& composition( *composition_up );
      emplace_back( std::move( composition_up ) );
      // if( gl )
      // {
      //   //gl->
      //   if( !composition.isInitializedGL() )
      //     composition.initialiseGL( *gl );
      // }
      return composition;
    }

   public:
    template<typename T, typename ... Args> inline
    std::unique_ptr<T> build( const Args& ... args )
    {
      if( !mReceiver_p )
        throw std::runtime_error( "[Composition::CompositionList] Need receiver" );
      auto c_up( std::make_unique<T>( *mReceiver_p, args ... ) );
      if( gl && widget && !c_up->isInitializedGL() )
      {
        widget->makeCurrent();
        c_up->initialiseGL( *gl );
        //widget->doneCurrent();
      }
      return c_up;
    }

   public:
    /**
     * @brief initialiseGL : initialiseGL composition
     */
    inline void initialiseGL( QOpenGLWidget* _widget, QOpenGLFunctions_3_3_Core* _gl )
    {
      widget = _widget;
      gl = _gl;

      // if( gl )
      //   return;
      // for( Composition_up& composition : *this )
      //   if( composition )
      //     if( !composition->isInitializedGL() )
      //       composition->initialiseGL( *gl );
    }

    //   public:
    //    class iterator
    //    {
    //     public:
    //      typedef iterator self_type;
    //      typedef AbstractComposition value_type;
    //      typedef AbstractComposition& reference;
    //      typedef AbstractComposition* pointer;
    //      typedef std::forward_iterator_tag iterator_category;
    //      typedef int difference_type;
    //      self_type operator ++() { self_type i = *this; mIt++; return i; }
    //      self_type operator ++( int ) { mIt++; return *this; }
    //      reference operator *() { return **mIt; }
    //      pointer operator ->() { return mIt->get(); }
    //      bool operator ==( const self_type& rhs ) { return mIt == rhs.mIt; }
    //      bool operator !=( const self_type& rhs ) { return mIt != rhs.mIt; }
    //     private:
    //      friend CompositionList;
    //      iterator( compositions_t::iterator it ) : mIt( it ) { }
    //      compositions_t::iterator mIt;
    //    };

    //    class const_iterator
    //    {
    //     public:
    //      typedef const_iterator self_type;
    //      typedef AbstractComposition value_type;
    //      typedef AbstractComposition& reference;
    //      typedef AbstractComposition* pointer;
    //      typedef std::forward_iterator_tag iterator_category;
    //      typedef int difference_type;
    //      self_type operator ++() { self_type i = *this; mIt++; return i; }
    //      self_type operator ++( int ) { mIt++; return *this; }
    //      reference operator *() { return **mIt; }
    //      pointer operator ->() { return mIt->get(); }
    //      bool operator ==( const self_type& rhs ) { return mIt == rhs.mIt; }
    //      bool operator !=( const self_type& rhs ) { return mIt != rhs.mIt; }
    //     private:
    //      friend CompositionList;
    //      const_iterator( compositions_t::const_iterator it ) : mIt( it ) { }
    //      compositions_t::const_iterator mIt;
    //    };

    //   public:
    //    iterator begin() {return iterator( compositions.begin() );}
    //    iterator end() {return iterator( compositions.end() );}
    //    const_iterator begin() const {return const_iterator( compositions.cbegin() );}
    //    const_iterator end()  const {return const_iterator( compositions.cend() );}
    //    const_iterator cbegin() const {return const_iterator( compositions.cbegin() );}
    //    const_iterator cend()  const {return const_iterator( compositions.cend() );}

    //   public:
    //    inline size_t size() const { return compositions.size(); }

    //   public:
    //    inline void clear() { return compositions.clear(); }
  };

}

