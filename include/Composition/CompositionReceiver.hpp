#pragma once

#include <QString>
#include <QWidget>
#include <QMenu>

#include <QMatrix4x4>

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>


namespace Composition {

  class CompositionReceiver
  {
   public:
    virtual ~CompositionReceiver() = default;
    virtual void repaint() = 0;
  };

}

