#pragma once

#include <Composition/AbstractComposition.hpp>
#include <Composition/CityShaderProgram/ShaderProgram/Construction.hpp>

#include <ShaderProgram/Vertex.hpp>

#include <memory>

namespace Gui { class Panel_Composition_City; }

namespace Composition {

  class City : public AbstractComposition
  {
   public:
    City( Composition::CompositionReceiver& _receiver );
    virtual ~City() override;
    friend Gui::Panel_Composition_City;

   protected:
    bool mDrawWireframe = true;
    bool mTransparent = false;
    bool mPrintGrid = true;

   protected:
    CityShaderProgram::Construction programblock_city_construction;
    ShaderProgram::Vertex programblock_basic_grid;

   protected:
    Math::Vec2f mWorldStart{ -10.f, -10.f };
    Math::Vec2f mWorldEnd{ +10.f, +10.f };

   protected:
    float mGridSpacing = 1.f;
    void updateGrid();

   public:
    void initialiseGL( QOpenGLFunctions_3_3_Core& gl ) override;
    void paintGL( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp ) override;

   private:
    bool mOpenglInitialized = false;

   public:
    inline bool isInitializedGL() const override { return mOpenglInitialized; }

   protected:
    Gui::Panel_Composition_City* const mAttachedCityWidget;

   public:
    virtual QWidget* getAttachedWidget() override;

  protected:
    uint mNbConstruction = 10;
    uint mNbPlace = 5 ;
    float mNeighborDistance = 5.f;

    bool isOneConstructionDemo = false;

  protected:
    void reloadLastDemo();

   public:
    void loadOneConstructionDemo();
    void loadDefaultDemo();

  };

  using City_up = std::unique_ptr<City>;

}
