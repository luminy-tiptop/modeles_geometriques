#pragma once

#include <ShaderProgram/AbstractShaderProgram.hpp>
#include <ShaderProgram/IndexedVertexValueUv.hpp>

#include <MeshAlgorithms/Generation/CityConstruction/Building.hpp>
#include <MeshAlgorithms/Generation/CityConstruction/VertexProperty.hpp>


#include <Math/Vec.hpp>
#include <Mesh.hpp>

#include <QOpenGLBuffer>
#include <QVector2D>
#include <QVector3D>
#include <bits/move.h>

namespace Composition {
  namespace CityShaderProgram {

    struct Construction : public ShaderProgram::AbstractShaderProgram
    {
     public:
      using GeneratedMesh_up = MeshAlgorithms::Generation::GeneratedMesh_up;
      using Attrib = MeshAlgorithms::Generation::CityConstruction::VertexProperty::Attrib;

     protected:
      struct ElementBuffer
      {
        uint verticeStart, verticeCount;
        uint indexStart, indexCount;

        inline ElementBuffer( uint _verticeStart, uint _verticeCount, uint _indexStart, uint _indexCount ) :
          verticeStart( std::move( _verticeStart ) ), verticeCount( std::move( _verticeCount ) ),
          indexStart( std::move( _indexStart ) ), indexCount( std::move( _indexCount ) )
        {}

        /** @todo remove it and replace b attrib divisor buffer */
        inline void draw( QOpenGLFunctions_3_3_Core& gl )
        {
          gl.glDrawElementsBaseVertex(
            GLenum( GL_TRIANGLES ),
            GLint( indexCount ),
            GL_UNSIGNED_INT,
            reinterpret_cast<const GLvoid*>( indexStart * sizeof( uint ) ),
            GLint( verticeStart )
          );
          //                    gl.glDrawElements(
          //                      GLenum( GL_TRIANGLES ),
          //                      GLint( indexCount ),
          //                      GL_UNSIGNED_INT,
          //                      nullptr
          //                    );
        }
      };

     protected:
      struct ConstructionFormat
      {
        Math::Vec3f vertex;
        //Attrib attrib;
        float attrib = 0.f;
        //int attrib;
        // inline ConstructionFormat( Math::Vec3f&& _vertex, Attrib&& _id ) : vertex( std::move( _vertex ) ), attrib( std::move( _id ) ) {}
        //inline ConstructionFormat( Math::Vec3f _vertex, Attrib _id ) : vertex( std::move( _vertex ) ), attrib( std::move( _id ) ) {}
        //inline ConstructionFormat( Math::Vec3f _vertex, Attrib _id ) : vertex( std::move( _vertex ) ), attrib( int( _id ) ) {}
        inline ConstructionFormat( Math::Vec3f _vertex, Attrib _id ) : vertex( std::move( _vertex ) ), attrib( float(_id) ) {}
      };
      using TriangleIndicesFormat = ShaderProgram::IndexedVertexValueUv::TriangleIndicesFormat;

     protected:
      struct PlacementFormat
      {
        QVector2D position;
        float angle;
        uint constructElementId;

        // inline PlacementFormat( Math::Vec2f&& _position, float&& _angle, uint&& _constructElementId ) :
        //   position( std::move( _position ) ), angle( std::move( _angle ) ), constructElementId( std::move( _constructElementId ) ) {}

        inline PlacementFormat( const Math::Vec2f& _position, float _angle, uint _constructElementId ) :
          position( _position[0], _position[1] ), angle( std::move( _angle ) ), constructElementId( std::move( _constructElementId ) ) {}
      };

     protected:
      bool mConstructionChanged = true;
      std::vector<ConstructionFormat> mVertices;
      std::vector<TriangleIndicesFormat> mTriangles;
      QOpenGLBuffer mVerticesBuffer{ QOpenGLBuffer::Type::VertexBuffer };
      QOpenGLBuffer mIndicesBuffer{ QOpenGLBuffer::Type::IndexBuffer };
      uint mCountVertices = 0;
      uint mCountIndices = 0;
      std::vector<ElementBuffer> mConstructionsInBuffers;

     protected:
      bool mPlacementsChanged = true;
      std::vector<PlacementFormat> mPlacements;
      // QOpenGLBuffer mPlacementsBuffer{ QOpenGLBuffer::Type::VertexBuffer };


     public:
      Construction();
      virtual ~Construction() override = default;

     protected:
      void updateConstructionBuffers();
      void updatePlacementBuffers();

     protected:
      bool mIsInitialized = false;

     public:
      virtual void initialise( QOpenGLFunctions_3_3_Core& gl ) override;
      virtual void paint( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp );

     public:
      void appendConstruction( const GeneratedMesh_up& genmesh_up );
      void placeConstruction( const Math::Vec2f& position, const float angle, const uint constructElementId );
      void clearConstruction();

     public:


    };


  }
}
