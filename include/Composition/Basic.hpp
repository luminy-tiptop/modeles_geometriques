#pragma once

#include <Composition/AbstractComposition.hpp>

#include <ShaderProgram/Vertex.hpp>
#include <ShaderProgram/VertexColorPack.hpp>
#include <ShaderProgram/VertexUvPack.hpp>

#include <Utils/EnumOperator.hpp>

#include <memory>


namespace Gui { class Panel_Composition_Basic; }

namespace Composition {

  class Basic : public AbstractComposition
  {
   public:
    enum class RenderMode : uint
    { NoRender = 0, Vertex = 1, VertexColor = 2, VertexUvTexture = 4 };

   public:
    Basic( Composition::CompositionReceiver& _receiver, std::string _name = "Basic" );
    virtual ~Basic() override;
    friend Gui::Panel_Composition_Basic;

   protected:
    Gui::Panel_Composition_Basic* const mAttachedBasicWidget;

   public:
    virtual QWidget* getAttachedWidget() override;
    //virtual QMenu * getAttachedMenu() override;
    virtual Mesh* getAttachedOpenMesh() override;

   protected:
    ShaderProgram::Vertex programblockByVertex;
    ShaderProgram::VertexColorPack programblockByVertexColor;
    ShaderProgram::VertexUvPack programblockByVertexUv;

   protected:
    QOpenGLBuffer bufferIndex{ QOpenGLBuffer::Type::IndexBuffer };
    uint bufferIndexCount = 0;
    bool drawUseIndexBuffer = false;

   protected:
    GLenum drawPrimitiveType = 0;
    ShaderProgram::AbstractShaderProgram* drawShaderProgram_p = nullptr;

   private:
    bool mOpenglInitialized = false;

   public:
    void initialiseGL( QOpenGLFunctions_3_3_Core& gl ) override;
    void paintGL( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp ) override;

   public:
    inline bool isInitializedGL() const override { return mOpenglInitialized; }

   protected:
    bool mDrawWireFrame = true;

   public:
    inline void drawWireframe( const bool value ) { mDrawWireFrame = value; }
    inline bool drawWireframe() const { return mDrawWireFrame; }

   private:
    void unsetDraw();
    bool isDone() const;

   private:
    //BasicAction::RenderCompatibleMode meshCompatibleMode = BasicAction::RenderCompatibleMode::NoRender;
    std::unique_ptr<Mesh> mMeshBase_up;
    std::string mBaseDirectory = ":/mesh/";
    OpenMesh::IO::Options mMeshOpt;
    bool mMeshFirstLoad = true;

   protected:
    RenderMode mActualMode = RenderMode::NoRender;
    RenderMode mCompatibleMode = RenderMode::NoRender;

   public:
    inline RenderMode compatibleMode() const { return mCompatibleMode; }
    inline RenderMode actualMode() const { return mActualMode; }
    void setActualMode( const RenderMode m );

   private:
    void setCompatibleAndActualForRenderMode( const RenderMode m );

   public:
    void loadMesh( const std::string& path, const std::string& baseDirectory, const bool importTexture = false, const bool importColor = true );
    void loadInternalMesh( const std::string& path, const bool importColor = true );

   private:
    void internalMeshIndex( const Mesh& mesh );

   public:
    void loadMesh( const std::vector<QVector3D>& vertices );
    void loadMesh( std::unique_ptr<Mesh> mesh_up, const bool importTexture = false, const bool importColor = true  );

   public:
    void reloadMesh();
  };

  using Basic_up = std::unique_ptr<Basic>;

}

// ---- ---- ---- ----

Utils_EnumOperator( Composition::Basic::RenderMode, uint )


