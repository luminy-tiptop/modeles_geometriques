#pragma once

#include <Composition/Basic.hpp>

#include <ShaderProgram/IndexedVertexValueUv.hpp>
#include <Math/ImplicitFunction.hpp>

#include <memory>


namespace Gui { class Panel_Composition_ImplicitFunction; }

namespace Composition {

  class ImplicitFunction : public AbstractComposition
  {
   public:
    ImplicitFunction( Composition::CompositionReceiver& _receiver );
    virtual ~ImplicitFunction() override;
    friend Gui::Panel_Composition_ImplicitFunction;

   protected:
    bool mDrawWireframe = true;
    bool mTransparent = false;
    bool mIndicesMode = true;
    bool mChangeSeed = true;

   protected:
    uint lastSeed = 1;
    void updateSeed();

   protected:
    ShaderProgram::IndexedVertexValueUv programblock;

   public:
    void initialiseGL( QOpenGLFunctions_3_3_Core& gl ) override;
    void paintGL( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp ) override;

   private:
    bool mOpenglInitialized = false;

   public:
    inline bool isInitializedGL() const override { return mOpenglInitialized; }

   protected:
    Gui::Panel_Composition_ImplicitFunction* const mAttachedImplicitFunctionWidget;

   public:
    virtual QWidget* getAttachedWidget() override;

   public:
    void compute(
      Math::ImplicitFunction::func_pf_t function,
      double size, int bounds,
      double x, double y, double z,
      bool withTetrahedralDecomposition,
      uint limitProcess = 100000
    );

  };

  using ImplicitFunction_up = std::unique_ptr<ImplicitFunction>;

}
