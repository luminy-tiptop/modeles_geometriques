/**
 * @author https://stackoverflow.com/a/5205786
 */

#include <ios>
#include <QIODevice>

namespace Wrapper {


  class QStreamBuf : public std::streambuf
  {
   public:
    inline QStreamBuf( QIODevice & dev ) : std::streambuf(), m_dev( dev ) {
      // Initialize get pointer.  This should be zero so that underflow is called upon first read.
      this->setg( nullptr, nullptr, nullptr );
    }

   protected:
    virtual inline std::streamsize xsgetn( std::streambuf::char_type * str, std::streamsize n ) {
      return m_dev.read( str, n );
    }

    virtual inline std::streamsize xsputn( const std::streambuf::char_type * str, std::streamsize n ) {
      return m_dev.write( str, n );
    }

    virtual inline std::streambuf::pos_type seekoff( std::streambuf::off_type off, std::ios_base::seekdir dir, std::ios_base::openmode /*__mode*/ ) {
      switch ( dir ) {
      case std::ios_base::beg:
        break;
      case std::ios_base::end:
        off = m_dev.size() - off;
        break;
      case std::ios_base::cur:
        off = m_dev.pos() + off;
        break;
      default:
        break;
      }
      if ( m_dev.seek( off ) )
        return m_dev.pos();
      else
        return std::streambuf::pos_type( std::streambuf::off_type( -1 ) );
    }

    virtual inline std::streambuf::pos_type seekpos( std::streambuf::pos_type off, std::ios_base::openmode /*__mode*/ ) {
      if ( m_dev.seek( off ) )
        return m_dev.pos();
      else
        return std::streambuf::pos_type( std::streambuf::off_type( -1 ) );
    }

    virtual inline std::streambuf::int_type underflow() {
      // Read enough bytes to fill the buffer.
      std::streamsize len = sgetn( m_inbuf, sizeof( m_inbuf ) / sizeof( m_inbuf[0] ) );

      // Since the input buffer content is now valid (or is new)
      // the get pointer should be initialized (or reset).
      setg( m_inbuf, m_inbuf, m_inbuf + len );

      // If nothing was read, then the end is here.
      if ( len == 0 )
        return traits_type::eof();

      // Return the first character.
      return traits_type::not_eof( m_inbuf[0] );
    }

   private:
    static const std::streamsize BUFFER_SIZE = 1024;
    std::streambuf::char_type m_inbuf[BUFFER_SIZE];
    QIODevice & m_dev;
  };

  class QIstream : private QStreamBuf, public std::istream
  {
   public:
    inline QIstream( QIODevice & dev ) :
      QStreamBuf( dev ),
      std::istream( this )
    {}

    virtual inline ~QIstream() {
      rdbuf( nullptr );
    }
  };

}
