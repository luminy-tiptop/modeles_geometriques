/**
 * @author GIG 2018 : D’AMBRA / BIRJAM / SONNTAG
 */


#pragma once

#include <Mesh.hpp>

#include <vector>

namespace MeshAlgorithms {
  namespace MeshRepair {

    void doRepair( Mesh& mesh );

  }
}
