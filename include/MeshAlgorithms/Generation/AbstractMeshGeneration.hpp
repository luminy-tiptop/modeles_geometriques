#pragma once

#include <Mesh.hpp>

#include <memory>

namespace MeshAlgorithms {
  namespace Generation {

    struct GeneratedMesh
    {
     public:
      bool haveColor = false;
      bool haveNormal = false;

     public:
      bool haveTexture = false;
      std::string texturePath;

     public:
      Mesh mesh;
    };

    // ---- ----

    using GeneratedMesh_up = std::unique_ptr<GeneratedMesh>;

    // ---- ----

    class AbstractMeshGeneration
    {
     public:
      virtual GeneratedMesh_up generate() = 0;
      virtual ~AbstractMeshGeneration() = default;
    };

  }
}

