#pragma once

#include <MeshAlgorithms/Generation/AbstractMeshGeneration.hpp>

namespace MeshAlgorithms {
  namespace Generation {
    namespace CityConstruction {

      class AbstractConstruction : public AbstractMeshGeneration
      {
       public:
        AbstractConstruction() = default;

       public:
        virtual GeneratedMesh_up generate() = 0;
      };

    }
  }
}

