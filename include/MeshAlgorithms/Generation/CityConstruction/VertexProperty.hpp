#pragma once

#include <MeshAlgorithms/Generation/CityConstruction/AbstractConstruction.hpp>
#include <Utils/AbstractProceduralSeed.hpp>
#include <Mesh.hpp>

namespace MeshAlgorithms {
  namespace Generation {
    namespace CityConstruction {
      namespace VertexProperty {

        enum class Attrib : int {
          Ground = 0   ,
          Grass        ,
          Water        ,
          BuildingGrey1,
          BuildingGrey2,
          BuildingRoof ,
        };

        // ---- ----

        using Attrib_Handle = OpenMesh::VPropHandleT<Attrib>;
        extern Attrib_Handle attribHandle;

        // ---- ----

        inline void Attrib_addProperty( Mesh& m )
        { m.add_property( attribHandle, "attrib" ); }

        inline bool Attrib_haveProperty( Mesh& m )
        { return m.get_property_handle( attribHandle, "attrib" ); }

        template<typename THandle>
        inline void Attrib_setProperty( Mesh& m, THandle& h, Attrib a )
        { m.property( attribHandle, h ) = a; }

        template<typename THandle>
        inline Attrib Attrib_getProperty( Mesh& m, THandle& h )
        { return m.property( attribHandle, h ); }

      }
    }
  }
}

