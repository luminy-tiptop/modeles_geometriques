#pragma once

#include <MeshAlgorithms/Generation/CityConstruction/AbstractConstruction.hpp>
#include <MeshAlgorithms/Generation/CityConstruction/VertexProperty.hpp>

#include <Utils/AbstractProceduralSeed.hpp>

namespace MeshAlgorithms {
  namespace Generation {
    namespace CityConstruction {

      class Building : public AbstractConstruction
      {
       public:
        enum class Type
        { small = 0, medium, large };

       public:
        struct Properties : public Utils::AbstractProceduralSeed
        {
         public:
          uint nbFloor = 0;
          float width = 1.f;
          float height = 1.f;
          Type type = Type::medium;

          float roofHeight = 0.5f;

         public:
          float roofScale = 1.1f;

         public:
          void refresh() override
          {
            type = randEnum( Type::small, Type::large );
            width = 1.f;
            switch( type )
            {
              case Type::small :
                //width = 1.f;
                height = randValue( 1.f, 2.f );
                roofHeight = randValue( 1.f, 2.f );
                nbFloor = randPositiveValue( 0, 1 );
                break;
              case Type::medium :
                //width = float(randValue( 1, 2 ) );
                height = 2.f;
                roofHeight = randValue( 1.f, 2.f );
                nbFloor = randPositiveValue( 1, 2 );
                break;
              case Type::large :
               //width = float(randValue( 2, 3 ) );
                height = randValue( 2.f, 3.f );
                roofHeight = randValue( 2.f, 3.f );
                nbFloor = randPositiveValue( 2, 3 );
                break;
            }
          }

         public:
          inline Properties() { refresh(); }
          virtual ~Properties() override = default;
        };

       public:
        Building();

       public:
        GeneratedMesh_up generate() override;
      };

    }
  }
}

