#pragma once

#include <Composition/AbstractComposition.hpp>
#include <memory>

namespace MeshAlgorithms {
  namespace Generation {

    class AbstractCompositionGeneration
    {
     public:
      virtual std::unique_ptr<::Composition::AbstractComposition> generate() = 0;
      virtual ~AbstractCompositionGeneration() = default;
    };

  }
}
