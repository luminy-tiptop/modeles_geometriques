#pragma once

#include <types.hpp>
#include <Mesh.hpp>

#include <vector>
#include <unordered_map>

namespace MeshAlgorithms {
  namespace AppIndus {

    struct MeshQuality
    {
     public:
      static constexpr float precision = 0.001f;
      static constexpr float radianToPi = float(180. / M_PI);

     public:
      inline float roundDot( const float a ) const { return std::round( a / precision ) * precision; }
      inline float toDegrees( const float radian ) const { return roundDot( radian * radianToPi ); }

     public:
      uint facesCount = 0;
      uint vertexCount = 0;
      // float surfaceQuality = 0.f;

     public:
      uint almostEquilateralTriangleCount = 0;
      uint notEquilateralTriangleCount = 0;

     public:
      using NbOccurence = uint;
      using Degrees = float;
      //std::unordered_map<float, NbOccurence> lessAngleCount;
      std::unordered_map<uint, NbOccurence> valenceVertexCount;
      std::unordered_map<Degrees, NbOccurence> dihedralAngleFaceCount;
      std::unordered_map<Degrees, NbOccurence> angleVertexCount;

     public:
      MeshQuality( const Mesh& m );

     private:
      //void addAngleQuality( float a );
      void addTriangleQuality( float a, float b, float c );
    };



    // struct MeshQuality
    // {
    //   uint facesCount = 0;
    //   float surfaceQuality = 0.f;
    //   uint equilateralTriangleCount = 0;
    //   uint notEquilateralTriangleCount = 0;
    //
    //   MeshQuality( const Mesh& m );
    // };



  }
}
