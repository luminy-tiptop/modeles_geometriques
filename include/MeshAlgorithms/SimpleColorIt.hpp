#pragma once

#include <Mesh.hpp>

namespace MeshAlgorithms {
  namespace SimpleColorIt {

    void colorNeighbour( Mesh & mesh );
    void colorVertices( Mesh & mesh );
    void colorCurves( Mesh & mesh );

  }
}
