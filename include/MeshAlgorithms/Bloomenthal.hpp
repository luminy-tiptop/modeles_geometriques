/**
 * @brief Bloomenthal implementation
 * @author Original Article can be found at :
 *  - http://www.unchainedgeometry.com/jbloom/pdf/polygonizer.pdf
 * @author Released by GIG 2018 Team
 */

#pragma once

#include <Math/ImplicitFunction.hpp>
#include <ShaderProgram/IndexedVertexValueUv.hpp>

#include <Mesh.hpp>
#include <types.hpp>

#include <chrono>
#include <memory>

namespace MeshAlgorithms {

  struct Bloomenthal_PROCESS;

  /**
   * @brief The Bloomenthal class
   *
   * @author Original Article can be found at :
   *  - http://www.unchainedgeometry.com/jbloom/pdf/polygonizer.pdf
   *  -
   * @author Released by GIG 2018 Team
   */
  class Bloomenthal
  {
   public:
    static constexpr bool FreeCubeTable = false;

   public:
    enum class TetrahedralMode {
      WithDecomposition = 0,     /**< use tetrahedral decomposition */
      WithoutDecomposition = 1,  /**< no tetrahedral decomposition */
    };

   public:
    using CN = Math::CN;
    using Point3D = Math::Point3D;

   public:
    /** surface vertex */
    using VertexFormat = ShaderProgram::IndexedVertexValueUv::VertexFormat;
    using TriangleIndicesFormat = ShaderProgram::IndexedVertexValueUv::TriangleIndicesFormat;

   public:
    using TrianglesIndices = std::vector<TriangleIndicesFormat>;  /**< triangles indices */
    using Vertices = std::vector<VertexFormat>;          /**< points property*/

   protected:
    Vertices mVertices;
    TrianglesIndices mTrianglesIndice;
    friend Bloomenthal_PROCESS;

   public:
    inline const Vertices& vertices() const { return mVertices; }
    inline const TrianglesIndices& triangles() const { return mTrianglesIndice; }

   public:
    Bloomenthal(
      Math::ImplicitFunction::func_pf_t function,
      CN size, int bounds,
      CN x, CN y, CN z,
      TetrahedralMode mode,
      uint seed = unsigned(std::chrono::system_clock::now().time_since_epoch().count() ),
      uint limitProcess = 100000
    );

    ~Bloomenthal();
  };



}
