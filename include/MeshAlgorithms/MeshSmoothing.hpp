#pragma once

#include <Mesh.hpp>
#include <Math/Vec.hpp>

namespace MeshAlgorithms {
  namespace MeshSmoothing {


    struct AbstractSmoothAlgorithm
    {
      virtual Math::Vec3f getSmoothedVertex( const Mesh& mesh, const Mesh::VertexIter& v_it ) = 0;
    };

    // ----- ---- ---- ----

    /** @todo parralelize it */
    inline void doSmooth( Mesh& mesh, AbstractSmoothAlgorithm& smoother )
    {
      Mesh::VertexIter v_it( mesh.vertices_begin() );
      const Mesh::VertexIter vEnd_cit( mesh.vertices_end() );
      for(; v_it != vEnd_cit; ++v_it )
        mesh.set_point( *v_it, smoother.getSmoothedVertex( mesh, v_it ) );
    }

    // ----- ---- ---- ----

    struct LaplaceBeltramiOperateurApproximationCotangentielle :
      public AbstractSmoothAlgorithm
    {
      const float coef_h;
      const float limit_diff_delta;

      LaplaceBeltramiOperateurApproximationCotangentielle( const float coef_h, const float limit_diff_delta );
      Math::Vec3f getSmoothedVertex( const Mesh& mesh, const Mesh::VertexIter& v_it ) override;
      virtual ~LaplaceBeltramiOperateurApproximationCotangentielle() = default;
    };

    // ---- ----

    //    struct LaplaceBeltramiOperateurApproximationUniforme :
    //      public AbstractSmoothAlgorithm
    //    {
    //      const float coef_h;
    //      LaplaceBeltramiOperateurApproximationUniforme( const float coef_h, const float limit_diff_delta );
    //      Math::Vec3f getSmoothedVertex( const Mesh& mesh, const Mesh::VertexIter& v_it ) override;
    //      virtual ~LaplaceBeltramiOperateurApproximationUniforme() = default;
    //    };

  }
}
