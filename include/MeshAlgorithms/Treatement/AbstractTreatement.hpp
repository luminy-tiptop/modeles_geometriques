#pragma once

#include <Mesh.hpp>

namespace MeshAlgorithms {
  namespace Treatement {

    class AbstractTreatement
    {
     public:
      virtual void process( Mesh& m ) = 0;
      virtual ~AbstractTreatement() = default;
    };

  }
}

