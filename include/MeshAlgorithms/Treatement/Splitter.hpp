#pragma once

#include <Mesh.hpp>

namespace MeshAlgorithms {
  namespace Treatement {

    class Splitter
    {
     protected:
      uint mNbSplit;

     public:
      Splitter( uint mNbSplit );

     public:
      void process( Mesh& m );

    };

  }
}

