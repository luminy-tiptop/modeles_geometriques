#pragma once


namespace Gui {
  namespace QtTricks {

    template<typename TItem>
    inline void setCollapsed( bool state, TItem* i )
    { i->setMaximumHeight( state ? 100000 : 20 ); }

    template<typename TItem>
    inline void setCollapsedAndCheck( bool state, TItem* i )
    { i->setChecked( state ); setCollapsed( state, i ); }

  }
}
