#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>

#include <Composition/CompositionList.hpp>
#include <Composition/CompositionReceiver.hpp>

#include <Matrices.hpp>

#include <OpenMesh/Core/Geometry/VectorT.hh>

#include <QDebug>
#include <QGLWidget>
#include <QMouseEvent>

#include <memory>
#include <vector>


namespace Gui {

  class MeshViewerWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
  {
    Q_OBJECT
   public:
    MeshViewerWidget( QWidget* _parent = nullptr );

   protected:

    /** matrice transformation */
    struct Camera
    {
      Matrices::ModelQuaternion model;

      /** @todo BaseCamera  */

      Matrices::ViewLookAt view;
      Matrices::Projection projection;

      Matrices::ModelUtils::TrackBall modelTrackball{ model };
      std::unique_ptr<Matrices::ModelUtils::TrackBall::Tracker> modelTrackballTracker_up;

      Camera();
      inline QMatrix4x4 compute() const { return projection.m * view.m * model.m; }
      inline void reset() { projection.reset(); view.reset(); model.reset(); }
    }
    mCamera;

   protected:
    struct CompositionReceiver : public ::Composition::CompositionReceiver
    {
      MeshViewerWidget& parent;
      CompositionReceiver( MeshViewerWidget& _parent ) : parent( _parent ) {}
      inline void repaint() override { parent.repaint(); }
    }
    mCompositionReceiver{ *this };

   protected:
    Composition::CompositionList& mCompositions;

   protected:
    void initializeGL();
    void resizeGL( int _w, int _h );
    void paintGL();

   protected:
    QPoint mMouseLastPosition;


   protected:
    // Qt mouse events
    virtual void mousePressEvent( QMouseEvent* );
    virtual void mouseReleaseEvent( QMouseEvent* );
    virtual void mouseMoveEvent( QMouseEvent* );
    virtual void wheelEvent( QWheelEvent* );

  };

}
