#pragma once

#include <QMainWindow>
#include <QVector>

#include <memory>

namespace Ui { class Panel_Composition_Basic_InfoModal; }

namespace MeshAlgorithms { namespace AppIndus {  struct MeshQuality; }}

class QCustomPlot;

namespace Gui {

  class Panel_Composition_Basic_InfoModal : public QMainWindow
  {
    Q_OBJECT

   protected:
    std::unique_ptr<MeshAlgorithms::AppIndus::MeshQuality> q_up;

   protected:
    QVector<double> x, y;

   public:
    Panel_Composition_Basic_InfoModal( std::unique_ptr<MeshAlgorithms::AppIndus::MeshQuality> q_up, QWidget* parent = nullptr );
    ~Panel_Composition_Basic_InfoModal();

   protected:
    void fillInfoBox();
    void autoCorrectCheckBox();

   private:
    void emplaceItem( std::unique_ptr<QCustomPlot>& var, void (Panel_Composition_Basic_InfoModal::*f)(QCustomPlot & p) );
    void removeItem( std::unique_ptr<QCustomPlot>& var );

   protected:
    std::unique_ptr<QCustomPlot> plot_valence;
    std::unique_ptr<QCustomPlot> plot_dihedralAngles;
    std::unique_ptr<QCustomPlot> plot_meshAngles;

   protected:
    void setupPlot_Valence( QCustomPlot& plot );
    void setupPlot_DihedralAngles( QCustomPlot& plot );
    void setupPlot_MeshAngles( QCustomPlot& plot );

   private slots:
    void on_checkBox_AnglesMaillage_stateChanged( int arg1 );
    void on_checkBox_AnglesDiedres_stateChanged( int arg1 );
    void on_checkBox_Valence_stateChanged( int arg1 );

    protected:
    Ui::Panel_Composition_Basic_InfoModal* const ui;
  };

}
