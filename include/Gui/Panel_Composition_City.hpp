#pragma once

#include <QWidget>
#include <Gui/Panel_Composition_City.hpp>

namespace Ui { class Panel_Composition_City; }
namespace Composition { class City; }

namespace Gui {

  class Panel_Composition_City : public QWidget
  {
    Q_OBJECT

   public:
    Panel_Composition_City( ::Composition::City& composition, QWidget* parent = nullptr );
    ~Panel_Composition_City();

   private:
    bool mRequestChangeIsLocked = false;

   protected:
    friend Composition::City;
    void requestChange( bool force = false );

   private slots:
    void on_checkBox_wireframe_stateChanged( int arg1 );
    void on_checkBox_transparent_stateChanged( int arg1 );
    void on_checkBox_grid_stateChanged(int arg1);

  private slots:
    void on_doubleSpinBox_gridSpacing_valueChanged(double arg1);

  private slots:
    void on_doubleSpinBox_prox_valueChanged(double arg1);
    void on_spinBox_nbElement_valueChanged(int arg1);
    void on_spinBox_nbConstruction_valueChanged(int arg1);

   private slots:
    void on_pushButton_generate_clicked();

   protected:
    Ui::Panel_Composition_City* const ui;
    ::Composition::City& mComposition;
  };


}




