#pragma once

#include <QMainWindow>

#include <Composition/AbstractComposition.hpp>

namespace Ui { class MainWindow; }
namespace Composition { class CompositionList; }

namespace Gui {

  class MainWindow : public QMainWindow
  {
    Q_OBJECT

   protected:
    bool is_initialized = false;
   public:
    explicit MainWindow( const QApplication& app, QWidget* parent = nullptr );
    ~MainWindow();
    void showEvent( QShowEvent* event );

   protected:
    Composition::AbstractComposition* currentComposition_p = nullptr;

   public:
    void setComposition( Composition::Composition_up c );

   protected:
    void clearComposition();
    void setActiveCompositionToUi( Composition::AbstractComposition* currentComposition_p  );

   protected:
    void loadExternalMeshByPopup( const bool importTexture );

   private slots:
    void on_actionOpenMesh_triggered();
    void on_actionOpenTexturedMesh_triggered();
    void on_actionOpenInternalBunny_triggered();
    void on_actionComputeInternalTriangle_triggered();

   private slots:
    void on_actionGenerateBuilding_triggered();
    void on_actionGenerateCity_triggered();

   private slots:
    void on_actionGenerateSurfaceImplicit_triggered();

   protected:
    Ui::MainWindow* const ui;
    Composition::CompositionList& mCompositions;
  };

}
