#pragma once

#include <QWidget>

#include <Mesh.hpp>

#include <map>
#include <functional>
#include <memory>

namespace Ui { class Panel_Composition_Basic; }
namespace Composition { class Basic; }

namespace Gui {

  class Panel_Composition_Basic : public QWidget
  {
    Q_OBJECT

   public:
    Panel_Composition_Basic( ::Composition::Basic& composition, QWidget* parent = nullptr );
    ~Panel_Composition_Basic();

   protected:
    friend ::Composition::Basic;
    void refreshUi();
    void refreshUiRenderModeChoice();

   private slots:
    void on_checkBox_drawWireframe_stateChanged( int arg1 );

   private slots:
    void on_radioButton_shaderVertexUv_clicked();
    void on_radioButton_shaderVertexColor_clicked();
    void on_radioButton_shaderVertex_clicked();

   protected:
    using applyAction_t = std::function<void ( )>;
    using applyActionMesh_t = std::function<void ( Mesh& )>;
    std::map<QString, std::function<void()>> applyTests;
    void applyTest( const applyActionMesh_t& actionOnMesh );

   private slots:
    void on_pushButton_applyTest_clicked();

   protected:
    std::unique_ptr<QWidget> info_up;

   private slots:
    void on_pushButton_Info_clicked();
    void on_pushButton_SmoothLB_clicked();
    void on_groupBox_SmoothLB_clicked(bool checked);

   protected:
    Ui::Panel_Composition_Basic* const ui;
    ::Composition::Basic& mComposition;
  private slots:
    void on_groupBox_Repair_clicked(bool checked);

  private slots:
    void on_pushButton_repair_clicked();
  };


}
