#pragma once

#include <Gui/Panel_Composition_Basic.hpp>
#include <Gui/Panel_Composition_ImplicitFunction.hpp>

namespace Ui { class Panel_Composition_ImplicitFunction; }
namespace Composition { class ImplicitFunction; }

namespace Gui {

  class Panel_Composition_ImplicitFunction : public QWidget
  {
    Q_OBJECT

   public:
    Panel_Composition_ImplicitFunction( ::Composition::ImplicitFunction& composition, QWidget* parent = nullptr );
    ~Panel_Composition_ImplicitFunction();

   private:
    bool mRequestChangeIsLocked = false;

   protected:
    friend Composition::ImplicitFunction;
    void requestChange( bool force = false );

   private slots:
    void on_checkBox_wireframe_stateChanged( int arg1 );
    void on_checkBox_transparent_stateChanged( int arg1 );
    void on_checkBox_changeSeed_stateChanged( int arg1 );
    void on_checkBox_indicesMode_stateChanged( int arg1 );

   protected:
    void setDefaultPreset( bool forceRefresh = false );

   private slots:
    void on_comboBox_function_activated( const QString& arg1 );
    void on_pushButton_defaultFunctionPreset_clicked();

  private slots:
    void on_checkBox_useTetrahedral_stateChanged(int arg1);

   private slots:
    void on_doubleSpinBox_size_valueChanged( double arg1 );
    void on_spinBox_bounds_valueChanged( int arg1 );
    void on_spinBox_limitProcess_valueChanged( int arg1 );

   private slots:
    void on_doubleSpinBox_x_valueChanged( double arg1 );
    void on_doubleSpinBox_y_valueChanged( double arg1 );
    void on_doubleSpinBox_z_valueChanged( double arg1 );

   private slots:
    void on_pushButton_generate_clicked();

   protected:
    Ui::Panel_Composition_ImplicitFunction* const ui;
    ::Composition::ImplicitFunction& mComposition;
  };


}



