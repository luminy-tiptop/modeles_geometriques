#pragma once

#include <Matrices/AbstractMatrice.hpp>

#include <QtMath>
#include <QVector3D>
#include <QQuaternion>

namespace Matrices {

  struct Model : public AbstractMatriceObject<Model>
  {
   public:
    QVector3D position;
    QVector3D rotation;
    QVector3D scale;

   public:
    inline Model() {reset();}
    inline Model( const QVector3D _position, const QVector3D _rotation, const QVector3D _scale ) :
      position( std::move( _position ) ), rotation( std::move( _rotation ) ), scale( std::move( _scale ) )
    { compute(); }

   public:
    inline const mat4x4_t & compute() override {  /** @todo check order */
      m.setToIdentity();
      m.scale( scale );
      m.translate( position );
      // Order YZX is convention :
      m.rotate( qRadiansToDegrees( rotation.y() ), QVector3D( 0.f, 1.f, 0.f ) );
      m.rotate( qRadiansToDegrees( rotation.z() ), QVector3D( 0.f, 0.f, 1.f ) );
      m.rotate( qRadiansToDegrees( rotation.x() ), QVector3D( 1.f, 0.f, 0.f ) );
      return m;
    }

   public:
    inline const Model & internalDefault() const override {
      static const Model defaultModel { { 0.f, 0.f, 0.f }, { 0.f, 0.f, 0.f }, { 1.f, 1.f, 1.f } };
      return defaultModel;
    }
  };

  struct ModelQuaternion : public AbstractMatriceObject<ModelQuaternion>
  {
   public:
    QVector3D position;
    QQuaternion rotation;
    QVector3D scale;

   public:
    inline ModelQuaternion() {reset();}
    inline ModelQuaternion( const QVector3D _position, const QQuaternion _rotation, const QVector3D _scale ) :
      position( std::move( _position ) ), rotation( std::move( _rotation ) ), scale( std::move( _scale ) )
    { compute(); }

   public:
    inline const mat4x4_t & compute() override {  /** @todo check order */
      m.setToIdentity();
      m.scale( scale );
      m.translate( position );
      m.rotate( rotation );
      return m;
    }

   public:
    inline const ModelQuaternion & internalDefault() const override {
      static const ModelQuaternion defaultModel { { 0.f, 0.f, 0.f }, { 1.f, 0.f, 0.f, 0.f }, { 1.f, 1.f, 1.f } };
      return defaultModel;
    }
  };

}

