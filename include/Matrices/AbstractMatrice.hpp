#pragma once

#include <QMatrix4x4>

#include <memory>

namespace Matrices {

  struct AbstractMatrice
  {
   public:
    virtual ~AbstractMatrice() = default;

   public:
    using mat4x4_t = QMatrix4x4;
    mat4x4_t m;

   public:
    /** @brief glm::Mat4x4 Cast Operator */
    inline operator const mat4x4_t & () const { return m; }

   public:
    virtual const mat4x4_t & compute() = 0;
    virtual const mat4x4_t & reset() = 0;
  };

  // ---- ---- ---- ----

  template<typename TMatrice>
  struct AbstractMatriceObject : public AbstractMatrice
  {
   public:
    AbstractMatriceObject() = default;
    AbstractMatriceObject( const AbstractMatriceObject & ) = default;
    AbstractMatriceObject( AbstractMatriceObject && ) = default;
    AbstractMatriceObject & operator=( const AbstractMatriceObject & ) = default;
    AbstractMatriceObject & operator=( AbstractMatriceObject && ) = default;
    virtual ~AbstractMatriceObject() = default;

   protected:
    std::shared_ptr<TMatrice> mDefault_sp;
    virtual const TMatrice & internalDefault() const = 0;

   protected:
    virtual void internalOverwrite( const TMatrice & from )
    { reinterpret_cast<TMatrice &>( *this ) = from; }

   public:
    /** @brief Reset from default matrice of sub-inheritance or user-custom-default Matrice */
    inline const mat4x4_t & reset() override {
      if ( mDefault_sp ) {
        auto k( mDefault_sp );
        internalOverwrite( *mDefault_sp );
        mDefault_sp = k;
      }
      else
        internalOverwrite( getDefault() );
      return m;
    }

   public:
    inline const TMatrice & getDefault() const {
      if ( mDefault_sp ) return *mDefault_sp;
      else return internalDefault();
    }
    inline void setDefault( const TMatrice & d ) {
      mDefault_sp = std::make_unique<TMatrice>( d );
      mDefault_sp->compute();
    }

  };

}

