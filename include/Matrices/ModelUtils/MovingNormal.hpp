#pragma once

#include <Matrices/Model.hpp>

#include <QVector2D>
#include <QVector3D>

#include <bits/move.h>
#include <cmath>

namespace Matrices {
  namespace ModelUtils {


    /**
     * @see Qt Cube example
     */
    struct MovingNormal
    {
     public:
      float trackingSpeed = 0.1f;

     public:
      struct Tracker
      {
       protected:
        MovingNormal & parent;

       public:
        QVector2D track;

       protected:
        friend MovingNormal;
        inline Tracker( MovingNormal & _parent, const QVector2D _newTrack ) :
          parent( _parent ),
          track( std::move( _newTrack ) )
        {}

       public:
        inline void move( const QVector2D & otherTrack ) {

          //
          const QVector2D diff( otherTrack - track );
          track = otherTrack;

          // Rotation axis is perpendicular to the mouse position difference vector
          QVector3D n( QVector3D( diff.y(), diff.x(), 0.0 ).normalized() );

          // Accelerate angular speed relative to the length of the mouse sweep
          const float acc( diff.length() * parent.trackingSpeed );

          // Calculate new rotation axis as weighted sum
          const QVector3D rotationAxis( ( n * acc ).normalized() );

          // Update rotation
          parent.model.rotation = QQuaternion::fromAxisAndAngle( rotationAxis, acc ) * parent.model.rotation;
          parent.model.compute();
        }
      };

     public:
      ModelQuaternion & model;

     public:
      inline MovingNormal( ModelQuaternion & _model ) :
        model( _model )
      {}

     public:
      inline Tracker newTrackPositionFromScreen( const QVector2D & newTrack )
      { return Tracker( *this, newTrack ); }

    };

  }
}

