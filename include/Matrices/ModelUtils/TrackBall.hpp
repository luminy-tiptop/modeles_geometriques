#pragma once

#include <Matrices/Model.hpp>

#include <Math/Vec.hpp>

#include <QVector2D>
#include <QVector3D>

#include <QMatrix3x3>

#include <bits/move.h>
#include <cmath>

#include <debug.hpp>

namespace Matrices {
  namespace ModelUtils {


    /**
     * @brief The TrackBall struct
     * @see https://www.khronos.org/opengl/wiki/Object_Mouse_Trackball
     * @see http://nehe.gamedev.net/tutorial/arcball_rotation/19003/
     * @see https://github.com/mmeeks/rep-snapper/blob/master/ArcBall.cpp
     */
    struct TrackBall
    {
     public:
      /**< @todo float trackingSpeed = 0.1f;  */

     public:
      struct Tracker
      {
       protected:
        TrackBall& parent;

       public:
        const QVector2D screenAjust;

       protected:
        const QQuaternion originalRotation;
        QVector3D trackStartOnSphere;

       protected:
        inline QVector3D matToSphere( const QVector2D& pointFromScreen )
        {
          //Adjust point coords and scale down to range of [-1 ... 1]
          const QVector2D p(
            ( pointFromScreen.x() * screenAjust.x() ) - 1.f,
            1.f - ( pointFromScreen.y() * screenAjust.y() )
          );

          //Compute the square of the length of the vector to the point from the center
          const float length( ( p.x() * p.x() ) + ( p.y() * p.y() ) );

          //If the point is mapped outside of the sphere... (length > radius squared)
          if( length > 1.0f )
          {
            //Compute a normalizing factor (radius / sqrt(length))
            const float norm( 1.0f / std::sqrt( length ) );

            //Return the "normalized" vector, a point on the sphere
            return QVector3D( p * norm, 0.f );
          }

          //Else it's on the inside
          else
          {

            //Return a vector to a point mapped inside the sphere sqrt(radius squared - length)
            return QVector3D( p, std::sqrt( 1.f - length ) );
          }
        }

       protected:
        friend TrackBall;
        inline Tracker( TrackBall& _parent, const QVector2D& newTrackFromScreen, const QVector2D& screenSize ) :
          parent( _parent ),
          screenAjust(
            1.0f / ( ( screenSize.x() - 1.0f ) * 0.5f ),
            1.0f / ( ( screenSize.y() - 1.0f ) * 0.5f )
          ),
          originalRotation( parent.model.rotation ),
          trackStartOnSphere( matToSphere( newTrackFromScreen ) )
        {}

       public:
        inline void move( const QVector2D& otherTrackFromScreen )
        {
          // assuming IEEE-754(float), which i believe has max precision of 7 bits
          static constexpr float epsilon( 1.e-5f );

          //
          const QVector3D trackMoveOnSphere( matToSphere( otherTrackFromScreen ) );

          //Compute the vector perpendicular to the begin and end vectors
          const QVector3D perpendicular( QVector3D::crossProduct( trackStartOnSphere, trackMoveOnSphere ) );

          //Compute the length of the perpendicular vector
          //if its non-zero
          if( perpendicular.length() > epsilon )
          {
            const QQuaternion rot(
              QVector3D::dotProduct( trackStartOnSphere, trackMoveOnSphere ),
              perpendicular.x(),
              perpendicular.y(),
              perpendicular.z()
            );

            //
            parent.model.rotation = rot * originalRotation;
          }
          else                                    //if its zero
          { //The begin and end vectors coincide, so return an identity transform
            //          parent.model.rotation
            //            NewRot->s.X =
            //              NewRot->s.Y =
            //                NewRot->s.Z =
            //                  NewRot->s.W = 0.0f;
          }

          // Update rotation
          parent.model.compute();
        }



      };

     public:
      ModelQuaternion& model;

     public:
      inline TrackBall( ModelQuaternion& _model ) :
        model( _model )
      {}

     public:
      inline Tracker newTrackPositionFromScreen( const QVector2D& newTrackFromScreen, const QVector2D& screenSize )
      { return Tracker( *this, newTrackFromScreen, screenSize ); }

    };

  }
}

