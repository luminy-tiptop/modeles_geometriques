#pragma once

#include <Matrices/AbstractMatrice.hpp>

namespace Matrices {

  struct Projection : public AbstractMatriceObject<Projection>
  {
   public:
    float aspect;
    float fov;      /**< @todo : fov for perspective only ?  */
    float zoom;     /**< @todo : zoom for ortho only ?       */
    bool ortho_mode;

   public:
    float znear, zfar;

   public:
    inline Projection() {reset();}
    inline Projection( const float _aspect, const float _fov, const float _zoom, const bool _ortho_mode, const float _znear, const float _zfar ) :
      aspect( std::move( _aspect ) ), fov( std::move( _fov ) ), zoom( std::move( _zoom ) ),
      ortho_mode( std::move( _ortho_mode ) ),
      znear( std::move( _znear ) ), zfar( std::move( _zfar ) )
    { compute(); }

   public:
    inline const mat4x4_t & compute() override {
      m.setToIdentity();
      if ( ortho_mode )
        m.ortho(
          -zoom * aspect, zoom * aspect,
          -zoom, zoom,
          znear, zfar
        );
      else
        m.perspective(
          fov,
          aspect,
          znear, zfar
        );
      return m;
    }

   public:
    inline const Projection & internalDefault() const override {
      static const Projection defaultProjection { 4.f / 3.f, 45.f, 5.f, false, 0.001f, 1000.f };
      return defaultProjection;
    }
  };

}

