#pragma once

#include <Matrices/AbstractMatrice.hpp>

#include <QVector3D>

namespace Matrices {

  struct ViewLookAt : public AbstractMatriceObject<ViewLookAt>
  {
   public:
    QVector3D position;  // La caméra est à ..., dans l'espace monde
    QVector3D direction; // et regarde l'origine
    QVector3D angle;     // La tête est vers le haut (utilisez 0,-1,0 pour regarder à l'envers)

   public:
    inline ViewLookAt() {reset();}
    inline ViewLookAt( const QVector3D _position, const QVector3D _direction, const QVector3D _angle ) :
      position( std::move( _position ) ), direction( std::move( _direction ) ), angle( std::move( _angle ) )
    { compute(); }

   public:
    inline const mat4x4_t & compute() override {
      m.setToIdentity();
      m.lookAt( position, direction, angle );
      return m;
    }

   public:
    inline const ViewLookAt & internalDefault() const override {
      static const ViewLookAt defaultView { { 0.f, 0.f, 8.f }, { 0.f, 0.f, 0.f }, { 0.f, 1.f, 0.f } };
      return defaultView;
    }
  };


}

