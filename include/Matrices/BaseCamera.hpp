#pragma once

#include <Matrices/ComposedMatrices.hpp>

namespace Matrices {

  struct BaseCamera : public ComposedMatrices
  {
   public:
    AbstractMatrice& projection;
    AbstractMatrice& view;

   public:
    BaseCamera( AbstractMatrice& _projection, AbstractMatrice& _view ) :
      ComposedMatrices( _projection, _view ),
      projection( _projection ),
      view( _view )
    {}

   public:
    virtual ~BaseCamera() = default;

  };

}
