#pragma once

#include <Matrices/AbstractMatrice.hpp>

#include <vector>

namespace Matrices {

  struct ComposedMatrices : AbstractMatrice
  {
   public:
    using matrices_p_t = std::vector<AbstractMatrice *>;
    const matrices_p_t matrices_p;

   protected:
    inline void unpack( matrices_p_t & ms, AbstractMatrice & matrice )
    { ms.emplace_back( &matrice ); }

    template<typename ... TArgs>
    inline void unpack( matrices_p_t & ms, AbstractMatrice & matrice, TArgs & ... Matrices )
    { unpack( ms, matrice ); unpack( ms, Matrices... ); }

   protected:
    template<typename ... TArgs>
    matrices_p_t unargs( TArgs & ... Matrices ) {
      matrices_p_t matrices_p;
      unpack( matrices_p, Matrices... );
      return matrices_p;
    }

   public:
    template<typename ... TArgs>
    ComposedMatrices( TArgs & ... Matrices ) :
      matrices_p( unargs( Matrices... ) )
    {compute();}

   public:
    inline void compute_only_submatrices() {
      for ( AbstractMatrice * matrice_p : matrices_p )
        matrice_p->compute();
    }

    inline mat4x4_t & compute_only_composition() {
      m.setToIdentity();
      for ( AbstractMatrice * matrice_p : matrices_p )
        m *= matrice_p->m;
      return m;
    }

   public:
    inline const mat4x4_t & compute() override {
      compute_only_submatrices();
      compute_only_composition();
      return m;
    }

    inline const mat4x4_t & reset() override {
      m.setToIdentity();
      for ( AbstractMatrice * matrice_p : matrices_p ) {
        matrice_p->reset();
        m *= matrice_p->m;
      }
      return m;
    }

  };

}
