#
# ModelesGeometriques Project
# GIG 2018 Team

#
##
QT       += core gui widgets
QT       += opengl

CONFIG += c++14
CONFIG += object_parallel_to_source


#
##
TARGET = ModelesGeometriques
TEMPLATE = app

#
##
QMAKE_CXXFLAGS += $$(CXXFLAGS)
QMAKE_CFLAGS   += $$(CFLAGS)
QMAKE_LFLAGS   += $$(LDFLAGS)

#
## for release only :
CONFIG(release, debug|release) {
  QMAKE_CXXFLAGS += $$(CXXFLAGS) -O3 -fno-builtin
  QMAKE_CFLAGS   += $$(CFLAGS)   -O3 -fno-builtin
  QMAKE_LFLAGS   += $$(LDFLAGS)  -s
}

#
##
LIBS += -lOpenMeshCore

#
##
INCLUDEPATH += include/

#
##
HEADERS += $$files(include/*.h, true)
HEADERS += $$files(include/*.hpp, true)
SOURCES += $$files(src/*.c, true)
SOURCES += $$files(src/*.cpp, true)
RESOURCES += glsl/glsl.qrc mesh/mesh.qrc icons/icons.qrc
FORMS += $$files(ui/*.ui, true)






