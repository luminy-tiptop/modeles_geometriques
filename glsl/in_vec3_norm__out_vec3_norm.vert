#version 330 core

layout( location = 0 ) in vec3 vertex_from_buffer;
layout( location = 1 ) in vec3 normal_from_buffer;

uniform mat4 uniform_mvp;

out vec3 vertex;
out vec3 normal;

void main()
{
  vertex = vertex_from_buffer;
  normal = normal_from_buffer;
  gl_Position = uniform_mvp * vec4( vertex_from_buffer, 1.f );
}
