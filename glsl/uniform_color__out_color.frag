#version 330 core

uniform int uniform_mode_wireframe = 0;
uniform vec4 uniform_color = vec4( 0.1f, 1.f, 0.f, 1.f );

out vec4 color;

void main()
{
  if ( uniform_mode_wireframe == 0 ) // use uniform_color
    color = uniform_color;
  else // just color
    color = vec4( 0.f, 0.f, 1.f, 1.f );

}
