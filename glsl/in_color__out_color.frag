#version 330 core

uniform int uniform_mode_wireframe = 0;

in vec3 vert_color;

out vec4 color;

void main()
{
  if ( uniform_mode_wireframe == 0 ) // use uniform_color
    color = vec4( vert_color, 1.f );
  else // just color
    color = vec4( 1.f, 0.f, 0.f, 1.f );

}
