#version 330 core

layout( location = 0 ) in vec3 vertex_from_construction_buffer;
//layout( location = 1 ) in int attrib_from_construction_buffer;
layout( location = 1 ) in float attrib_from_construction_buffer;

// layout( location = 2 ) in vec2 position_from_placement_buffer;
// layout( location = 3 ) in float angle_from_placement_buffer;


uniform mat4 uniform_mvp;

uniform vec2 uniform_position;
uniform float uniform_angle;

//flat out int attrib;
out float attrib;


mat2 rotate2D( float a )
{
  return mat2(
    cos( a ), -sin( a ),
    sin( a ), cos( a )
  );
}

void main()
{
  /** @todo fix it by attrib divisor */
  vec2 position_from_placement_buffer = uniform_position;
  float angle_from_placement_buffer = uniform_angle;

  //
  //attrib = float(attrib_from_construction_buffer);
  attrib = attrib_from_construction_buffer;

  //
  vec3 v = vertex_from_construction_buffer;
  // v.xz = rotate2D( angle_from_placement_buffer ) * v.xz;
  v.xz += position_from_placement_buffer.xy;

  //
  gl_Position = uniform_mvp * vec4( v, 1.f );
}
