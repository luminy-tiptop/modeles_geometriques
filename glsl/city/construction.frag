#version 330 core

#define Hex2Vex3( hex ) vec3( ( float( ( hex / 256 / 256 ) % 256 ) / 256.f ), ( float( ( hex / 256 ) % 256 ) / 256.f ), ( float( ( hex ) % 256 ) / 256.f ) )


uniform int uniform_mode_wireframe = 0;

const vec3 attrib_colors[6] = vec3[] (
  Hex2Vex3( 0x320E15 ), // Ground
  Hex2Vex3( 0xB7CA79 ), // Grass
  Hex2Vex3( 0x5EB6DD ), // Water
  Hex2Vex3( 0xDCDCDC ), // BuildingGrey1
  Hex2Vex3( 0xFFF5EF ), // BuildingGrey2
  Hex2Vex3( 0xE83C1A )  // BuildingRoof
);

//flat in int attrib;
in float attrib;

out vec4 color;

void main()
{
  //int attrib_i = attrib;
  int attrib_i = int(floor( attrib + 0.1f ) );
  if( ( attrib_i<0 ) || ( attrib_i>5 ) )
    attrib_i = 2;

  // use colors
  //if( uniform_mode_wireframe == 0 )

    color = vec4( attrib_colors[attrib_i], 1.f );

  //// just color
  //else
  //  color = vec4( 0.f, 0.f, 1.f, 1.f );

}


