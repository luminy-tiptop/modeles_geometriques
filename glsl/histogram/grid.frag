#version 330 core

in vec2 uv;
out vec4 color;

void main()
{
  color = vec4( uv.x, uv.y, sqrt( uv.x * uv.y ), 1.f );
}
