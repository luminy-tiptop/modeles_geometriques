#ifndef LANDGEN_H
#define LANDGEN_H

#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <random>
#include <chrono>
#include <thread>
#include <opencv2/opencv.hpp>


class landGen
{
public:
    landGen(int h, double coef);
    double** diamantcarre();


    double moyenne_diamant(int x, int y, int d, int gen);
    double moyenne_carre(int x, int y, int d, int gen);

    int geth();
    double ** gettab();



private:
    double ** tab;
    double lissage;
    int h;
    //test : rectif alex
    double mmin, mmax;

};

#endif // LANDGEN_H
