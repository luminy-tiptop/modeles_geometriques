#ifndef ROADGEN_H
#define ROADGEN_H

#include "math.h"
#include <iostream>
#include <random>
#include <chrono>
#include <thread>
struct noeud
{
    int xpos, ypos, zpos;
    float distance[8];
};

class roadGen
{
public:
    roadGen(double** tableau, int h, int pas);
    void arbrecouvrant();
    float calcDist(int xa, int xb, int ya, int yb, int za, int zb);
    void gridGen();
    double ** gettab();
private:
    int h, pas, tailletab;
    double ** tab;
    double ** tabcop;
};

#endif // ROADGEN_H
