#include "affichage.h"

Affichage::Affichage(int h)
{
    this->h = h;
    mmin = h;
    mmax = -h;
    mimg = cv::Mat(h, h, CV_8UC3);
}



void Affichage::remplirImg(double ** tab){

        for (int i = 0 ; i < h ; i++){
            for(int j = 0 ; j < h ; j++){
                if(tab[i][j] < mmin){
                    mmin = tab[i][j];
                }
                if(tab[i][j] > mmax){
                    mmax = tab[i][j];
                }
            }
        }

    //*

    double temp;
    for (int i = 0 ; i < h ; i++){
        for (int j = 0 ; j < h ; j++){
            //std::cout << "tab(" << i/zoom << "|" << j/zoom << ") = " << tab[i/zoom][j/zoom] << std::endl;
            //temp =int(std::round(float( tab[i/zoom][j/zoom] * 255)/float(h)));
            temp = (tab[i][j] - mmin) * 255 / (mmax - mmin);
            mimg.at<cv::Vec3b>(j,i)[0] = int(std::round(temp));
            mimg.at<cv::Vec3b>(j,i)[1] = int(std::round(temp));
            mimg.at<cv::Vec3b>(j,i)[2] = int(std::round(temp));
        }
    }
    /*/
    double temp;
    double temp2;
    for(int i = 0 ; i < h ; i+=h-1)
        for (int j = 0; j < h ; j+=h-1) {
            temp = (tab[i][j] - mmin) * 255 / (mmax - mmin);

            for(int x = 0; x < zoom; x++)
                for(int y = 0; y < zoom; y++) {
                    img.at<cv::Vec3b>(zoom*j+y,zoom*i+x)[0] = 0;
                    img.at<cv::Vec3b>(zoom*j+y,zoom*i+x)[1] = 0;
                    img.at<cv::Vec3b>(zoom*j+y,zoom*i+x)[2] = int(std::round(temp));
                }
        }

    int cpt = 0;
    for(int d = h-1; d > 1; d>>=1) {
        for(int i = d/2 ; i < h ; i+=d)
            for (int j = d/2 ; j < h ; j+=d) {
                temp = (tab[i][j] - mmin) * 255 / (mmax - mmin);

                for(int x = 0; x < zoom; x++)
                    for(int y = 0; y < zoom; y++) {
                        img.at<cv::Vec3b>(zoom*j+y,zoom*i+x)[0] = 0;
                        img.at<cv::Vec3b>(zoom*j+y,zoom*i+x)[1] = 0;
                        img.at<cv::Vec3b>(zoom*j+y,zoom*i+x)[2] = int(std::round(temp));
                    }
            }
        for (int i = 0 ; i < h ; i += d)
            for (int j = d/2 ; j < h ; j += d) {
                temp = (tab[i][j] - mmin) * 255 / (mmax - mmin);
                temp2 = (tab[j][i] - mmin) * 255 / (mmax - mmin);

                for(int x = 0; x < zoom; x++)
                    for(int y = 0; y < zoom; y++) {
                        img.at<cv::Vec3b>(zoom*j+y,zoom*i+x)[0] = 0;
                        img.at<cv::Vec3b>(zoom*j+y,zoom*i+x)[1] = int(std::round(temp));
                        img.at<cv::Vec3b>(zoom*j+y,zoom*i+x)[2] = 0;

                        img.at<cv::Vec3b>(zoom*i+y,zoom*j+x)[0] = 0;
                        img.at<cv::Vec3b>(zoom*i+y,zoom*j+x)[1] = int(std::round(temp2));
                        img.at<cv::Vec3b>(zoom*i+y,zoom*j+x)[2] = 0;
                    }
            }
    }
    //*/
    return;
}

void Affichage::creerCarte(){
    cv::Mat grad;
    grad = cv::imread("gradient.png", cv::IMREAD_COLOR);

    int valeur;
    for (int j = 0 ; j < h ; j++){
        for (int i = 0 ; i < h ; i++){
            valeur = mimg.at<cv::Vec3b>(j,i)[0];
            mimg.at<cv::Vec3b>(j,i)[0] = grad.at<cv::Vec3b>(0, valeur)[0];
            mimg.at<cv::Vec3b>(j,i)[1] = grad.at<cv::Vec3b>(0, valeur)[1];
            mimg.at<cv::Vec3b>(j,i)[2] = grad.at<cv::Vec3b>(0, valeur)[2];
        }
    }
    return;
}

cv::Mat Affichage::getImg(){
    return mimg;
}
