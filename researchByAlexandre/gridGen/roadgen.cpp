
#include "roadgen.h"

roadGen::roadGen(double** tableau, int h, int pas)
{
    this->h = h;
    this->pas = pas;
    tailletab = h-1/pas-1;
    tab = tableau;
    tabcop = new double*[h];
    for(int i = 0; i < h; i++)
    {
        tabcop[i] = new double[h];
        for (int j = 0 ; j < h ; j++){
            tabcop[i][j] = 0;
        }
    }
}

void roadGen::arbrecouvrant(){

    noeud arbre[tailletab][tailletab];
    for (int x = 1 ; x <= tailletab ; x +=1){
        for (int y = 1 ; y <= tailletab ; y +=1){
            //arbre[x][y].xpos = moyennex(x*pas, y*pas, pas);
            //arbre[x][y].ypos = moyenney(x*pas, y*pas, pas);
            //arbre[x][y].zpos = moyennez(x*pas, y*pas, pas);

            //std::cout << "sur tableau en valeur : " << x/pas << "|" << y/pas << std::endl;
            //arbre[x-1][y-1].xpos = ;
            //arbre[x-1][y-1].ypos = ;
            //arbre[x-1][y-1].zpos = ;
        }
    }

//    //std::cout << "bornes : " << h/pas +1 << "|" << h/pas +1 << std::endl;
//    noeud arbre[h/pas][h/pas];

//    for (int x = 0 ; x < h ; x +=pas){
//        for (int y = 0 ; y < h ; y +=pas){
//            //std::cout << "sur tableau en valeur : " << x/pas << "|" << y/pas << std::endl;
//            arbre[x/pas][y/pas].xpos = x;
//            arbre[x/pas][y/pas].ypos = y;
//            arbre[x/pas][y/pas].zpos = tab[x][y];
//        }
//    }

//    for (int i = 0 ; i < h/pas ; i++){
//        for (int j = 0 ; j < h/pas ; j++){
//            for (int v = 0 ; v < 8 ; v++ ){
//                int voisinx = i + voisins[v][0];
//                int voisiny = j + voisins[v][1];
//                if (voisinx > 0 && voisiny > 0)
//                    arbre[i][j].distance[v] = calcDist(arbre[i][j].xpos, arbre[voisinx][voisiny].xpos,
//                                                       arbre[i][j].ypos, arbre[voisinx][voisiny].ypos,
//                                                       arbre[i][j].zpos, arbre[voisinx][voisiny].zpos);
//            }
//        }
//    }
//    for (int i = 0 ; i < h ; i++){
//        for (int j = 0 ; j < h ; j++){
//            std::cout << "distances de (" << arbre[i][j].xpos << ", " << arbre[i][j].ypos << ") : ";

//            for (int v = 0 ; v < 8 ; v++){
//                std::cout << arbre[i][j].distance[v] << "|";
//            }
//            //std::cout << std::endl;
//        }
//    }
//    return;
}
float roadGen::calcDist(int xa, int xb, int ya, int yb, int za, int zb){
    return (sqrt((xb-xa)*(xb-xa) +
                 (yb-ya)*(yb-ya) +
                 (zb-za)*(zb-za)));
}

void roadGen::gridGen(){
    double mmin = h;
    double mmax = h;
    auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed1);
    std::uniform_int_distribution<int> dis((h/4), (h/4)*3);
    int centre_x = dis(gen);
    int centre_y = dis(gen);
    int pasmodifx = pas;
    int pasmcount_x = 1;
    int pasmodify = pas;
    int pasmcount_y = 1;

    std::cout << "centre_x = " << centre_x << ", centre_y = " << centre_y << std::endl;

    for(int x = 0 ; x < h ; x++){
        for (int y = 0 ; y < h ; y++){
            if (tab[x][y] < mmin){
                mmin = tab[x][y];
            }
            if (tab[x][y] > mmax){
                mmax = tab[x][y];
            }
        }
    }
    int temp;
    int flag = 0;
    for (int x = 1 ; x < h ; x += pasmodifx){
        for (int y = 1 ; y < h ; y ++){
            temp = int ((tab[x][y] - mmin) * 255 / (mmax - mmin));
            //std::cout << "coincoin" << std::endl;
            if (temp > 130 && temp < 190 && (x-1) >= 0 && (x+1) <h && (y-1) >= 0 && (y+1) < h){
                tabcop[x][y] = 1;
                //tabcop[x+1][y] = 1;
                //tabcop[x+1][y+1] = 1;
                //tabcop[x][y+1] = 1;
                //tabcop[x-1][y+1] = 1;
                //tabcop[x-1][y] = 1;
                //tabcop[x-1][y-1] = 1;
                //std::cout << "tabcop en ("<< x << "|" << y << ") tab[x][y]("<< temp << ")" << std::endl;
            }
        }
        if (x < centre_x && x > centre_x - 250){
            //std::cout << ("x inférieur a centre_x") << std::endl;
            //std::cout <<"le pas est de : " << pasmodifx << std::endl;
            pasmcount_x ++;
            temp = pasmodifx - pasmodifx/pasmcount_x;
            if(temp == pasmodifx && temp > 4){
                pasmodifx --;
                std::cout << "pasmodif == temp ==" << temp << std::endl;
            }
            else{
                pasmodifx = temp;
            }
            std::cout <<"le pas est maintenant de : " << pasmodifx << std::endl;
        }
        if (x>centre_x && flag == 0){
            pasmcount_x = 1;
            flag = 1;
        }
        if (x > centre_x && x <= centre_x +250){
            std::cout << ("x supérieur a centre_x") << std::endl;
            pasmcount_x ++;
            temp = pasmodifx + pasmodifx/pasmcount_x;
            if(temp> pas){
                temp = pas;
            }
            pasmodifx = temp;
            std::cout <<"le pas est maintenant de : " << pasmodifx << std::endl;
        }
        if (x > centre_x + 250){
            pasmcount_x = pas;
        }
    }
    for (int y = 1 ; y < h ; y += pasmodify){
        for (int x = 1 ; x < h ; x ++){
            temp = int ((tab[x][y] - mmin) * 255 / (mmax - mmin));
            //std::cout << "coincoin" << std::endl;
            if (temp > 130 && temp < 190 && (y-1) >= 0 && (y+1) <h && (y-1) >= 0 && (y+1) < h){
                tabcop[x][y] = 1;
                //tabcop[y+1][y] = 1;
                //tabcop[y+1][y+1] = 1;
                //tabcop[y][y+1] = 1;
                //tabcop[y-1][y+1] = 1;
                //tabcop[y-1][y] = 1;
                //tabcop[y-1][y-1] = 1;
                //std::cout << "tabcop en ("<< y << "|" << y << ") tab[y][y]("<< temp << ")" << std::endl;
            }
        }
        if (y < centre_y && y > centre_y - 250){
            //std::cout << ("y inférieur a centre_y") << std::endl;
            //std::cout <<"le pas est de : " << pasmodify << std::endl;
            pasmcount_y ++;
            temp = pasmodify - pasmodify/pasmcount_y;
            if(temp == pasmodify && temp > 3){
                pasmodify --;
                std::cout << "pasmodif == temp ==" << temp << std::endl;
            }
            else{
                pasmodify = temp;
            }
            std::cout <<"le pas est maintenant de : " << pasmodify << std::endl;
        }
        if (y>centre_y && flag == 0){
            pasmcount_y = 1;
            flag = 1;
        }
        if (y > centre_y && y <= centre_y +250){
            std::cout << ("y supérieur a centre_y") << std::endl;
            pasmcount_y ++;
            temp = pasmodify + pasmodify/pasmcount_y;
            if(temp> pas){
                temp = pas;
            }
            pasmodify = temp;
            std::cout <<"le pas est maintenant de : " << pasmodify << std::endl;
        }
        if (y > centre_y + 250){
            pasmcount_y = pas;
        }
    }
//    for (int y = 1 ; y < h ; y += pasmodify){
//        for (int x = 1 ; x < h ; x ++){
//            temp = int ((tab[x][y] - mmin) * 255 / (mmax - mmin));
//            //std::cout << "coincoin" << std::endl;
//            if (temp > 130 && temp < 190 && (x-1) >= 0 && (x+1) <h && (y-1) >= 0 && (y+1) < h){
//                tabcop[x][y] = 1;
//                tabcop[x+1][y] = 1;
//                tabcop[x+1][y+1] = 1;
//                tabcop[x][y+1] = 1;
//                tabcop[x-1][y+1] = 1;
//                tabcop[x-1][y] = 1;
//                tabcop[x-1][y-1] = 1;
//                //std::cout << "tabcop en ("<< x << "|" << y << ") tab[x][y]("<< temp << ")" << std::endl;
//            }
//        }
//        if (y <= centre_y){
//            pasmcount_y ++;
//            pasmodify -= (pasmodify/10) * pasmcount_y;
//        }
//        if (y > centre_y){
//            pasmcount_y --;
//            pasmodify += (pasmodify/10) * pasmcount_y;
//        }
//    }

}

double ** roadGen::gettab(){
    return tabcop;
}

