/*Enregistrement d'une image

  g++ -Wall ex02-imwrite.cpp -lopencv_highgui -lopencv_core -o ex02-imwrite
  ./ex02-imwrite tux1.png tmp1.png
*/

#include <iostream>
#include "landgen.h"
#include "roadgen.h"
#include "affichage.h"
#include <opencv2/opencv.hpp>

int main (int argc, char**argv)
{
    if (argc-1 != 1) {
        std::cout << "Usage: " << argv[0] << " size h" << std::endl;
        return 1;
    }
    double coef = 1;
    landGen diamsq(atoi(argv[1]), coef);
    Affichage affichage(atoi(argv[1]));
    Affichage carte(atoi(argv[1]));


    diamsq.diamantcarre();
    roadGen grille(diamsq.gettab(), atoi(argv[1]), 150);
    grille.gridGen();
    std::cout << "coincoin" << std::endl;
    carte.remplirImg(diamsq.gettab());
    affichage.remplirImg(grille.gettab());


    carte.creerCarte();

    cv::namedWindow ("carte", cv::WINDOW_AUTOSIZE);
    cv::namedWindow ("route", cv::WINDOW_AUTOSIZE);
    cv::imshow ("carte", carte.getImg());
    cv::imshow ("route", affichage.getImg());
    std::cout << "Pressez une touche dans l'image..." << std::endl;
    cv::waitKey (0);
    cv::destroyWindow ("route");
    cv::destroyWindow ("carte");

    if (! cv::imwrite ("coin.png", affichage.getImg()))
         std::cout << "Erreur d'enregistrement" << std::endl;
    else std::cout << "Enregistrement effectuÃ©" << std::endl;

    if (! cv::imwrite ("coin2.png", carte.getImg()))
        std::cout << "Erreur d'enregistrement" << std::endl;
    else std::cout << "Enregistrement effectuÃ©" << std::endl;

    return 0;
}
