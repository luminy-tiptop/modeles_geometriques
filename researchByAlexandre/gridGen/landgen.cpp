#include "landgen.h"


landGen::landGen(int h, double coef ){
    this->h = h;

    lissage = coef;

    tab = new double*[h];
    for(int i = 0; i < h; i++)
    {
        tab[i] = new double[h];
    }
}
double landGen::moyenne_carre(int x, int y, int d, int gen){

    d >>= 1;

    double moyenne = (tab[x-d][y-d] + tab[x-d][y+d] + tab[x+d][y+d] + tab[x+d][y-d])/4;
    

    return moyenne+ (gen * lissage);
}

double landGen::moyenne_diamant(int x, int y, int d, int gen){

    double somme = 0;
    int n = 0;

    d >>= 1;

    if(x - d >= 0) {
        somme += tab[x-d][y];
        n ++;
    }
    if(x + d < h) {
        somme += tab[x+d][y];
        n++;
    }
    if (y - d >= 0) {
        somme += tab[x][y-d];
        n++;
    }
    if (y + d < h) {
        somme += tab[x][y + d];
        n++;
    }
    return somme / n+ (gen * lissage);
}
double** landGen::diamantcarre(){
    auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed1);
    std::uniform_int_distribution<int> dis(-h, h);

    std::cout << "génération ... " << std::endl;

     tab[0][0] = dis(gen);
     tab[0][h-1] = dis(gen);
     tab[h-1][h-1] = dis(gen);
     tab[h-1][0] = dis(gen);

    for(int d = h-1; d > 1; d>>=1) {
        std::uniform_int_distribution<int> dis2(-d, d);
        /* CARRE */
        for(int x = d/2 ; x < h ; x+=d)
            for (int y = d/2 ; y < h ; y+=d) {
                tab[x][y]= moyenne_carre(x, y, d, dis2(gen));
            }
        /* DIAMANT */
        for (int x = 0 ; x < h ; x += d)
            for (int y = d/2 ; y < h ; y += d) {
                tab[x][y] = moyenne_diamant(x, y, d, dis2(gen));
                tab[y][x] = moyenne_diamant(y, x, d, dis2(gen));
            }
    }

    std::cout << "done" << std::endl;
    return tab;
}

int landGen::geth(){
    return h;
}
double ** landGen::gettab(){
    return tab;
}
