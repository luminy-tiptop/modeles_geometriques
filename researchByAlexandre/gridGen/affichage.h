#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include <opencv2/opencv.hpp>

class Affichage
{
public:
    Affichage(int h);
    void remplirImg(double** tab);
    void creerCarte();

    cv::Mat getImg();
private:
    cv::Mat mimg;
    double mmin, mmax;
    int h;
};

#endif // AFFICHAGE_H
