var renderer, scene, camera, mesh, light, cube, objet3D;
var time = 0;

var anim = 0;


init();
animate();
window.onresize = onWindowResize;

function init(){

    // si WebGL ne fonctionne pas sur votre navigateur vous pouvez utiliser le moteur de rendu Canvas à la place
    // renderer = new THREE.CanvasRenderer()
    scene = new THREE.Scene();
    scene.background = new THREE.Color("#202020");

    light = buildLights(scene);
    camera = buildCamera(window.innerWidth, window.innerHeight);
    renderer = buildRender(document.getElementById('container').clientWidth , document.getElementById('container').clientHeight);

    // controls
    var controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.screenSpacePanning = false;
    controls.minDistance = 10;
    controls.maxDistance = 1000;
    controls.maxPolarAngle = Math.PI / 2;


    mesh = addObjects(scene);

    document.getElementById('container').appendChild(renderer.domElement);


    window.addEventListener('mousedown', function(event){allumerlefeu(event);}, true);

}

function buildLights(scene) {
    var light = new THREE.SpotLight("#fff", 0.8);
    light.position.y = 30;
    light.position.z = -60;

    light.angle = 1.05;

    light.decacy = 2;
    light.penumbra = 1;

    light.shadow.camera.near = 0.1;
    light.shadow.camera.far = 1000;
    light.shadow.camera.fov = 30;


    light.castShadow = true;

    //var helper = new THREE.CameraHelper( light.shadow.camera );
    //scene.add( helper );

    scene.add(light);
    var ambientLight = new THREE.AmbientLight( 0xcccccc, 0.3 );
    scene.add( ambientLight );

    return light;
}

function buildCamera(width, height) {
    var aspectRatio = width / height;
    var fieldOfView = 60;
    var nearPlane = 10;
    var farPlane = 5000;
    var camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);

    camera.position.z = 100;

    return camera;
}

function buildRender(width, height) {
    var renderer = new THREE.WebGLRenderer({antialias: true, alpha: true });
    var DPR = (window.devicePixelRatio) ? window.devicePixelRatio : 1;
    renderer.setPixelRatio(DPR);
    renderer.setSize(width, height);
    //renderer.shadowMapEnabled = true;
    //renderer.shadowMapType = THREE.PCFSoftShadowMap;

    renderer.gammaInput = true;
    renderer.gammaOutput = true;

    return renderer;
}

function addObjects(scene) {
  // Loader d'obj
  var loader = new THREE.OBJLoader();
  loader.load(
  	// resource URL
  	'./obj/hd.obj',
  	// called when resource is loaded
  	function (obj) {
  		objet3D = obj;
  		obj.position.y = 0;
  		obj.scale.set(40,40,40);
  		obj.castShadow = true;
  		objet3D = obj;
  		scene.add( objet3D );

  		console.log('on a ajouté : ' + objet3D);

  	},
  	// called when loading is in progresses
  	function (xhr) {//xhr représente l'ensemble d'objets 3D a loader
  		console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );//NaN% implique qu'on en a qu'un -> parfait!
  	},
  	// called when loading has errors
  	function ( error ) {

  		console.log( 'le putain d\'obj ' );
  	}
  );

  // var coin = new THREE.CubeGeometry(30, 30, 30);
  // var material = new THREE.MeshStandardMaterial({ color: "#000", roughness: 0.6 });
  // cube = new THREE.Mesh(coin, material);
  // cube.position.y = 70;
  // cube.position.z = -60;
  // scene.add(cube);


  var geometry2 = new THREE.PlaneGeometry( 200, 200, 32 );
  var material2 = new THREE.MeshStandardMaterial( {color: 0x252525, side: THREE.DoubleSide, roughness:0.8} );
  var plane = new THREE.Mesh( geometry2, material2 );
  plane.rotation.x = 3.14/2;
  plane.position.y = -30;
  plane.receiveShadow = true; //default
  scene.add(plane);
}

function animate(){
    if(anim == 1){
    time ++;

    var deplacement = 3*Math.cos(time*0.04);
    var deplacementB = 3*Math.sin(time*0.04);
    light.position.x += deplacement;
    light.position.z += deplacementB;
//    cube.position.x += deplacement;
//    cube.position.z += deplacementB;
    }

    //test click
    renderer.render(scene, camera );
    requestAnimationFrame( animate );
}
function allumerlefeu( event ) {
        event.preventDefault();
        var mouse3D = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1,
                                -( event.clientY / window.innerHeight ) * 2 + 1,
                                0.5 );
        var raycaster =  new THREE.Raycaster();
        raycaster.setFromCamera( mouse3D, camera );
        //console.log(objet3D.children[0]);

        var intersects = raycaster.intersectObject(objet3D.children[0]);

        if ( intersects.length > 0 ) {
          intersects[0].face.color.setHex( 0xDDC2A3);
          objet3D.children[0].geometry.colorsNeedUpdate = true;
            //intersects[0].face.color.set(0xff0000);
            console.log(intersects[0]);
        }
  }

function finit() {
  document.getElementById("objchrg").innerHTML = objet3D.materialLibraries[0] + " chargé";
}
function getinfo() {
  var geometry = new THREE.Geometry().fromBufferGeometry( objet3D.children[0].geometry );
   geometry.computeFaceNormals();
   geometry.mergeVertices();
   geometry.computeVertexNormals();
  // coin = objet3D.children[0].geometry.attributes.normal.count/objet3D.children[0].geometry.attributes.normal.itemSize;
  var verts = geometry.vertices.length;
  var faces = geometry.faces.length;
  document.getElementById("infos").innerHTML = verts + " vertices </br>" + faces + " faces </br>";
}

function launchanim(){
  if (anim == 0) {
    anim = 1;
    document.getElementById("im2").innerHTML = "arret anim";
  }
  else if (anim == 1 ){
    anim = 0;
    document.getElementById("im2").innerHTML = "animer";
  }
}

function changemod(){
  finit();
  document.getElementById("infos").innerHTML = "done";
}

function calculangle(){
  var geometry = new THREE.Geometry().fromBufferGeometry( objet3D.children[0].geometry );
   geometry.computeFaceNormals();
   geometry.mergeVertices();
   geometry.computeVertexNormals();

}

function onWindowResize() {

		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();

		renderer.setSize(window.innerWidth, window.innerHeight);

	}
