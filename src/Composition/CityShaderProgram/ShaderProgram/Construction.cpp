#include <Composition/CityShaderProgram/ShaderProgram/Construction.hpp>

#include <debug.hpp>

namespace Composition {
  namespace CityShaderProgram {


    Construction::Construction()
    {
      mVerticesBuffer.setUsagePattern( QOpenGLBuffer::UsagePattern::DynamicDraw );
      mIndicesBuffer.setUsagePattern( QOpenGLBuffer::UsagePattern::DynamicDraw );
    }

    // ---- ----

    void Construction::updateConstructionBuffers()
    {
      {
        const int vertices_sizeof( int(mVertices.size() * sizeof( ConstructionFormat ) ) );
        mVerticesBuffer.bind();
        mVerticesBuffer.allocate( mVertices.data(), vertices_sizeof );
        mVerticesBuffer.release();
      }
      {
        const int triangles_sizeof( int(mTriangles.size() * sizeof( TriangleIndicesFormat ) ) );
        mIndicesBuffer.bind();
        mIndicesBuffer.allocate( mTriangles.data(), triangles_sizeof );
        mIndicesBuffer.release();
      }
      mConstructionChanged = false;
    }

    void Construction::updatePlacementBuffers()
    {

      // count = GLuint( vec.size() );
      // const GLuint vertex_sizeof( count * sizeof( Format ) );
      // mPlacementsBuffer.bind();
      // mPlacementsBuffer.allocate( vec.data(), int(vertex_sizeof) );
      // mPlacementsBuffer.release();

      mPlacementsChanged = false;
    }

    // ---- ----

    void Construction::initialise( QOpenGLFunctions_3_3_Core& gl )
    {
      buildProgram(
        ":/glsl/city/construction.vert",
        ":/glsl/city/construction.frag"
      );
      mVerticesBuffer.create();
      mIndicesBuffer.create();
      vao.create();

      vao.bind();
      gl.glEnableVertexAttribArray( 0 );
      gl.glEnableVertexAttribArray( 1 );
      mIndicesBuffer.bind();
      mVerticesBuffer.bind();

      const int buffer_stride( sizeof( ConstructionFormat ) );
      const void* const vertex_offset( reinterpret_cast<void*>( offsetof( ConstructionFormat, vertex ) ) );
      const void* const attrib_offset( reinterpret_cast<void*>( offsetof( ConstructionFormat, attrib ) ) );

      // glsl : layout( location = 0 ) in vec3 vertex_from_construction_buffer:
      gl.glVertexAttribPointer( 0, 3, GL_FLOAT, false, buffer_stride, vertex_offset );

      // glsl : layout( location = 1 ) in float attrib_from_construction_buffer:
      gl.glVertexAttribPointer( 1, 1, GL_FLOAT, false, buffer_stride, attrib_offset );
      //gl.glVertexAttribPointer( 1, 1, GL_INT, true, buffer_stride, attrib_offset );


      vao.release();
      // After vao release :
      mVerticesBuffer.release();
      mIndicesBuffer.release();
      gl.glDisableVertexAttribArray( 0 );
      gl.glDisableVertexAttribArray( 1 );
      mIsInitialized = true;
    }

    // ---- ----

    void Construction::paint( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp )
    {
      if( mPlacements.empty() || mConstructionsInBuffers.empty() )
        return;

      //
      if( mPlacementsChanged )
        updatePlacementBuffers();
      if( mConstructionChanged )
        updateConstructionBuffers();

      //
      const size_t nbConstruction( mConstructionsInBuffers.size() );


      bind();

      program.setUniformValue( "uniform_mvp", mvp );
      program.setUniformValue( "uniform_color", QVector3D( 1.f, 0.f, 0.f ) );
      program.setUniformValue( "uniform_mode_wireframe", int(0) );

      /** @todo remove it and replace b attrib divisor buffer  */
      for( auto& el : mPlacements )
      {
        program.setUniformValue( "uniform_position", el.position );
        // program.setUniformValue( "uniform_angle", el.angle );

        ElementBuffer& eb( mConstructionsInBuffers[el.constructElementId % nbConstruction] );
        eb.draw( gl );
      }

      release();
    }

    // ---- ---- ---- ----

    void Construction::appendConstruction( const GeneratedMesh_up& genmesh_up )
    {
      //
      if( !genmesh_up )
        throw std::runtime_error( "[Construction::appendConstruction] genmesh_up invalid" );
      mConstructionChanged = true;
      Mesh& mesh( genmesh_up->mesh );

      //
      using namespace MeshAlgorithms::Generation::CityConstruction;

      //
      const size_t nbVertices( mesh.n_vertices() );
      mVertices.reserve( mVertices.size() + nbVertices );
      Mesh::VertexIter vertex_it( mesh.vertices_begin() );
      const Mesh::VertexIter vertex_end_it( mesh.vertices_end() );
      for(; vertex_it != vertex_end_it; ++vertex_it )
      {
        mVertices.emplace_back(
          mesh.point( *vertex_it ),                             // glsl : vec3 vertex_from_buffer;
          VertexProperty::Attrib_getProperty( mesh, vertex_it ) // glsl : int attrib_from_buffer;
        );
      }

      //
      const size_t nbTriangles( mesh.n_faces() );
      const size_t nbIndices( mesh.n_faces() * 3 );
      mTriangles.reserve( mTriangles.size() + nbTriangles );
      Mesh::ConstFaceIter face_it( mesh.faces_begin() );
      const Mesh::ConstFaceIter face_end_it( mesh.faces_end() );
      for(; face_it != face_end_it; ++face_it )
      {
        Mesh::ConstFaceVertexIter face_vertex_it( mesh.cfv_iter( *face_it ) );
        TriangleIndicesFormat t;
        t.i = GLuint( face_vertex_it->idx() );
        ++face_vertex_it;
        t.j = GLuint( face_vertex_it->idx() );
        ++face_vertex_it;
        t.k = GLuint( face_vertex_it->idx() );
        mTriangles.emplace_back( std::move( t ) );
      }

      //
      mConstructionsInBuffers.emplace_back(
        mCountVertices, nbVertices,
        mCountIndices,  nbIndices
      );

      //
      mCountVertices += nbVertices;
      mCountIndices += nbIndices;
    }

    void Construction::placeConstruction( const Math::Vec2f& position, const float angle, const uint constructElementId )
    {
      mConstructionChanged = true;
      mPlacements.emplace_back( position, angle, constructElementId );
    }

    void Construction::clearConstruction()
    {
      mConstructionChanged = true;
      mPlacementsChanged = true;
      mCountVertices = 0;
      mCountIndices = 0;
      mPlacements.clear();
      mConstructionsInBuffers.clear();
      mTriangles.clear();
      mVertices.clear();
    }

    // ---- ----





  }
}
