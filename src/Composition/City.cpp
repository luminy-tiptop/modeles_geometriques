#include <Composition/City.hpp>

#include <MeshAlgorithms/Generation/CityConstruction/Building.hpp>

#include <Gui/Panel_Composition_City.hpp>
#include <Math/Number.hpp>

#include <array>

namespace Composition {

  City::City( Composition::CompositionReceiver& _receiver ) :
    AbstractComposition( "City", _receiver ),
    mAttachedCityWidget( new Gui::Panel_Composition_City( *this ) )
  { }

  City::~City()
  { if( mAttachedCityWidget ) delete mAttachedCityWidget; }

  // ---- ---- ---- ----

  void City::updateGrid()
  {
    //
    if( Math::equal( mGridSpacing, 0.f ) )
      return;
    // throw std::runtime_error( "[City::updateGrid] mGridSpacing need != 0.f" );

    const Math::Vec2f mWorldSize(
      std::abs( mWorldEnd[0] - mWorldStart[0] ),
      std::abs( mWorldEnd[1] - mWorldStart[1] )
    );
    const Math::Vec2ui nbLinePerDirection(
      uint( std::ceil( mWorldSize[0] / mGridSpacing ) ),
      uint( std::ceil( mWorldSize[1] / mGridSpacing ) )
    );

    const uint nbTotalLine( nbLinePerDirection[0] + nbLinePerDirection[1] );
    if( nbTotalLine==0 )
      throw std::runtime_error( "[City::updateGrid] Bad nbTotalLine" );

    //
    std::vector<std::array<Math::Vec3f, 2>> lines;
    lines.reserve( nbTotalLine );

    // Vertical
    for( uint i( 0 ); i<nbLinePerDirection[0]; ++i )
    {
      const float currentPart( mWorldStart[0] + float(i) * mGridSpacing );
      lines.push_back( {{{ currentPart, 0.f, -mWorldStart[1] }, { currentPart, 0.f, +mWorldStart[1] }}} );
    }

    // Horizontal
    for( uint i( 0 ); i<nbLinePerDirection[1]; ++i )
    {
      const float currentPart( mWorldStart[1] + float(i) * mGridSpacing );
      lines.push_back( {{{ -mWorldStart[0], 0.f, currentPart }, { +mWorldStart[0], 0.f, currentPart }}} );
    }

    //
    programblock_basic_grid.send( lines.data()->data(), lines.size() * 2 );
  }


  // ---- ---- ---- ----

  void City::initialiseGL( QOpenGLFunctions_3_3_Core& gl )
  {
    programblock_city_construction.initialise( gl );
    programblock_basic_grid.initialise( gl );
    mOpenglInitialized = true;

    //
    updateGrid();

  }

  void City::paintGL( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp  )
  {
    if( ( programblock_basic_grid.count >0 ) && mPrintGrid )
      programblock_basic_grid.paint( gl, mvp );

    programblock_city_construction.paint( gl, mvp );
  }

  // ---- ---- ---- ----

  QWidget* City::getAttachedWidget()
  { return mAttachedCityWidget; }

  // ---- ---- ---- ----

  void City::reloadLastDemo()
  {
    if( isOneConstructionDemo )
      loadOneConstructionDemo();
    else
      loadDefaultDemo();
  }

  void City::loadOneConstructionDemo()
  {
    using namespace MeshAlgorithms::Generation::CityConstruction;
    programblock_city_construction.clearConstruction();

    isOneConstructionDemo = true;
    Building b;

    programblock_city_construction.appendConstruction( b.generate() );
    programblock_city_construction.appendConstruction( b.generate() );
    programblock_city_construction.appendConstruction( b.generate() );
    programblock_city_construction.appendConstruction( b.generate() );

    programblock_city_construction.placeConstruction( { 0.f, 0.f }, 0.f, 0 );
    programblock_city_construction.placeConstruction( { 5.f, 0.f }, 0.f, 1 );
    programblock_city_construction.placeConstruction( { 0.f, 5.f }, 0.f, 2 );
  }

  void City::loadDefaultDemo()
  {
    using namespace MeshAlgorithms::Generation::CityConstruction;
    programblock_city_construction.clearConstruction();

    isOneConstructionDemo = false;
    Building b;

    if( mNbConstruction == 0 || mNbPlace == 0 )
      return;

    for( uint i( 0 ); i<mNbConstruction; ++i )
      programblock_city_construction.appendConstruction( b.generate() );

    std::ranlux48_base generator( std::chrono::system_clock::now().time_since_epoch().count() );
    std::uniform_int_distribution<int> distribution_x( mWorldStart[0], mWorldEnd[0] );
    std::uniform_int_distribution<int> distribution_y( mWorldStart[1], mWorldEnd[1] );

    std::uniform_int_distribution<uint> distributionConstruction( 0, mNbConstruction );

    std::vector<Math::Vec2f> addPosition;
    addPosition.reserve( mNbPlace );

    uint loopBreaker( 0 );
    const uint maxLoop( 500 );

    for( uint i( 0 ); i<mNbPlace; ++i )
    {
      while( true )
      {
        loopBreaker++;
        if( loopBreaker>maxLoop )
          return;

        bool exist( false );
        Math::Vec2f p( Math::Vec2f( float(distribution_x( generator ) ), float(distribution_y( generator ) ) ) );
        for( const auto& a : addPosition )
        {
          if( ( std::abs( a[0] - p[0] ) <mNeighborDistance ) ||  ( std::abs( a[1] - p[1] ) < mNeighborDistance ) )
          { exist = true; break;}
        }
        if( !exist )
        {
          addPosition.emplace_back( p );
          programblock_city_construction.placeConstruction(
            p, 0.f, distributionConstruction( generator )
          );
          break;
        }

      }

    }



  }

}


