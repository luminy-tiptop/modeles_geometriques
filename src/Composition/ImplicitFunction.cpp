#include <Composition/ImplicitFunction.hpp>

#include <Gui/Panel_Composition_ImplicitFunction.hpp>

#include <Wrapper/QReadFile.hpp>

#include <QVector2D>
#include <QVector3D>
#include <QImage>

#include <stdexcept>
#include <debug.hpp>

#include <MeshAlgorithms/Bloomenthal.hpp>
#include <Math/ImplicitFunction.hpp>

namespace Composition {

  ImplicitFunction::ImplicitFunction( Composition::CompositionReceiver& _receiver ) :
    AbstractComposition( "ImplicitFunction", _receiver ),
    mAttachedImplicitFunctionWidget( new Gui::Panel_Composition_ImplicitFunction( *this ) )
  {
    updateSeed();
  }

  ImplicitFunction::~ImplicitFunction()
  { if( mAttachedImplicitFunctionWidget ) delete mAttachedImplicitFunctionWidget; }

  // ---- ---- ---- ----

  void ImplicitFunction::updateSeed()
  {
    lastSeed = unsigned(std::chrono::system_clock::now().time_since_epoch().count() );
  }

  // ---- ---- ---- ----

  void ImplicitFunction::initialiseGL( QOpenGLFunctions_3_3_Core& gl )
  {
    programblock.initialise( gl );
    mAttachedImplicitFunctionWidget->setDefaultPreset( true );
    mOpenglInitialized = true;
  }

  void ImplicitFunction::paintGL( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp  )
  {
    if( programblock.count == 0 || programblock.indicesCount == 0  )
      return;

    //
    programblock.bind();

    programblock.program.setUniformValue( "uniform_mvp", mvp );

    if( !mTransparent )
    {
      programblock.program.setUniformValue( "uniform_mode_wireframe", int(0) );
      if( mIndicesMode )
        gl.glDrawElements( GL_TRIANGLES, GLint( programblock.indicesCount ), GL_UNSIGNED_INT, nullptr );
      else
        gl.glDrawArrays( GL_TRIANGLES, 0, GLint( programblock.count ) );
    }

    if( mDrawWireframe || mTransparent )
    {
      programblock.program.setUniformValue( "uniform_mode_wireframe", int(1) );
      gl.glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
      if( mIndicesMode )
        gl.glDrawElements( GL_TRIANGLES, GLint( programblock.indicesCount ), GL_UNSIGNED_INT, nullptr );
      else
        gl.glDrawArrays( GL_TRIANGLES, 0, GLint( programblock.count ) );
      gl.glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    }

    programblock.release();
  }

  // ---- ---- ---- ----

  QWidget* ImplicitFunction::getAttachedWidget()
  { return mAttachedImplicitFunctionWidget; }

  // ---- ---- ---- ----

  void ImplicitFunction::compute(
    Math::ImplicitFunction::func_pf_t function,
    double size, int bounds,
    double x, double y, double z,
    bool withTetrahedralDecomposition,
    uint limitProcess
  )
  {
    //
    if( mChangeSeed )
      updateSeed();

    //
    using CN = Math::CN;

    //
    MeshAlgorithms::Bloomenthal bloom(
      function,
      CN( size ), bounds,
      CN( x ), CN( y ), CN( z ),
      ( withTetrahedralDecomposition ?
        MeshAlgorithms::Bloomenthal::TetrahedralMode::WithDecomposition :
        MeshAlgorithms::Bloomenthal::TetrahedralMode::WithoutDecomposition ),
      lastSeed,
      limitProcess
    );

    //
    programblock.sendVertices( bloom.vertices() );
    programblock.sendTriangles( bloom.triangles() );

  }

}


