#include <Composition/Basic.hpp>

#include <Gui/Panel_Composition_Basic.hpp>

#include <Wrapper/QReadFile.hpp>

#include <QVector2D>
#include <QVector3D>
#include <QImage>

#include <stdexcept>
#include <debug.hpp>

namespace Composition {

  Basic::Basic( Composition::CompositionReceiver& _receiver, std::string _name ) :
    AbstractComposition( _name, _receiver ),
    mAttachedBasicWidget( new Gui::Panel_Composition_Basic( *this ) )
  {}

  Basic::~Basic()
  { if(mAttachedBasicWidget) delete mAttachedBasicWidget; }

  // ---- ---- ---- ----

  QWidget* Basic::getAttachedWidget()
  { return mAttachedBasicWidget; }

  Mesh* Basic::getAttachedOpenMesh()
  { return mMeshBase_up.get(); }

  // ---- ---- ---- ----

  void Basic::initialiseGL( QOpenGLFunctions_3_3_Core& gl )
  {
    // Shader program
    programblockByVertex.initialise( gl );
    programblockByVertexColor.initialise( gl );
    programblockByVertexUv.initialise( gl );
    // External buffer index
    bufferIndex.create();
    //
    mOpenglInitialized = true;
  }

  void Basic::paintGL( QOpenGLFunctions_3_3_Core& gl, const QMatrix4x4& mvp )
  {
    //
    if( !isDone() )
      return;

    //
    ShaderProgram::AbstractShaderProgram& shaderProgram( *drawShaderProgram_p );

    //
    if( shaderProgram.count == 0 )
      return;

    //
    shaderProgram.bind();

    shaderProgram.program.setUniformValue( "uniform_mvp", mvp );

    //
    if( drawUseIndexBuffer )
    {
      bufferIndex.bind();

      shaderProgram.program.setUniformValue( "uniform_mode_wireframe", int(0) );
      gl.glDrawElements( drawPrimitiveType, GLint( bufferIndexCount ), GL_UNSIGNED_INT, nullptr );

      if( mDrawWireFrame )
      {
        shaderProgram.program.setUniformValue( "uniform_mode_wireframe", int(1) );
        gl.glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        gl.glDrawElements( drawPrimitiveType, GLint( bufferIndexCount ), GL_UNSIGNED_INT, nullptr );
        gl.glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
      }

      bufferIndex.release();
    }
    else
    {

      shaderProgram.program.setUniformValue( "uniform_mode_wireframe", int(0) );
      gl.glDrawArrays( drawPrimitiveType, 0, GLint( shaderProgram.count ) );

      if( mDrawWireFrame )
      {
        shaderProgram.program.setUniformValue( "uniform_mode_wireframe", int(1) );
        gl.glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        gl.glDrawArrays( drawPrimitiveType, 0, GLint( shaderProgram.count ) );
        gl.glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
      }

    }

    //
    shaderProgram.release();
  }

  // ---- ---- ---- ----

  void Basic::unsetDraw()
  {
    drawUseIndexBuffer = false;
    drawPrimitiveType = 0;
    bufferIndexCount = 0;
    drawShaderProgram_p = nullptr;
  }

  bool Basic::isDone() const
  {
    return !(
      ( drawShaderProgram_p == nullptr ) ||
      ( drawUseIndexBuffer && bufferIndexCount == 0 ) ||
      ( mOpenglInitialized == false )
    );
  }

  // ---- ---- ---- ----

  void Basic::setActualMode( const RenderMode m )
  {
    if( !any( mCompatibleMode & m ) )
      throw std::runtime_error( "Incompatible mode" );
    else
    {
      mActualMode = m;
      mAttachedBasicWidget->refreshUiRenderModeChoice();
    }
  }

  // ---- ----

  void Basic::setCompatibleAndActualForRenderMode( const RenderMode m )
  {
    if( !any( mCompatibleMode & m ) || ( m == RenderMode::NoRender ) )
      mActualMode = m;

    if( m == RenderMode::Vertex )
      mCompatibleMode = RenderMode::Vertex;
    else if( m == RenderMode::VertexColor )
      mCompatibleMode = RenderMode::Vertex | RenderMode::VertexColor;
    else if( m == RenderMode::VertexUvTexture )
      mCompatibleMode = RenderMode::Vertex | RenderMode::VertexColor | RenderMode::VertexUvTexture;
    else
      mCompatibleMode = RenderMode::NoRender;

    if( !any( mCompatibleMode & mActualMode ) )
      mActualMode = m;

    // @todo add loadMesh for real render ?
    // if ( !any( mCompatibleMode & mActualMode ) || ( m == RenderMode::NoRender ) )
    //   mActualMode = m;

    //
    mAttachedBasicWidget->refreshUiRenderModeChoice();
  }

  // ---- ---- ---- ----

  void Basic::loadMesh( const std::string& path, const std::string& baseDirectory, const bool importTexture, const bool importColor )
  {
    std::unique_ptr<Mesh> mesh_up( std::make_unique<Mesh>() );

    mBaseDirectory = baseDirectory;
    mMeshOpt = OpenMesh::IO::Options();
    if( importTexture ) mMeshOpt += OpenMesh::IO::Options::VertexTexCoord;
    if( importColor ) mMeshOpt += OpenMesh::IO::Options::VertexColor;

    if( !OpenMesh::IO::read_mesh( *mesh_up, path, mMeshOpt ) )
      throw std::runtime_error( "openmesh error on external file : " + path );

    loadMesh( std::move( mesh_up ), importTexture, importColor );
  }

  // ---- ---- ---- ----

  void Basic::loadInternalMesh( const std::string& mesh_file, const bool importColor )
  {
    const std::string mesh_resource_path( ":/mesh/" + mesh_file );
    std::cout << "internal loading from : " << mesh_resource_path << std::endl;

    //
    try{
      std::unique_ptr<Mesh> mesh_up( std::make_unique<Mesh>() );

      mMeshOpt = OpenMesh::IO::Options();
      //if ( importTexture ) mMeshOpt += OpenMesh::IO::Options::VertexTexCoord;
      if( importColor ) mMeshOpt += OpenMesh::IO::Options::VertexColor;

      //
      Wrapper::QReadFile mesh_file( mesh_resource_path );
      std::unique_ptr<Wrapper::QIstream> mesh_file_stream_up( mesh_file.getStream() );

      //
      if( !OpenMesh::IO::read_mesh( *mesh_up, *mesh_file_stream_up, "obj", mMeshOpt ) )
        throw std::runtime_error( "openmesh error on internal file : " + mesh_resource_path );

      //
      loadMesh( std::move( mesh_up ), false, importColor );
    }
    catch( const std::exception& e )
    { std::cout << "mesh loading error : " << e.what() << std::endl; }

  }

  // ---- ---- ---- ----

  void Basic::internalMeshIndex( const Mesh& mesh )
  {
    bufferIndexCount = GLuint( mesh.n_faces() * 3 );
    std::vector<GLuint> index_data;
    index_data.reserve( bufferIndexCount );

    //
    Mesh::ConstFaceIter face_it( mesh.faces_begin() );
    const Mesh::ConstFaceIter face_end_it( mesh.faces_end() );
    for(; face_it != face_end_it; ++face_it )
    {
      Mesh::ConstFaceVertexIter face_vertex_it( mesh.cfv_iter( *face_it ) );
      index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
      ++face_vertex_it;
      index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
      ++face_vertex_it;
      index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
    }

    //
    const GLuint index_sizeof( bufferIndexCount * sizeof( GLuint ) );

    bufferIndex.bind();
    bufferIndex.allocate( index_data.data(), int(index_sizeof) );
    bufferIndex.release();

    drawUseIndexBuffer = true;
  }

  // ---- ---- ---- ----

  void Basic::loadMesh( const std::vector<QVector3D>& vertices )
  {
    mMeshBase_up.reset();
    mMeshFirstLoad = false;

    setCompatibleAndActualForRenderMode( RenderMode::NoRender );

    //
    throwIfNotInitializedGL();
    unsetDraw();

    // BufferData of Vertex
    programblockByVertex.send( vertices );

    // Configure Render
    drawPrimitiveType = GL_TRIANGLES;
    drawShaderProgram_p = &programblockByVertex;

    //
    mReceiver.repaint();
  }

  // ---- ----

  void Basic::loadMesh( std::unique_ptr<Mesh> mesh_up, const bool importTexture, const bool importColor )
  {
    mMeshBase_up = std::move( mesh_up );
    mMeshFirstLoad = true;

    if( importTexture ) setCompatibleAndActualForRenderMode( RenderMode::VertexUvTexture );
    else if( importColor ) setCompatibleAndActualForRenderMode( RenderMode::VertexColor );
    else setCompatibleAndActualForRenderMode( RenderMode::Vertex );

    reloadMesh();
  }


  void Basic::reloadMesh()
  {
    if( !mMeshBase_up )
    {std::cout << "No mesh for this action" << std::endl; return;}

    if( !any( mActualMode & mCompatibleMode ) )
    {std::cout << "No compatible mesh for this action" << std::endl; return;}

    //
    throwIfNotInitializedGL();
    unsetDraw();

    //
    Mesh& mesh( *mMeshBase_up );

    // BufferData of Index
    internalMeshIndex( mesh );

    //

    if( mActualMode == RenderMode::Vertex )  // BufferData of Vertex
    {
      programblockByVertex.send( mesh.points(), mesh.n_vertices() );

      // Configure Render
      drawPrimitiveType = GL_TRIANGLES;
      drawShaderProgram_p = &programblockByVertex;
    }
    else if( mActualMode == RenderMode::VertexColor )  // BufferData of Vertex / Color
    {
      mesh.request_vertex_colors();
      std::vector<ShaderProgram::VertexColorPack::Format> vecVertexColor;
      vecVertexColor.reserve( mesh.n_vertices() );

      //
      Mesh::VertexIter vertex_it( mesh.vertices_begin() );
      const Mesh::VertexIter vertex_end_it( mesh.vertices_end() );
      for(; vertex_it != vertex_end_it; ++vertex_it )
      {
        vecVertexColor.emplace_back(
          mesh.point( *vertex_it ),   // glsl : vec3 vertex_from_buffer;
          mesh.color( *vertex_it )    // glsl : vec3 color_from_buffer;
        );
      }

      //
      programblockByVertexColor.send( vecVertexColor );

      // Configure Render
      drawPrimitiveType = GL_TRIANGLES;
      drawShaderProgram_p = &programblockByVertexColor;
    }
    else if( mActualMode == RenderMode::VertexUvTexture )  // BufferData of Vertex / Uv
    {
      mesh.request_vertex_texcoords2D();
      std::vector<ShaderProgram::VertexUvPack::Format> vertexUvData;
      vertexUvData.reserve( mesh.n_vertices() );

      //
      Mesh::VertexIter vertex_it( mesh.vertices_begin() );
      const Mesh::VertexIter vertex_end_it( mesh.vertices_end() );
      for(; vertex_it != vertex_end_it; ++vertex_it )
      {
        vertexUvData.emplace_back(
          mesh.point( *vertex_it ),      // glsl : vec3 vertex_from_buffer;
          mesh.texcoord2D( *vertex_it )  // glsl : vec2 uv_from_buffer;
        );
      }

      //
      programblockByVertexUv.send( vertexUvData, mMeshFirstLoad );

      // Configure Render
      drawPrimitiveType = GL_TRIANGLES;
      drawShaderProgram_p = &programblockByVertexUv;

      //
      if( mMeshFirstLoad )
      {
        debug_cout << "base_mesh_directory : " << mBaseDirectory << std::endl;
        debug_cout << "loading texture ..." << std::endl;

        // Texture
        {
          OpenMesh::MPropHandleT<std::map<int, std::string>> texture_property;
          std::string first_texture_entry;
          if( mesh.get_property_handle( texture_property, "TextureMapping" ) )
            if( !mesh.property( texture_property ).empty() )
              first_texture_entry = mesh.property( texture_property ).begin()->second;
          if( first_texture_entry.empty() )
            debug_cout << "  no texture property found." << std::endl;
          else
          {
            if( mesh.property( texture_property ).size() > 1 )
              for( const std::pair<int, std::string>& el : mesh.property( texture_property ) )
                debug_cout << "  texture_entry n" << el.first << " : " << el.second << std::endl;
            debug_cout << "  selected texture : " << first_texture_entry << std::endl;


            const std::string texture_path( mBaseDirectory + "/" + first_texture_entry );
            debug_cout << "  opening texture_path : " << texture_path << std::endl;


            // Opening it
            QImage image( QString::fromStdString( texture_path ) );
            QImage::Format format( image.format() );

            if( format == QImage::Format_Invalid )
              debug_cout << "  !!! No texture found !!! " << std::endl;
            else
            {
              //
              debug_cout << "  Image size " << image.width() << "x" << image.height() << std::endl;

              //
              programblockByVertexUv.sendTexture( image );
            }
          }
        }
      }

    }
    else
    {
      drawPrimitiveType = 0xFFFF;
      drawShaderProgram_p = nullptr;
    }

    mMeshFirstLoad = false;
    mReceiver.repaint();
  }

}


