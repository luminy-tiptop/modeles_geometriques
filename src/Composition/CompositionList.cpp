#include <Composition/CompositionList.hpp>

namespace Composition {

  std::unique_ptr<CompositionList> CompositionList::mSingleton;

  CompositionList& CompositionList::get()
  {
    if( !mSingleton )
      mSingleton = std::unique_ptr<CompositionList>( new CompositionList() );
    return *mSingleton;
  }

  //  AbstractComposition & CompositionList::addComposition( composition_up_t composition_up )
  //  {
  //    compositions.emplace_back( std::move( composition_up ) );
  //    AbstractComposition & composition( *compositions.back() );
  //    if ( gl )
  //      composition.initialiseGL( *gl );
  //    return composition;
  //  }

  // ---- ----

  // void CompositionList::initialiseGL( QOpenGLFunctions_3_3_Core & _gl )
  // {
  //   if ( gl ) return;
  //   gl = &_gl;
  //   for ( AbstractComposition & composition : *this )
  //     composition.initialiseGL( *gl );
  // }


}

