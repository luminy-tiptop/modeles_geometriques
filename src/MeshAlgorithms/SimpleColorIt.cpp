#include <MeshAlgorithms/SimpleColorIt.hpp>

#include <Math/Vec.hpp>
#include <Color/hslPlot.h>

#include <debug.hpp>
#include <vector>
#include <iostream>
#include <iomanip>
#include <unordered_map>

namespace MeshAlgorithms {
  namespace SimpleColorIt {

    static constexpr uint plotTestSize( 720 );
    static std::vector<OpenMesh::Vec3uc> plotTest( Color::hslPlot<OpenMesh::Vec3uc>( plotTestSize, 1.f ) );
    struct plotTestColorPicker
    {
      const float padding;
      plotTestColorPicker( uint nbElement ) : padding( plotTestSize / float( nbElement ) ) {}
      inline OpenMesh::Vec3uc get( uint id ) const { return plotTest[uint( float( id ) * padding ) % plotTestSize];  }
    };

    // ---- ---- ---- ----

    void colorNeighbour( Mesh & mesh )
    {
      std::cout << "ColorNeighbour" << std::endl;

      //
      mesh.request_vertex_colors();
      std::unordered_map<int, OpenMesh::Vec3uc> vertexIdByColor;
      const plotTestColorPicker colorPicker( uint( mesh.n_vertices() ) );
      uint colorNum( 0 );

      //
      Mesh::VertexIter v_it( mesh.vertices_begin() );
      const Mesh::VertexIter vEnd_cit( mesh.vertices_end() );
      for ( ; v_it != vEnd_cit; ++v_it )
      {
        const int vId( v_it->idx() );

        //
        uint neighbourCount( 0 );

        // Find color of vertex or create new
        auto findV( vertexIdByColor.find( vId ) );
        OpenMesh::Vec3uc vColor;
        if ( findV == vertexIdByColor.end() )
        {
          vColor = colorPicker.get( colorNum++ ); //plotTest[( colorNum++ ) % plotTestSize ];
          vertexIdByColor.emplace( vId, vColor );
        }
        else vColor = findV->second;

        Mesh::VertexVertexIter vv_it( mesh.vv_iter( *v_it ) );
        for ( ; vv_it.is_valid(); ++vv_it )
        {
          auto findVV( vertexIdByColor.find( vv_it->idx() ) );
          if ( findVV == vertexIdByColor.end() )
            mesh.set_color( *vv_it, vColor );
          else
            mesh.set_color( *vv_it, ( vColor / 2 ) + ( findVV->second / 2 ) );

          //
          ++neighbourCount;
        }

        //
        std::cout << "  Vertex(" << std::setw( 5 ) << vId << ") nbNeighboor(" << neighbourCount << ")" << std::endl;
      }

    }

    // ---- ----

    void colorVertices( Mesh & mesh )
    {
      //
      mesh.request_vertex_colors();
      const plotTestColorPicker colorPicker( uint( mesh.n_vertices() ) );
      uint colorNum( 0 );

      //
      Mesh::VertexIter v_it( mesh.vertices_begin() );
      const Mesh::VertexIter vEnd_cit( mesh.vertices_end() );
      for ( ; v_it != vEnd_cit; ++v_it )
        mesh.set_color( *v_it, colorPicker.get( colorNum++ ) );
    }

    // ---- ----

    void colorCurves( Mesh & mesh )
    {
      //
      mesh.request_vertex_colors();

      //
      Mesh::VertexIter v_it( mesh.vertices_begin() );
      const Mesh::VertexIter vEnd_cit( mesh.vertices_end() );
      for ( ; v_it != vEnd_cit; ++v_it )
      {
        //
        Mesh::VertexVertexIter vv_it( mesh.vv_iter( *v_it ) );
        if ( !vv_it.is_valid() ) continue;

        //
        float areaSum( 0 );
        float angleSum( 0 );

        //
        const Mesh::Point pointTop( mesh.point( *v_it ) );
        const Mesh::Point pointFirst( mesh.point( *vv_it ) );
        Mesh::Point pointLast( pointFirst );
        //float distanceLast( Math::distance( pointTop, pointLast ) );

        debug_cout << v_it->idx() << " : " << std::endl;

        //
        debug_cout << "  " << vv_it->idx();
        ++vv_it;
        for ( ; vv_it.is_valid(); ++vv_it )
        {
          const Mesh::Point pointCurrent( mesh.point( *vv_it ) );
          areaSum += Math::triangleArea( pointTop, pointLast, pointCurrent );
          angleSum += Math::triangleAngle( pointTop, pointLast, pointCurrent );
          pointLast = pointCurrent;

          debug_cout << "  " << vv_it->idx();
        }
        areaSum += Math::triangleArea( pointTop, pointLast, pointFirst );
        angleSum += Math::triangleAngle( pointTop, pointLast, pointFirst );

        constexpr float pi2( float( 2.0 * M_PI ) );

        float K( ( pi2 - angleSum ) / areaSum );
        mesh.set_color( *v_it, { K, K / 10.f, K / 100.f } );

        //
        debug_cout << " K(" << K << ") areaSum(" << areaSum << ") angleSum(" << angleSum << ") " << std::endl;

      }
    }

  }
}
