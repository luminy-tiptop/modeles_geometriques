#include <MeshAlgorithms/Generation/CityConstruction/Building.hpp>

namespace MeshAlgorithms {
  namespace Generation {
    namespace CityConstruction {

      struct BuildingBuilder
      {
       public:
        using Point = Mesh::Point;
        using VertexHandle = Mesh::VertexHandle;

       public:
        float curentHeight = 0.f;

       public:
        Building& b;
        const Building::Properties& p;
        GeneratedMesh_up generatedMesh_up;
        Mesh& mesh;

       protected:
        virtual void addGroundFloor()
        { addFloor(); }

        virtual void addRoof()
        {
          // Roof
          {
            std::vector<VertexHandle> vertices;
            const auto placeVertex([&]( const Point& v ) { vertices.emplace_back( mesh.add_vertex( v ) ); return vertices.back(); } );
            const float roofWidth( p.width * p.roofScale );
            VertexHandle centerFront( placeVertex( { 0.f, curentHeight + p.roofHeight, +roofWidth } ) );
            VertexHandle centerBack( placeVertex(  { 0.f, curentHeight + p.roofHeight, -roofWidth } ) );
            VertexHandle leftFront( placeVertex(  { -roofWidth, curentHeight, +roofWidth } ) );
            VertexHandle leftBack( placeVertex(   { -roofWidth, curentHeight, -roofWidth } ) );
            VertexHandle rightFront( placeVertex( { +roofWidth, curentHeight, +roofWidth } ) );
            VertexHandle rightBack( placeVertex(  { +roofWidth, curentHeight, -roofWidth } ) );
            for( auto& vh : vertices )
              VertexProperty::Attrib_setProperty( mesh, vh, VertexProperty::Attrib::BuildingRoof );
            mesh.add_face_quad( leftBack, centerBack, centerFront, leftFront );
            mesh.add_face_quad( rightBack, rightFront, centerFront, centerBack );
          }

          // Roof front & back faces
          {
            std::vector<VertexHandle> vertices;
            const auto placeVertex([&]( const Point& v ) { vertices.emplace_back( mesh.add_vertex( v ) ); return vertices.back(); } );
            VertexHandle centerFront( placeVertex( { 0.f, curentHeight + p.roofHeight, p.width } ) );
            VertexHandle centerBack( placeVertex(  { 0.f, curentHeight + p.roofHeight, -p.width } ) );
            VertexHandle leftFront( placeVertex(  { -p.width, curentHeight, +p.width } ) );
            VertexHandle leftBack( placeVertex(   { -p.width, curentHeight, -p.width } ) );
            VertexHandle rightFront( placeVertex( { +p.width, curentHeight, +p.width } ) );
            VertexHandle rightBack( placeVertex(  { +p.width, curentHeight, -p.width } ) );
            for( auto& vh : vertices )
              VertexProperty::Attrib_setProperty( mesh, vh, VertexProperty::Attrib::BuildingGrey2 );
            mesh.add_face( rightFront, centerFront, leftFront );
            mesh.add_face( rightBack, centerBack, leftBack );
          }

          //
          curentHeight += p.roofHeight;
        }

        virtual void addFloor()
        {
          //
          std::vector<VertexHandle> vertices;
          const auto placeVertex([&]( const Point& v ) { vertices.emplace_back( mesh.add_vertex( v ) ); return vertices.back(); } );

          //
          VertexHandle baseFrontLeft(  placeVertex( { -p.width, curentHeight + 0.f,      +p.width } ) );
          VertexHandle baseFrontRight( placeVertex( { +p.width, curentHeight + 0.f,      +p.width } ) );
          VertexHandle baseBackLeft(   placeVertex( { -p.width, curentHeight + 0.f,      -p.width } ) );
          VertexHandle baseBackRight(  placeVertex( { +p.width, curentHeight + 0.f,      -p.width } ) );
          VertexHandle topFrontLeft(   placeVertex( { -p.width, curentHeight + p.height, +p.width } ) );
          VertexHandle topFrontRight(  placeVertex( { +p.width, curentHeight + p.height, +p.width } ) );
          VertexHandle topBackLeft(    placeVertex( { -p.width, curentHeight + p.height, -p.width } ) );
          VertexHandle topBackRight(   placeVertex( { +p.width, curentHeight + p.height, -p.width } ) );
          curentHeight += p.height;

          //
          for( auto& vh : vertices )
            VertexProperty::Attrib_setProperty( mesh, vh, VertexProperty::Attrib::BuildingGrey1 );

          /**< @todo re-use last roof, indeed curentHeight */

          //
          mesh.add_face_quad( topFrontRight, baseFrontRight, baseFrontLeft, topFrontLeft ); // Front face
          mesh.add_face_quad( topFrontLeft, baseFrontLeft, baseBackLeft, topBackLeft );     // Left face
          mesh.add_face_quad( topBackLeft, baseBackLeft, baseBackRight, topBackRight );     // Back face
          mesh.add_face_quad( topBackRight, baseBackRight, baseFrontRight, topFrontRight ); // Right face
          mesh.add_face_quad( topBackRight, topFrontRight, topFrontLeft, topBackLeft );     // Top face
          mesh.add_face_quad( baseFrontLeft, baseFrontRight, baseBackRight, baseBackLeft ); // Bottom face
        }


       public:
        inline BuildingBuilder( Building& _b, const Building::Properties& _p ) :
          b( _b ), p( _p ),
          generatedMesh_up( std::make_unique<GeneratedMesh>() ),
          mesh( generatedMesh_up->mesh )
        {
          VertexProperty::Attrib_addProperty( mesh );
          addGroundFloor();
          for( uint i( 0 ); i<p.nbFloor; ++i )
            addFloor();
          addRoof();
        }

      };


      // ---- ---- ---- ----


      Building::Building()
      { }

      GeneratedMesh_up Building::generate()
      { return BuildingBuilder( *this, Properties() ).generatedMesh_up; }

    }
  }
}
