/**
 * @author GIG 2018 : D’AMBRA / BIRJAM / SONNTAG
 */

#include <MeshAlgorithms/MeshSmoothing.hpp>

#include <Math/Vec.hpp>

#define USE_COT 0

namespace MeshAlgorithms {
  namespace MeshSmoothing {


    LaplaceBeltramiOperateurApproximationCotangentielle::
    LaplaceBeltramiOperateurApproximationCotangentielle( const float _coef_h, const float _limit_diff_delta  ) :
      coef_h( std::move( _coef_h ) ),
      limit_diff_delta( std::move( _limit_diff_delta ) )
    {}

    Math::Vec3f LaplaceBeltramiOperateurApproximationCotangentielle::
    getSmoothedVertex( const Mesh& mesh, const Mesh::VertexIter& v_it )
    {
      //
      const Mesh::Point pointTop( mesh.point( *v_it ) );

      //
      Mesh::ConstVertexVertexIter vv_it( mesh.cvv_iter( *v_it ) );
      if( !vv_it.is_valid() )
        return pointTop;

      //
      Mesh::Point angleSumByDiffFromTop( 0.f, 0.f, 0.f );
      float areaSumOfBarycenter( 0.f );
      uint pointNeighborCount( 0 );

      {
        //
        const Mesh::Point pointNeighborFirst( mesh.point( *vv_it ) );

        //
        Mesh::Point pointNeighborLast( 0.f, 0.f, 0.f );
        Mesh::Point pointNeighborCurrent( 0.f, 0.f, 0.f );
        Mesh::Point barycenterFirst( 0.f, 0.f, 0.f );
        Mesh::Point barycenterLast( 0.f, 0.f, 0.f );

        // On utilise une boucle sans fin qui s'arrete une fois qu'elle
        // a parcourrut l'ensemble des voisins, ainsi que le tout premier, à nouveau.
        // Cela permet de trouver :
        //  - trouver les barycentres (entre chaque triangle)
        //  - calculer l'aire du triangle entre 2 barycentres et pointTop
        for(;; )
        {
          //
          const bool lastLoop( !vv_it.is_valid() );

          //
          const Mesh::Point pointNext(
            lastLoop ?
            pointNeighborFirst :
            mesh.point( *vv_it )
          );

          // Ici l'on calcul :
          //  - la somme des anges
          //  - la somme des aires des triangles formés par les barycentres
          // Pour cela l'on doit connaitre les variables :
          //  - pointNeighborCurrent
          //  - pointNeighborLast
          // ce qui necessite 2 tours avant
          if( pointNeighborCount >= 3 )
          {
            //
            const float alpha( Math::triangleAngle( pointNeighborLast, pointTop, pointNeighborCurrent ) );
            const float beta( Math::triangleAngle( pointNext, pointNeighborCurrent, pointTop ) );

            //
            #if USE_COT == 1
            const float alphaCotangente( std::cos( alpha ) / std::sin( alpha ) );
            const float betaCotangente( std::cos( beta ) / std::sin( beta ) );
            angleSumByDiffFromTop += ( alphaCotangente + betaCotangente ) * ( pointNeighborCurrent - pointTop );
            #else
            angleSumByDiffFromTop += ( alpha + beta ) * ( pointNeighborCurrent - pointTop );
            #endif

            //
            const Math::Vec3f barycenterCurrent( ( pointTop + pointNeighborLast + pointNeighborCurrent ) / 3.f );

            //
            if( pointNeighborCount == 3 )
              barycenterFirst = barycenterCurrent;

            // L'on doit connaitre les variables :
            //  - barycenterCurrent
            //  - barycenterLast
            // ce qui necessite 3 tours ( 1 de plus que pour connaitre un voisin )
            if( lastLoop )
              areaSumOfBarycenter += Math::triangleArea( pointTop, barycenterCurrent, barycenterFirst );
            else if( pointNeighborCount >= 4 )
              areaSumOfBarycenter += Math::triangleArea( pointTop, barycenterLast, barycenterCurrent );

            //
            barycenterLast = barycenterCurrent;
          }

          //
          pointNeighborLast = pointNeighborCurrent;
          pointNeighborCurrent = pointNext;

          //
          if( lastLoop )
            break;

          //
          ++pointNeighborCount;
          ++vv_it;
        }
      }

      // Correction si : Le points ne contient pas assez de voisin
      if( pointNeighborCount <= 3 )
        return pointTop;

      // Correction si : Erreur de calcul des angles ou aires
      const bool notValidCompute(
        std::isnan( angleSumByDiffFromTop[0] )
        || std::isnan( angleSumByDiffFromTop[1] )
        || std::isnan( angleSumByDiffFromTop[2] )
        || std::isnan( areaSumOfBarycenter )
      );
      if( notValidCompute )
        return pointTop;

      //
      const Math::Vec3f deltaPoint(
        angleSumByDiffFromTop / ( 2.f * areaSumOfBarycenter )
      );

      // Correction si : la longueur depasse la limite de diff (fournit par l'utilisateur pour corriger les bugs)
      const bool notValidDeltaPoint( Math::length( deltaPoint ) > limit_diff_delta );
      if( notValidDeltaPoint )
        return pointTop;

      //
      return pointTop + ( deltaPoint * coef_h );
    }


    // ---- ---- ---- ----


  }
}
