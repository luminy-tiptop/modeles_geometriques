/**
 * @author GIG 2018 : D’AMBRA / BIRJAM / SONNTAG
 */

#include <MeshAlgorithms/MeshRepair.hpp>

namespace MeshAlgorithms {
  namespace MeshRepair {


    // ---- ---- ---- ----

    static constexpr inline float square( float value ) { return value * value; }

    // ---- ---- ---- ----

    static float computeArea( const Mesh::Point & pA, const Mesh::Point & pB, const Mesh::Point & pC )
    {
      // longueur des trois côté du triangle
      const float a( std::sqrt( square( pA[0] - pB[0] ) + square( pA[1] - pB[1] ) + square( pA[2] - pB[2] ) ) );
      const float b( std::sqrt( square( pB[0] - pC[0] ) + square( pB[1] - pC[1] ) + square( pB[2] - pC[2] ) ) );
      const float c( std::sqrt( square( pA[0] - pC[0] ) + square( pA[1] - pC[1] ) + square( pA[2] - pC[2] ) ) );

      // Formule de Héron
      const float P( ( a + b + c ) / ( 2 ) );
      return std::sqrt( P * ( P - a ) * ( P - b ) * ( P - c ) );
    }

    // ---- ---- ---- ----

    float qualite(
      uint i, uint k, uint j,
      const Mesh& mesh,
      const std::vector<Mesh::VertexHandle>& holeVertex
    )
    {
      const Mesh::Point pA( mesh.point( holeVertex[i] ) );
      const Mesh::Point pB( mesh.point( holeVertex[k] ) );
      const Mesh::Point pC( mesh.point( holeVertex[j] ) );
      return computeArea( pA, pB, pC );
    }

    float qualite(
      uint i, uint k, uint j,
      const Mesh& mesh,
      const std::vector<Mesh::HalfedgeHandle>& hole
    )
    {
      const Mesh::Point pA( mesh.point( mesh.from_vertex_handle( hole[i] ) ) );
      const Mesh::Point pB( mesh.point( mesh.from_vertex_handle( hole[k] ) ) );
      const Mesh::Point pC( mesh.point( mesh.from_vertex_handle( hole[j] ) ) );
      return computeArea( pA, pB, pC );
    }

    // ---- ---- ---- ----

    static float val(
      uint i, uint j,
      std::vector<std::vector<float>>& matrice,
      std::vector<std::vector<uint>>& pos,
      const Mesh& mesh,
      const std::vector<Mesh::VertexHandle>& holeVertex
    )
    {
      //
      float min( FLT_MAX ), tmp( 0.f );
      uint indice( 0 );

      //
      for( uint k( i + 1 ); ( k < j ); ++k ) {
        tmp = qualite( i, k, j, mesh, holeVertex ) + matrice[i][k] + matrice[k][j];
        if( min > tmp )
        {
          min = tmp;
          indice = k;
        }
      }

      //
      matrice[i][j] = min;
      pos[i][j] = indice;
      return min;
    }

    // static float val(
    //   uint i, uint j,
    //   std::vector<std::vector<float>>& matrice,
    //   std::vector<std::vector<uint>>& pos,
    //   Mesh& mesh,
    //   std::vector<Mesh::HalfedgeHandle>& hole
    // )
    // {
    //   //
    //   if( i > j - 2 )
    //     return matrice[i][j];
    //
    //   //
    //   float min( FLT_MAX ), tmp( 0 );
    //   uint indice( 0 );
    //
    //   //
    //   for( uint k( i + 1 ); ( k < j ); ++k )
    //   {
    //     tmp = qualite( i, k, j, mesh, hole ) + matrice[i][k] + matrice[k][j];
    //     if( min > tmp )
    //     {
    //       min = tmp;
    //       indice = k;
    //     }
    //   }
    //
    //   //
    //   matrice[i][j] = min;
    //   pos[i][j] = indice;
    //   return min;
    // }

    // ---- ---- ---- ----

    static void fill(
      uint i, uint j,
      const std::vector<std::vector<uint>>& pos,
      const std::vector<Mesh::VertexHandle>& holeVertex,
      Mesh& mesh
    )
    {
      if( j > ( i + 1 ) )
      {
        mesh.add_face( holeVertex[i], holeVertex[pos[i][j]], holeVertex[j] );
        fill( i, pos[i][j], pos, holeVertex, mesh );
        fill( pos[i][j], j, pos, holeVertex, mesh );
      }
    }


    // ---- ---- ---- ----

    void doRepair( Mesh& mesh )
    {
      // static constexpr uint maxLoop( 100 );
      // uint count( 0 );
      // while( true )
      // {
      //
      Mesh::ConstHalfedgeIter halfedge_it( mesh.halfedges_begin() );
      const Mesh::ConstHalfedgeIter halfedge_end_it( mesh.halfedges_end() );
      for(; halfedge_it!=halfedge_end_it; ++halfedge_it )
        if( mesh.is_boundary( *halfedge_it ) )
          break;

      //
      if( halfedge_it==halfedge_end_it )
      {
        // if( count == 0 )
        throw std::runtime_error( "[MeshRepair] No boundary found" );
        // else
        //   return;
      }

      // //
      // if( count>maxLoop )
      //   throw std::runtime_error( "[MeshRepair] Loop number exceeded " + std::to_string( maxLoop ) );
      // count++;

      //
      Mesh::HalfedgeHandle starting_heh( *halfedge_it );
      Mesh::HalfedgeHandle current_heh( mesh.next_halfedge_handle( starting_heh ) );

      //
      std::vector<Mesh::HalfedgeHandle> hole;
      std::vector<Mesh::VertexHandle> holeVertex;

      // hole traitement
      while( starting_heh != current_heh )
      {
        hole.push_back( current_heh );
        current_heh = mesh.next_halfedge_handle( current_heh );
      }

      //
      for( auto & heh : hole )
        holeVertex.emplace_back( ( mesh.from_vertex_handle( heh ) ) );
      holeVertex.emplace_back( mesh.from_vertex_handle( current_heh ) );

      //
      std::vector<std::vector<float>> matrice( holeVertex.size(), std::vector<float>( holeVertex.size() ) );
      std::vector<std::vector<uint>> pos( holeVertex.size(), std::vector<uint>( holeVertex.size() ) );

      //
      for( uint i( 0 ); i < ( holeVertex.size() - 2 ); ++i )
      {
        matrice[i][i + 2] = qualite( i, i + 1, i + 2, mesh, holeVertex );
        pos[i][i + 2] = i + 1;
      }

      //
      for( uint j( 3 ); j < holeVertex.size(); ++j )
        for( uint i( 0 ); i < ( holeVertex.size() - j ); ++i )
          val( i, i + j, matrice, pos, mesh, holeVertex );

      //
      fill( 0, uint( holeVertex.size() - 1 ), pos, holeVertex, mesh );

      //  }

    }

  }
}
