#include <MeshAlgorithms/Treatement/Splitter.hpp>

#include <stdexcept>

namespace MeshAlgorithms {
  namespace Treatement {

    Splitter::Splitter( uint nbSplit ) :
      mNbSplit( nbSplit )
    {}


    void Splitter::process( Mesh& )
    {
      /** @todo implt it by Voronoy Diagram */
      throw std::runtime_error( "[Splitter::process] not implanted" );
    }

  }
}
