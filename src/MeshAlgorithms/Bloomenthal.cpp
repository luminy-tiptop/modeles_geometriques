#include <MeshAlgorithms/Bloomenthal.hpp>

#include <Math.hpp>

#include <random>
#include <chrono>

#include <debug.hpp>

namespace MeshAlgorithms {

  /**
   * @brief Bloomenthal implementation
   * @author Original Article can be found at :
   *  - http://www.unchainedgeometry.com/jbloom/pdf/polygonizer.pdf
   * @author Released by GIG 2018 Team
   *
   * an implicit surface polygonizer, translated from Mesa
   * applications should call polygonize()
   * Authored by Jules Bloomenthal, Xerox PARC.
   * Copyright (c) Xerox Corporation, 1991. All rights reserved.
   * Permission is granted to reproduce, use and distribute this code for
   * any and all purposes, provided that this notice appears in all
   * copies.
   *
   */

  static constexpr uint RES  = 10;  /**< # converge iterations */
  static constexpr uint L = 0;      /**< left direction: -x, -i */
  static constexpr uint R = 1;      /**< right direction: +x, +i */
  static constexpr uint B = 2;      /**< bottom direction: -y, -j */
  static constexpr uint T = 3;      /**< top direction: +y, +j */
  static constexpr uint N = 4;      /**< near direction: -z, -k */
  static constexpr uint F = 5;      /**< far direction: +z, +k */
  static constexpr uint LBN = 0;    /**< left bottom near corner */
  static constexpr uint LBF = 1;    /**< left bottom far corner */
  static constexpr uint LTN = 2;    /**< left top near corner */
  static constexpr uint LTF = 3;    /**< left top far corner */
  static constexpr uint RBN = 4;    /**< right bottom near corner */
  static constexpr uint RBF = 5;    /**< right bottom far corner */
  static constexpr uint RTN = 6;    /**< right top near corner */
  static constexpr uint RTF = 7;    /**< right top far corner */
  /** the LBN corner of cube (i, j, k), corresponds with location
   * (start[0]+(i-.5)*size, start[1]+(j-.5)*size, start[2]+(k-.5)*size) */

  // ---- ---- ---- ----

  /** hash table size (32768) */
  /** @todo use 65536 hash map to prevent HASH() + HASH() in "setedge" ... */
  static constexpr size_t HASHBIT( 5 );
  static constexpr size_t HASHSIZE( 1 << ( ( 3 * HASHBIT ) + 1 ) );
  static constexpr size_t MASK( ( 1 << HASHBIT ) - 1 );

  // ---- ----

  // Note :
  // See hex bits works : https://coliru.stacked-crooked.com/a/a4d48423e78d7469

  // /** @brief HASH */
  // inline uint HASH( uint i, uint j, uint k )
  // { return ( ( ( ( uint( i ) & MASK ) << HASHBIT ) | ( uint( j ) & MASK ) ) << HASHBIT ) | ( uint( k ) & MASK ); }
  //
  // /** @brief BIT */
  // inline uint BIT( uint i, uint bit )
  // { return ( ( i ) >> ( bit ) ) & 1; }
  //
  // /** @brief FLIP flip the given bit of i */
  // inline uint FLIP( uint i, uint bit )
  // { return ( i ) ^ 1 << ( bit ); }

#define HASH( i, j, k ) uint( ( ( ( ( size_t( i ) & MASK ) << HASHBIT ) | ( size_t( j ) & MASK ) ) << HASHBIT ) | ( size_t( k ) & MASK ) )
#define BIT( i, bit ) ( ( ( i ) >> ( bit ) ) & 1 )
#define FLIP( i, bit ) ( ( i ) ^ 1 << ( bit ) )

  // ---- ---- ---- ----

  using CN = Bloomenthal::CN;
  using Point3D = Bloomenthal::Point3D;
  using VertexFormat = Bloomenthal::VertexFormat;

  using TrianglesIndices = Bloomenthal::TrianglesIndices;
  using Vertices = Bloomenthal::Vertices;

  // ---- ----

  /** test the function for a signed value */
  struct TEST
  {
    Point3D p;      /**< location of test */
    CN value; /**< function value at p */
    int ok;       /**< if value is of correct sign */
  };

  /** corner of a cube */
  struct CORNER
  {
    int i, j, k;   /**< (i, j, k) is index within lattice  */
    Point3D position; /**< location */
    CN value;    /**< value */
  };

  /** partitioning cell (cube) */
  struct CUBE
  {
    int i, j, k;       /**< lattice location of cube */
    CORNER* corners[8]; /**< eight corners */

    CUBE() = default;
    inline CUBE( int _i, int _j, int _k ) :
      i( std::move( _i ) ), j( std::move( _j ) ), k( std::move( _k ) ),
      corners{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr }
    {}

  };

  /** linked list of cubes acting as stack */
  struct CUBES
  {
    CUBE cube{ 0, 0, 0 };   /**< a single cube */
    CUBES* next{ nullptr }; /**< remaining elements */

    CUBES() = default;
  };

  /** list of cube locations */
  struct CENTERLIST
  {
    int i, j, k;      /**< cube location */
    CENTERLIST* next; /**< remaining elements */
  };

  /** list of corners */
  struct CORNERLIST
  {
    int i, j, k;      /**< corner id */
    CN value;     /**< corner value */
    CORNERLIST* next; /**< remaining elements */
  };

  /** list of edges */
  struct EDGELIST
  {
    int i1, j1, k1, i2, j2, k2; /**< edge corner ids */
    uint vid;                    /**< vertex id */
    EDGELIST* next;             /**< remaining elements */
  };

  /** list of integers */
  struct INTLIST
  {
    uint i;         /**< an integer */
    INTLIST* next; /**< remaining elements */
  };

  /** list of list of integers */
  struct INTLISTS
  {
    INTLIST* list;  /**< a list of integers */
    INTLISTS* next; /**< remaining elements */
  };

  // ---- ---- ---- ----

  static INTLISTS* cubetable[256];

  // ---- ---- ---- ----

  /** @brief converge: from two points of differing sign, converge to zero crossing */
  Point3D converge( const Point3D& p1, const Point3D& p2, CN v, Math::ImplicitFunction::func_pf_t function )
  {
    uint i = 0;
    Point3D pos, neg;
    if( v < 0 )
    {
      pos = p2;
      neg = p1;
    }
    else
    {
      pos = p1;
      neg = p2;
    }
    Point3D p;
    while( 1 ) {
      p = 0.5 * ( pos + neg );

      if( i++ == RES )
        return p;

      if( ( function( p ) ) > CN( 0.0 ) )
        pos = p;
      else
        neg = p;
    }
  }

  /**
   * setcenter: set (i,j,k) entry of table[]
   * @return 1 if already set; otherwise, set and return 0
   */
  int setcenter( CENTERLIST* table[], int i, int j, int k )
  {
    //
    const uint index( HASH( i, j, k ) );
    assert( index < HASHSIZE );

    CENTERLIST* const q( table[index] );
    for( CENTERLIST* l( q ); l != nullptr; l = l->next )
      if( ( l->i == i ) && ( l->j == j ) && ( l->k == k ) )
        return 1;

    CENTERLIST* o = new CENTERLIST;

    o->i = i; o->j = j; o->k = k; o->next = q;
    table[index] = o;
    return 0;
  }


  /** setedge: set vertex id for edge */
  void setedge( EDGELIST* table[], int i1, int j1, int k1, int i2, int j2, int k2, uint vid )
  {
    if( i1>i2 || ( i1==i2 && ( j1>j2 || ( j1==j2 && k1>k2 ) ) ) )
    {
      /** @todo here */
      int t = i1;
      i1 = i2;
      i2 = t;
      t = j1;
      j1 = j2;
      j2 = t;
      t = k1;
      k1 = k2;
      k2 = t;
    }

    const uint index( HASH( i1, j1, k1 ) + HASH( i2, j2, k2 ) );
    assert( index < HASHSIZE );

    EDGELIST* o( new EDGELIST );

    o->i1 = i1; o->j1 = j1; o->k1 = k1;
    o->i2 = i2; o->j2 = j2; o->k2 = k2;
    o->vid = vid;
    o->next = table[index];
    table[index] = o;
  }

  /**
   * getedge: return vertex id for edge;
   * @return -1 if not set
   */
  int getedge( EDGELIST* table[], int i1, int j1, int k1, int i2, int j2, int k2 )
  {
    if( ( i1>i2 ) || ( i1==i2 && ( ( j1>j2 ) || ( ( j1==j2 ) && ( k1>k2 ) ) ) ) )
    {
      /** @todo here */
      int t = i1;
      i1 = i2;
      i2 = t;
      t = j1;
      j1 = j2;
      j2 = t;
      t = k1;
      k1 = k2;
      k2 = t;
    }

    const uint index( HASH( i1, j1, k1 ) + HASH( i2, j2, k2 ) );
    assert( index < HASHSIZE );

    const EDGELIST* q( table[index] );
    for(; q != nullptr; q = q->next )
      if(
        q->i1 == i1 && q->j1 == j1 && q->k1 == k1 &&
        q->i2 == i2 && q->j2 == j2 && q->k2 == k2
      )
        return int(q->vid);
    return -1;
  }

  // ---- ---- ---- ----

  /**<*** Cubical Polygonization (optional) ****/
  static constexpr uint LB = 0;  /**< left bottom edge */
  static constexpr uint LT = 1;  /**< left top edge */
  static constexpr uint LN = 2;  /**< left near edge */
  static constexpr uint LF = 3;  /**< left far edge */
  static constexpr uint RB = 4;  /**< right bottom edge */
  static constexpr uint RT = 5;  /**< right top edge */
  static constexpr uint RN = 6;  /**< right near edge */
  static constexpr uint RF = 7;  /**< right far edge */
  static constexpr uint BN = 8;  /**< bottom near edge */
  static constexpr uint BF = 9;  /**< bottom far edge */
  static constexpr uint TN = 10; /**< top near edge */
  static constexpr uint TF = 11; /**< top far edge */

  /** edge: LB, LT, LN, LF, RB, RT, RN, RF, BN, BF, TN, TF */
  static const uint corner1[12]
  { LBN, LTN, LBN, LBF, RBN, RTN, RBN, RBF, LBN, LBF, LTN, LTF };

  static const uint corner2[12]
  { LBF, LTF, LTN, LTF, RBF, RTF, RTN, RTF, RBN, RBF, RTN, RTF };

  static const uint leftface[12]
  { B, L, L, F, R, T, N, R, N, B, T, F };

  /** face on left when going corner1 to corner2 */
  static const uint rightface[12]
  { L, T, N, L, B, R, R, F, B, F, N, T };

  // ---- ---- ---- ----

  /** nextcwedge: return next clockwise edge from given edge around given face */
  uint nextcwedge( uint edge, uint face )
  {
    switch( edge )
    {
      case LB : return ( face == L ) ? LF : BN;
      case LT : return ( face == L ) ? LN : TF;
      case LN : return ( face == L ) ? LB : TN;
      case LF : return ( face == L ) ? LT : BF;
      case RB : return ( face == R ) ? RN : BF;
      case RT : return ( face == R ) ? RF : TN;
      case RN : return ( face == R ) ? RT : BN;
      case RF : return ( face == R ) ? RB : TF;
      case BN : return ( face == B ) ? RB : LN;
      case BF : return ( face == B ) ? LB : RF;
      case TN : return ( face == T ) ? LT : RN;
      case TF : return ( face == T ) ? RT : LF;
    }
    return 0;
  }

  /** otherface: return face adjoining edge that is not the given face */
  uint otherface( uint edge, uint face )
  {
    const uint other( leftface[edge] );
    return ( face == other ) ? rightface[edge] : other;
  }

  /** makecubetable: create the 256 entry table for cubical polygonization */
  void makecubetable()
  {
    uint done[12], pos[8];
    for( uint i( 0 ); i < 256; ++i )
    {
      for( uint e( 0 ); e < 12; ++e )
        done[e] = 0;
      for( uint c( 0 ); c < 8; ++c )
        pos[c] = BIT( i, c );

      for( uint e( 0 ); e < 12; ++e )
      {
        if( !done[e] && ( pos[corner1[e]] != pos[corner2[e]] ) )
        {
          //
          INTLIST* ints( nullptr );
          INTLISTS* lists( new INTLISTS );

          //
          uint start( e ), edge( e );

          /**< get face that is to right of edge from pos to neg corner: */
          uint face = pos[corner1[e]] ? rightface[e] : leftface[e];
          while( 1 )
          {
            edge = nextcwedge( edge, face );

            done[edge] = 1;
            if( pos[corner1[edge]] != pos[corner2[edge]] )
            {
              INTLIST* tmp = ints;
              ints = new INTLIST;
              ints->i = edge;
              ints->next = tmp; /**< add edge to head of list */
              if( edge == start )
                break;
              face = otherface( edge, face );
            }
          }
          lists->list = ints; /**< add ints to head of table entry */
          lists->next = cubetable[i];
          cubetable[i] = lists;
        }
      }
    }
  }


  // ---- ---- ---- ----

  static std::uniform_real_distribution<CN> positiveNormalDistribution{ 0., 1. };

  /**< parameters, function, storage */
  struct Bloomenthal_PROCESS
  {
   public:
    Bloomenthal& b;

   public:
    const Math::ImplicitFunction::func_pf_t function;  /**< implicit surface function */

   public:
    const CN size, delta;  /**< cube size, normal delta */
    const uint bounds;        /**< cube range within lattice */
    Point3D start;              /**< start point on surface */
    CUBES* cubes;             /**< active cubes */

   public:
    Vertices& vertices;         /**< surface vertices */
    TrianglesIndices& indices;         /**< surface vertices */

   public:
    CENTERLIST* centers[HASHSIZE]{ nullptr };  /**< cube center hash table */
    CORNERLIST* corners[HASHSIZE]{ nullptr };  /**< corner value hash table */
    EDGELIST* edges[HASHSIZE]{ nullptr };      /**< edge and vertex id hash table */

   public:
    std::ranlux48_base randGenerator;

   public:
    //inline CN rand() { return positiveNormalDistribution( randGenerator ); }

    // inline CN RAND() { return positive_normal_distribution( rand_generator ); }
    inline CN rand() { return ( ::rand() & 32767 ) / 32767.; }

   public:
    Bloomenthal_PROCESS(
      Bloomenthal& _b ,
      Math::ImplicitFunction::func_pf_t function,
      CN size, uint bounds,
      CN x, CN y, CN z,
      Bloomenthal::TetrahedralMode mode,
      uint seed,
      uint limitProcess
    );
    ~Bloomenthal_PROCESS();

   public:
    inline uint insertTriangle( uint i, uint j, uint k )
    { indices.emplace_back( i, j, k ); return 1; }

   public:
    void testface( int i, int j, int k, const CUBE& old, uint face, uint c1, uint c2, uint c3, uint c4 );
    uint docube( CUBE& cube );
    uint dotet( CUBE& cube, uint c1, uint c2, uint c3, uint c4 );
    TEST find( uint sign, CN x, CN y, CN z );
    CORNER* setcorner( int i, int j, int k );
    uint vertid( const CORNER& c1, const CORNER& c2 );
    Point3D vnormal( const Point3D& point );
  };

  // ---- ---- ---- ----

  Bloomenthal_PROCESS::Bloomenthal_PROCESS(
    Bloomenthal& _b,
    Math::ImplicitFunction::func_pf_t _function,
    CN _size, uint _bounds,
    CN x, CN y, CN z,
    Bloomenthal::TetrahedralMode mode,
    uint _seed,
    uint limitProcess
  ) :
    b( _b ),
    function( _function ),
    size( _size ),
    delta( size / CN( RES * RES ) ),
    bounds( _bounds ),
    vertices( b.mVertices ),
    indices( b.mTrianglesIndice ),
    randGenerator( _seed )
  {
    //
    srand( _seed );

    //
    for( uint i( 0 ); i<HASHSIZE; ++i )
    {
      centers[i] = nullptr;
      corners[i] = nullptr;
      edges[i] = nullptr;
    }

    //
    makecubetable();

    // find point on surface, beginning search at (x, y, z):
    debug_cout << "  find point on surface, beginning search at (x, y, z)" << std::endl;
    const TEST in( find( 1, x, y, z ) );
    const TEST out( find( 0, x, y, z ) );

    if( !in.ok || !out.ok )
      throw std::runtime_error( "[Bloomenthal] Can't find starting point" );

    //
    start = converge( in.p, out.p, in.value, function );

    // push initial cube on stack:
    cubes = new CUBES();

    // set corners of initial cube :
    debug_cout << "  set corners of initial cube" << std::endl;
    for( uint n( 0 ); n < 8; ++n )
      cubes->cube.corners[n] =
        setcorner( BIT( n, 2 ), BIT( n, 1 ), BIT( n, 0 ) );

    //
    setcenter( centers, 0, 0, 0 );

    //
    uint count_loop( 0 );
    while( cubes != nullptr )   /**< Bloomenthal_PROCESS active cubes till none left */
    {
      ++count_loop;
      if( ( count_loop % 100 ) == 1 )
        debug_cout << '\r' << "  Bloomenthal_PROCESS active cubes till none left (" << std::setw( 6 ) << count_loop++ << " times)" << std::flush; //<< std::endl;

      if( count_loop> limitProcess )
      {
        debug_cout << "  Bloomenthal_PROCESS aborted at " << limitProcess << " loops" << std::endl;
        break;
      }

      CUBES* const temp( cubes );
      CUBE c( cubes->cube );

      //
      const bool noabort(
        ( mode == Bloomenthal::TetrahedralMode::WithDecomposition ) ?
        /**< either decompose into tetrahedra and polygonize: */
        dotet( c, LBN, LTN, RBN, LBF ) &&
        dotet( c, RTN, LTN, LBF, RBN ) &&
        dotet( c, RTN, LTN, LTF, LBF ) &&
        dotet( c, RTN, RBN, LBF, RBF ) &&
        dotet( c, RTN, LBF, LTF, RBF ) &&
        dotet( c, RTN, LTF, RTF, RBF )
        :
        /**< or polygonize the cube directly: */
        docube( c )
      );

      if( !noabort )
        throw std::runtime_error( "[Bloomenthal] aborted" );

      /**< pop current cube from stack */
      cubes = cubes->next;
      delete temp;

      /**< test six face directions, maybe add to stack: */
      testface( c.i - 1, c.j, c.k, c, L, LBN, LBF, LTN, LTF );
      testface( c.i + 1, c.j, c.k, c, R, RBN, RBF, RTN, RTF );

      testface( c.i, c.j - 1, c.k, c, B, LBN, LBF, RBN, RBF );
      testface( c.i, c.j + 1, c.k, c, T, LTN, LTF, RTN, RTF );

      testface( c.i, c.j, c.k - 1, c, N, LBN, LTN, RBN, RTN );
      testface( c.i, c.j, c.k + 1, c, F, LBF, LTF, RBF, RTF );
    }

    debug_cout << '\r' << "  Bloomenthal_PROCESS active cubes till none left (" << std::setw( 6 ) << count_loop << " times)" << std::endl;

  }

  // ---- ----

  /**< free all the memory we’ve allocated (except cubetable) */
  Bloomenthal_PROCESS::~Bloomenthal_PROCESS()
  {

    INTLISTS* lists, * listsnext;
    INTLIST* ints, * intsnext;
    for( uint index( 0 ); index < HASHSIZE; index++ )
    {
      CORNERLIST* lnext( nullptr );
      for( CORNERLIST* l( corners[index] ); l; l = lnext )
      {
        lnext = l->next;
        delete l; /**< free CORNERLIST */
      }

      CENTERLIST* clnext( nullptr );
      for( CENTERLIST* cl( centers[index] ); cl; cl = clnext )
      {
        clnext = cl->next;
        delete cl; /**< free CENTERLIST */
      }

      EDGELIST* edgenext( nullptr );
      for( EDGELIST* edge( edges[index] ); edge; edge = edgenext )
      {
        edgenext = edge->next;
        delete edge; /**< free EDGELIST */
      }
    }

    // delete edges;        /**< free array of EDGELIST pointers */
    // delete corners;      /**< free array of CORNERLIST  pointers */
    // delete centers;      /**< free array of CENTERLIST  pointers */

    if( b.FreeCubeTable && true )
    {
      for( uint i( 0 ); i < 256; i++ )
      {
        for( lists = cubetable[i]; lists; lists = listsnext )
        {
          listsnext = lists->next;
          for( ints = lists->list; ints; ints = intsnext ) {
            intsnext = ints->next;
            delete ints; /**< free INTLIST */
          }
          delete lists; /**< free INTLISTS */
        }
        cubetable[i] = nullptr;
      }
    }
  }

  // ---- ---- ---- ----

  /**< testface: given cube at lattice (i, j, k), and four corners of face,
   * if surface crosses face, compute other four corners of adjacent cube
   * and add new cube to cube stack */
  void Bloomenthal_PROCESS::testface( int i, int j, int k, const CUBE& old, uint face, uint c1, uint c2, uint c3, uint c4 )
  {
    static const uint facebit[6] { 2, 2, 1, 1, 0, 0 };

    CUBES* const oldcubes( cubes );

    const uint pos( ( old.corners[c1]->value > CN( 0. ) ) ? 1 : 0 );
    const uint bit( facebit[face] );

    // test if no surface crossing, cube out of bounds, or already
    // visited:
    if( ( ( old.corners[c2]->value > 0 ) == pos ) &&
        ( ( old.corners[c3]->value > 0 ) == pos ) &&
        ( ( old.corners[c4]->value > 0 ) == pos ) )
      return;

    if(
      ( i > int(bounds) ) ||
      ( j > int(bounds) ) ||
      ( k > int(bounds) )
    )
      return;

    if( setcenter( centers, i, j, k ) )
      return;

    /**< create new cube: */
    CUBE o( i, j, k );
    o.corners[FLIP( c1, bit )] = old.corners[c1];
    o.corners[FLIP( c2, bit )] = old.corners[c2];
    o.corners[FLIP( c3, bit )] = old.corners[c3];
    o.corners[FLIP( c4, bit )] = old.corners[c4];

    for( int n( 0 ); n < 8; ++n )
      if( o.corners[n] == nullptr )
        o.corners[n] = setcorner( i + BIT( n, 2 ), j + BIT( n, 1 ), k + BIT( n, 0 ) );

    /**<add cube to top of stack: */
    cubes = new CUBES;
    cubes->cube = o;
    cubes->next = oldcubes;
  }

  /**< setcorner: return corner with the given lattice location set (and cache) its function value */
  CORNER* Bloomenthal_PROCESS::setcorner( int i, int j, int k )
  {
    /**< for speed, do corner value caching here */
    CORNER* const c( new CORNER );

    const uint index( HASH( i, j, k ) );
    assert( index < HASHSIZE );

    CORNERLIST* l( corners[index] );

    c->i = i; c->position[0] = start[0] + ( CN( i ) - CN( .5 ) ) * size;
    c->j = j; c->position[1] = start[1] + ( CN( j ) - CN( .5 ) ) * size;
    c->k = k; c->position[2] = start[2] + ( CN( k ) - CN( .5 ) ) * size;

    for(; l != nullptr; l = l->next )
    {
      if( ( l->i == i ) && ( l->j == j ) && ( l->k == k ) )
      {
        c->value = l->value;
        return c;
      }
    }

    l = new CORNERLIST;

    l->i = i; l->j = j; l->k = k;
    l->value = c->value = function( c->position );
    l->next = corners[index];
    corners[index] = l;
    return c;
  }

  /**< find: search for point with value of given sign (0: neg, 1: pos) */
  TEST Bloomenthal_PROCESS::find( uint sign, CN x, CN y, CN z )
  {
    TEST test;
    CN range = size;
    test.ok = 1;
    for( uint i( 0 ); i < 10000; ++i )
    {
      test.p[0] = x + range * ( rand() - CN( 0.5 ) );
      test.p[1] = y + range * ( rand() - CN( 0.5 ) );
      test.p[2] = z + range * ( rand() - CN( 0.5 ) );
      test.value = function( test.p );
      if( sign == ( test.value > CN( 0.0 ) ) )
        return test;
      range = range * CN( 1.0005 ); /**< slowly expand search outwards */
    }
    test.ok = 0;
    return test;
  }

  // ---- ---- ---- ----

  /**
   * @brief dotet: triangulate the tetrahedron
   * @param b, c, d should appear clockwise when viewed from a
   * @return 0 if client aborts, 1 otherwise
   */
  uint Bloomenthal_PROCESS::dotet( CUBE& cube, uint c1, uint c2, uint c3, uint c4 )
  {
    const CORNER& a( *cube.corners[c1] );
    const CORNER& b( *cube.corners[c2] );
    const CORNER& c( *cube.corners[c3] );
    const CORNER& d( *cube.corners[c4] );

    uint index( 0 );
    uint apos( 0 ), bpos( 0 ), cpos( 0 ), dpos( 0 );
    uint e1( 0 ), e2( 0 ), e3( 0 ), e4( 0 ), e5( 0 ), e6( 0 );

    if( ( apos = ( a.value > CN( 0.0 ) ) ) ) index += 8;
    if( ( bpos = ( b.value > CN( 0.0 ) ) ) ) index += 4;
    if( ( cpos = ( c.value > CN( 0.0 ) ) ) ) index += 2;
    if( ( dpos = ( d.value > CN( 0.0 ) ) ) ) index += 1;

    /**< index is now 4-bit number representing one of the 16 possible cases */
    if( apos != bpos ) e1 = vertid( a, b );
    if( apos != cpos ) e2 = vertid( a, c );
    if( apos != dpos ) e3 = vertid( a, d );
    if( bpos != cpos ) e4 = vertid( b, c );
    if( bpos != dpos ) e5 = vertid( b, d );
    if( cpos != dpos ) e6 = vertid( c, d );

    /**< 14 productive tetrahedral cases (0000 and 1111 do not yield polygons */
    switch( index ) {
      case 1 : return insertTriangle( e5, e6, e3 );
      case 2 : return insertTriangle( e2, e6, e4 );
      case 3 : return insertTriangle( e3, e5, e4 ) && insertTriangle( e3, e4, e2 );
      case 4 : return insertTriangle( e1, e4, e5 );
      case 5 : return insertTriangle( e3, e1, e4 ) && insertTriangle( e3, e4, e6 );
      case 6 : return insertTriangle( e1, e2, e6 ) && insertTriangle( e1, e6, e5 );
      case 7 : return insertTriangle( e1, e2, e3 );
      case 8 : return insertTriangle( e1, e3, e2 );
      case 9 : return insertTriangle( e1, e5, e6 ) && insertTriangle( e1, e6, e2 );
      case 10 : return insertTriangle( e1, e3, e6 ) && insertTriangle( e1, e6, e4 );
      case 11 : return insertTriangle( e1, e5, e4 );
      case 12 : return insertTriangle( e3, e2, e4 ) && insertTriangle( e3, e4, e5 );
      case 13 : return insertTriangle( e6, e2, e4 );
      case 14 : return insertTriangle( e5, e3, e6 );
    }
    return 1;
  }

  // ---- ---- ---- ----

  /** docube: triangulate the cube directly, without decomposition */
  uint Bloomenthal_PROCESS::docube( CUBE& cube )
  {
    uint index( 0 );

    for( uint i( 0 ); i < 8; ++i )
      if( cube.corners[i]->value > CN( 0.0 ) )
        index += ( 1 << i );

    for( INTLISTS* polys( cubetable[index] ); polys; polys = polys->next )
    {
      uint a( 0 ), b( 0 ), count( 0 );

      for( INTLIST* edges( polys->list ); edges; edges = edges->next )
      {
        CORNER& c1( *cube.corners[corner1[edges->i]] );
        CORNER& c2( *cube.corners[corner2[edges->i]] );
        const uint c( vertid( c1, c2 ) );
        if( ++count > 2 )
          if( !insertTriangle( a, b, c ) )
            return 0;
        if( count < 3 )
          a = b;
        b = c;
      }
    }
    return 1;
  }

  // ---- ---- ---- ----

  /**<
   * @brief vertid: return index for vertex on edge:
   * c1->value and c2->value are presumed of different sign
   * return saved index if any; else compute vertex and save
   */
  uint Bloomenthal_PROCESS::vertid( const CORNER& c1, const CORNER& c2 )
  {
    {
      const int vid( getedge( edges, c1.i, c1.j, c1.k, c2.i, c2.j, c2.k ) );
      if( vid != -1 )
        return uint( vid ); /**< previously computed */
    }
    {
      const Point3D a( c1.position ), b( c2.position );

      //
      const Point3D position( converge( a, b, c1.value, function ) );
      const Point3D normal( vnormal( position ) );
      vertices.emplace_back( position, normal );

      //
      const uint vid( uint( vertices.size() ) - 1 );
      setedge( edges, c1.i, c1.j, c1.k, c2.i, c2.j, c2.k, vid );

      return vid;
    }
  }

  /** @brief vnormal: compute unit length surface normal at point */
  Point3D Bloomenthal_PROCESS::vnormal( const Point3D& point )
  {
    CN f( function( point ) );
    Point3D v(
      function( { point[0] + delta, point[1], point[2] } ) - f,
      function( { point[0], point[1] + delta, point[2] } ) - f,
      function( { point[0], point[1], point[2] + delta } ) - f
    );
    f = std::sqrt( v[0] * v[0] + v[1] * v[1] + v[2] * v[2] );
    if( Math::not_equal<CN>( f, CN( 0.0 ) ) )
      v /= f;
    return v;
  }


  /** polygonize: polygonize the implicit surface function
   * arguments are:
   * CN function (x, y, z)
   * CN x, y, z (an arbitrary 3D point)
   * the implicit surface function
   * return negative for inside, positive for outside
   * CN size
   * width of the partitioning cube
   * uint bounds
   * max. range of cubes (+/- on the three axes) from first
     cube
   * CN x, y, z
   * coordinates of a starting point on or near the surface
   * may be defaulted to 0., 0., 0.
   * int insertTriangle (i1, i2, i3)
   * uint i1, i2, i3 (indices into the vertex array)
   * Vertices vertices (the vertex array, indexed from 0)
   * called for each triangle
   * the triangle coordinates are (for i = i1, i2, i3):
   * vertices.ptr[i].position[0], [1], and [2]
   * vertices are ccw when viewed from the out (positive) side
   * in a left-handed coordinate system
   * vertex normals point outwards
   * return 1 to continue, 0 to abort
   * int mode
   * TET: decompose cube and polygonize six tetrahedra
   * NOTET: polygonize cube directly
   * returns error or nullptr
   */
  Bloomenthal::Bloomenthal(
    Math::ImplicitFunction::func_pf_t function,
    CN size, int bounds,
    CN x, CN y, CN z,
    TetrahedralMode mode,
    uint seed,
    uint limitProcess
  )
  {
    //
    if( ( size < CN( 0.000001 ) ) || ( bounds <= 0 ) )
      throw std::runtime_error( "[Bloomenthal] Need positive non-null : size & bounds" );

    if( !function )
      throw std::runtime_error( "[Bloomenthal] Need valid function" );

    //
    auto start_time( std::chrono::system_clock::now() );
    std::time_t start_ctime( std::chrono::system_clock::to_time_t( start_time ) );

    debug_cout << std::endl;
    debug_cout << "Bloomenthal_PROCESS Bloomenthal at " << std::ctime( &start_ctime );

    Bloomenthal_PROCESS p(
      *this,
      function,
      size, uint( bounds ),
      x, y, z,
      mode,
      seed,
      limitProcess
    );


    debug_cout << "  total triangles indice :" << mTrianglesIndice.size() << std::endl;
    debug_cout << "  total vertices :" << mVertices.size() << std::endl;

    auto end_time( std::chrono::system_clock::now() );
    debug_cout << "  finished in "
               << std::chrono::duration_cast<std::chrono::milliseconds>( end_time - start_time ).count() << " ms" << std::endl;

  }

  // ---- ----

  Bloomenthal::~Bloomenthal()
  { }


}

