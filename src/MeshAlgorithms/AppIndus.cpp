#include <MeshAlgorithms/AppIndus.hpp>

#include <Math/Vec.hpp>

#include <cmath>
#include <map>
#include <debug.hpp>

namespace MeshAlgorithms {
  namespace AppIndus {


    //    inline void MeshQuality::addAngleQuality( float a )
    //    {
    //#define addIfUpper( min ) if( a >= min ) {++lessAngleCount[min]; return; }
    //      addIfUpper( 3.14f )
    //      addIfUpper( ( 3.14f / 2.f ) )
    //      addIfUpper( ( 3.14f / 3.f ) )
    //      addIfUpper( ( 3.14f / 4.f ) )
    //      addIfUpper( ( 3.14f / 5.f ) )
    //      addIfUpper( ( 3.14f / 6.f ) )
    //      addIfUpper( ( 3.14f / 7.f ) )
    //      addIfUpper( ( 3.14f / 8.f ) )
    //      addIfUpper( 0.f )
    //    }

    inline void MeshQuality::addTriangleQuality( float a, float b, float c )
    {
      const float angle_coef( std::abs( a - b ) + std::abs( a - c ) );
      if( angle_coef <= 0.2f )
        ++almostEquilateralTriangleCount;
      else
        ++notEquilateralTriangleCount;
    }


    MeshQuality::MeshQuality( const Mesh& mesh )
    {
      //mesh.request_face_normals();

      //
      vertexCount = uint( mesh.n_vertices() );

      //
      //      lessAngleCount[0.f] = 0;
      //      lessAngleCount[3.14f] = 0;

      // Valence
      Mesh::ConstVertexIter v_cit( mesh.vertices_begin() );
      const Mesh::ConstVertexIter vEnd_cit( mesh.vertices_end() );
      for(; v_cit != vEnd_cit; ++v_cit )
      {
        const uint nbEdges( mesh.valence( *v_cit ) );
        ++valenceVertexCount[nbEdges];
      }

      //
      Mesh::ConstFaceIter f_cit( mesh.faces_begin() );
      const Mesh::ConstFaceIter fEnd_cit( mesh.faces_end() );
      for(; f_cit != fEnd_cit; ++f_cit )
      {
        //
        ++facesCount;

        //
        Mesh::ConstFaceVertexIter fv_cit( mesh.cfv_iter( *f_cit ) );
        if( fv_cit.is_valid() )
        {
          const Mesh::Point p1( mesh.point( *fv_cit ) );
          ++fv_cit;
          if( fv_cit.is_valid() )
          {
            const Mesh::Point p2( mesh.point( *fv_cit ) );
            ++fv_cit;
            if( fv_cit.is_valid() )
            {
              const Mesh::Point p3( mesh.point( *fv_cit ) );
              {
                const float p1Angle( Math::triangleAngle( p1, p2, p3 ) );
                const float p2Angle( Math::triangleAngle( p2, p1, p3 ) );
                const float p3Angle( Math::triangleAngle( p3, p2, p1 ) );

                ++angleVertexCount[toDegrees( p1Angle )];
                ++angleVertexCount[toDegrees( p2Angle )];
                ++angleVertexCount[toDegrees( p3Angle )];

                //                addAngleQuality( p1Angle );
                //                addAngleQuality( p2Angle );
                //                addAngleQuality( p3Angle );
                addTriangleQuality( p1Angle, p2Angle, p3Angle );
              }
            }
          }
        }
      }

      // Dihedral
      Mesh::ConstEdgeIter e_cit( mesh.edges_begin() );
      const Mesh::ConstEdgeIter eEnd_cit( mesh.edges_end() );
      for(; e_cit != eEnd_cit; ++e_cit )
        ++dihedralAngleFaceCount[toDegrees( mesh.calc_dihedral_angle( *e_cit ) )];

    }


  }
}
