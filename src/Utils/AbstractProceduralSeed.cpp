#include <Utils/AbstractProceduralSeed.hpp>

#include <random>
#include <chrono>

namespace Utils {


  std::uniform_real_distribution<float> AbstractProceduralSeed::positiveNormalValueDistribution( 0.01f, 1.f );
  std::uniform_real_distribution<float> AbstractProceduralSeed::normalPositionDistribution( -1.f, 1.f );

  //  // ---- ---- ---- ----
  //
  //  AbstractProceduralSeed::AbstractProceduralSeed()
  //  {
  //
  //
  //
  //  }
  //
  //  // ---- ----
  //
  //  float AbstractProceduralSeed::positiveNormalValue( const float coef )
  //  { return positiveNormalValue( rand_generator ) * coef; }
  //
  //  Math::Vec3f normalPosition( const float coef )
  //  { return coef * Math::Vec3f( normalPosition( rand_generator ), normalPosition( rand_generator ), normalPosition( rand_generator ) ); }
  //


  // AbstractProceduralSeed


}
