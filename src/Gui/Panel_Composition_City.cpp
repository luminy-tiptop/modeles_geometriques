#include <Gui/Panel_Composition_City.hpp>
#include <ui_Panel_Composition_City.h>

#include <Composition/City.hpp>

namespace Gui {

  static Ui::Panel_Composition_City* buildAndSetup_Panel_Composition_City( Panel_Composition_City* w )
  {
    Ui::Panel_Composition_City* const ui( new Ui::Panel_Composition_City() );
    ui->setupUi( w );
    return ui;
  }

  Panel_Composition_City::Panel_Composition_City( ::Composition::City& composition, QWidget* parent ) :
    ui( buildAndSetup_Panel_Composition_City( this ) ),
    mComposition( composition )
  {
    mRequestChangeIsLocked = true;
    // ui->checkBox_wireframe->setChecked( mComposition.mDrawWireframe );
    // ui->checkBox_transparent->setChecked( mComposition.mTransparent );
    ui->checkBox_grid->setChecked( mComposition.mPrintGrid );
    ui->doubleSpinBox_gridSpacing->setValue( double(mComposition.mGridSpacing) );

    ui->spinBox_nbConstruction->setValue( int(mComposition.mNbConstruction) );
    ui->spinBox_nbElement->setValue( int(mComposition.mNbPlace) );
   // ui->doubleSpinBox_prox->setValue( double(mComposition.mNeighborDistance) );

    mRequestChangeIsLocked = false;

  }

  Panel_Composition_City::~Panel_Composition_City()
  { delete ui;  }

  // ---- ---- ---- ----

  void Panel_Composition_City::requestChange( bool force )
  {
    //
    if( mRequestChangeIsLocked )
      return;

    //
    if( !force && !ui->checkBox_requestGenerate->isChecked() )
      return;

    //
    try {

      //
      // if( !function )
      //   throw std::runtime_error( "Unknown selected function" );

      //
      mComposition.reloadLastDemo();
      mComposition.mDoPaint = true;

    }
    catch( const std::exception& e )
    {
      // ui->outputInfo->setText( QString( e.what() ) );
      mComposition.mDoPaint = false;
    }

    mComposition.mReceiver.repaint();
  }

  // ---- ---- ---- ----

  void Panel_Composition_City::on_checkBox_wireframe_stateChanged( int v )
  { mComposition.mDrawWireframe = v; mComposition.mReceiver.repaint(); }

  void Panel_Composition_City::on_checkBox_transparent_stateChanged( int v )
  { mComposition.mTransparent = v; mComposition.mReceiver.repaint(); }

  void Panel_Composition_City::on_checkBox_grid_stateChanged( int v )
  { mComposition.mPrintGrid = v; mComposition.mReceiver.repaint(); }

  // ---- ----

  void Panel_Composition_City::on_doubleSpinBox_gridSpacing_valueChanged( double v )
  { mComposition.mGridSpacing = float(v); mComposition.updateGrid(); mComposition.mReceiver.repaint(); /*requestChange();*/ }

  // ---- ---- ---- ----

  void Panel_Composition_City::on_spinBox_nbConstruction_valueChanged( int arg1 )
  { mComposition.mNbConstruction = uint( arg1 ); requestChange(); }

  void Panel_Composition_City::on_spinBox_nbElement_valueChanged( int arg1 )
  { mComposition.mNbPlace = uint( arg1 ); requestChange(); }

  void Panel_Composition_City::on_doubleSpinBox_prox_valueChanged( double arg1 )
  { mComposition.mNeighborDistance = float(arg1); requestChange(); }

  // ---- ----

  void Panel_Composition_City::on_pushButton_generate_clicked()
  { requestChange( true ); }

}



