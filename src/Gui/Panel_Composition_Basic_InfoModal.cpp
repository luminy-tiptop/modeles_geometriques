#include <Gui/Panel_Composition_Basic_InfoModal.hpp>
#include <ui_Panel_Composition_Basic_InfoModal.h>

#include <Gui/External/qcustomplot.h>
#include <MeshAlgorithms/AppIndus.hpp>

#include <sstream>
#include <iomanip>
#include <debug.hpp>
#include <QMessageBox>

//#include <stdexcept>

namespace Gui {

  static Ui::Panel_Composition_Basic_InfoModal* buildAndSetup_UiHistogramModal( Panel_Composition_Basic_InfoModal* w )
  {
    Ui::Panel_Composition_Basic_InfoModal* const ui( new Ui::Panel_Composition_Basic_InfoModal() );
    ui->setupUi( w );
    return ui;
  }

  Panel_Composition_Basic_InfoModal::Panel_Composition_Basic_InfoModal( std::unique_ptr<MeshAlgorithms::AppIndus::MeshQuality> _q_up, QWidget* parent ) :
    QMainWindow( parent ),
    q_up( std::move( _q_up ) ),
    ui( buildAndSetup_UiHistogramModal( this ) )
  {
    setWindowTitle( "Histogramme(s) et informations sur le maillage" );
    fillInfoBox();
    on_checkBox_AnglesDiedres_stateChanged( true );
    on_checkBox_Valence_stateChanged( true );
    on_checkBox_AnglesMaillage_stateChanged( true );

  }

  Panel_Composition_Basic_InfoModal::~Panel_Composition_Basic_InfoModal()
  {
    delete ui;
  }

  // ---- ---- ---- ----

  void Panel_Composition_Basic_InfoModal::fillInfoBox()
  {
    MeshAlgorithms::AppIndus::MeshQuality& q( *q_up );
    std::stringstream msg;

    msg << "==== Basic Count" << std::endl;
    msg << "  facesCount    : " << q.facesCount << std::endl
      //<< "  surfaceQuality: " << q.surfaceQuality << std::endl
        << "  vertexCount   : " << q.vertexCount << std::endl
        << std::endl;

    //    msg << "==== Angle Count" << std::endl;
    //    {
    //      float lastAngle( 0.f );
    //      uint lastCount( 0 );
    //      for( const std::pair<const float, uint>& aSupCount : q.lessAngleCount ) {
    //        if( lastCount != 0 )
    //          msg << "  " << lastAngle << " <= " << lastCount << " < " << aSupCount.first << std::endl;
    //        lastAngle = aSupCount.first;
    //        lastCount = aSupCount.second;
    //      }
    //      if( lastCount != 0 )
    //        msg << "  " << lastAngle << " <= " << lastCount << std::endl;
    //    }
    //    msg << std::endl;

    const float equilateralPourcent(
      std::round( ( q.almostEquilateralTriangleCount / float(q.facesCount) ) * 10000.f ) / 10.f
    );

    msg << "==== Triangle quality Count"  << std::endl;
    msg << "  almostEquilateral   : " << q.almostEquilateralTriangleCount << std::endl
        << "  notEquilateral      : " << q.notEquilateralTriangleCount << std::endl
        << "  equilateralPourcent : " << equilateralPourcent  << " %" << std::endl
        << std::endl;

    ui->plainText->setPlainText( QString::fromStdString( msg.str() ) );
  }


  // ---- ---- ---- ----

  void Panel_Composition_Basic_InfoModal::setupPlot_Valence( QCustomPlot& customPlot )
  {
    ui->checkBox_Valence->setChecked( true );
    MeshAlgorithms::AppIndus::MeshQuality& q( *q_up );

    //
    x.clear();
    y.clear();
    x.reserve( int(q.valenceVertexCount.size() ) );
    y.reserve( int(q.valenceVertexCount.size() ) );

    //
    customPlot.clearGraphs();

    // add two new graphs and set their look:
    customPlot.addGraph();
    customPlot.graph()->setLineStyle( QCPGraph::lsImpulse );
    //customPlot.graph()->setName( );
    // customPlot.graph()->setPen( QPen( Qt::blue ) ); // line color blue for first graph
    // customPlot.graph()->setBrush( QBrush( QColor( 0, 0, 255, 20 ) ) ); // first graph will be filled with translucent blue
    customPlot.graph()->setScatterStyle( QCPScatterStyle( QCPScatterStyle::ssCircle, 5 ) );

    //
    customPlot.xAxis->setLabel( "Nombre d'arrêtes voisines" );
    customPlot.yAxis->setLabel( "Occurence des valances" );
    for( const auto& valence_pair : q.valenceVertexCount ) {
      x.push_back( double(valence_pair.first) );
      y.push_back( double(valence_pair.second) );
    }

    //
    customPlot.graph()->setData( x, y );
    customPlot.graph()->rescaleAxes();

    //
    customPlot.setInteractions( QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables );
    customPlot.replot();
  }

  void Panel_Composition_Basic_InfoModal::setupPlot_DihedralAngles( QCustomPlot& customPlot )
  {
    ui->checkBox_AnglesDiedres->setChecked( true );
    MeshAlgorithms::AppIndus::MeshQuality& q( *q_up );

    //
    x.clear();
    y.clear();
    x.reserve( int(q.dihedralAngleFaceCount.size() ) );
    y.reserve( int(q.dihedralAngleFaceCount.size() ) );

    //
    customPlot.clearGraphs();

    // add two new graphs and set their look:
    customPlot.addGraph();
    customPlot.graph()->setLineStyle( QCPGraph::lsImpulse );
    //customPlot.graph()->setName( );
    customPlot.graph()->setPen( QPen( QColor( 0, 0, 255, 155 ) ) );
    // customPlot.graph()->setPen( QPen( Qt::blue ) ); // line color blue for first graph
    // customPlot.graph()->setBrush( QBrush( QColor( 0, 0, 255, 20 ) ) ); // first graph will be filled with translucent blue
    customPlot.graph()->setScatterStyle( QCPScatterStyle( QCPScatterStyle::ssCircle, 5 ) );

    //
    customPlot.xAxis->setLabel( "Angle (en degré)" );
    customPlot.yAxis->setLabel( "Occurence des angles Dièdres" );
    for( const auto& dihedralAngle_pair : q.dihedralAngleFaceCount ) {
      x.push_back( double(dihedralAngle_pair.first) );
      y.push_back( double(dihedralAngle_pair.second) );
    }

    //
    customPlot.graph()->setData( x, y );
    customPlot.graph()->rescaleAxes();

    //
    customPlot.setInteractions( QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables );
    customPlot.replot();
  }

  void Panel_Composition_Basic_InfoModal::setupPlot_MeshAngles( QCustomPlot& customPlot )
  {
    ui->checkBox_AnglesMaillage->setChecked( true );
    MeshAlgorithms::AppIndus::MeshQuality& q( *q_up );

    //
    x.clear();
    y.clear();
    x.reserve( int(q.angleVertexCount.size() ) );
    y.reserve( int(q.angleVertexCount.size() ) );

    //
    customPlot.clearGraphs();

    // add two new graphs and set their look:
    customPlot.addGraph();
    customPlot.graph()->setLineStyle( QCPGraph::lsImpulse );
    //customPlot.graph()->setName( );
    customPlot.graph()->setPen( QPen( QColor( 0, 0, 255, 100 ) ) );
    // customPlot.graph()->setPen( QPen( Qt::blue ) ); // line color blue for first graph
    // customPlot.graph()->setBrush( QBrush( QColor( 0, 0, 255, 20 ) ) ); // first graph will be filled with translucent blue
    customPlot.graph()->setScatterStyle( QCPScatterStyle( QCPScatterStyle::ssCircle, 5 ) );

    //
    customPlot.xAxis->setLabel( "Angle (en degré)" );
    customPlot.yAxis->setLabel( "Occurence des angles au sein du maillage" );
    for( const auto& angleQuality_pair : q.angleVertexCount ) {
      x.push_back( double(angleQuality_pair.first) );
      y.push_back( double(angleQuality_pair.second) );
    }

    //
    customPlot.graph()->setData( x, y );
    customPlot.graph()->rescaleAxes();

    //
    customPlot.setInteractions( QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables );
    customPlot.replot();
  }

  // ---- ---- ---- ----

  void Panel_Composition_Basic_InfoModal::autoCorrectCheckBox()
  {
    uint countChecked( 0 );
    countChecked += ui->checkBox_Valence->isChecked();
    countChecked += ui->checkBox_AnglesDiedres->isChecked();
    countChecked += ui->checkBox_AnglesMaillage->isChecked();
    if( countChecked == 1 )
    {
      const static auto fixCheckBox([]( auto& cb )
        { if( cb.isChecked() ) cb.setEnabled( false ); }
      );
      fixCheckBox( *ui->checkBox_Valence );
      fixCheckBox( *ui->checkBox_AnglesDiedres );
      fixCheckBox( *ui->checkBox_AnglesMaillage );
    }
    else
    {
      ui->checkBox_AnglesMaillage->setEnabled( true );
      ui->checkBox_AnglesDiedres->setEnabled( true );
      ui->checkBox_Valence->setEnabled( true );
    }
  }

  // ---- ----

  void Panel_Composition_Basic_InfoModal::emplaceItem( std::unique_ptr<QCustomPlot>& var, void ( Panel_Composition_Basic_InfoModal::* f )( QCustomPlot& p ) )
  {
    if( !var ) var = std::make_unique<QCustomPlot>( this );
    ( this->*f )( *var );
    ui->horizontalWidgetsLayout->addWidget( var.get() );
    var->setHidden(false);
  }

  void Panel_Composition_Basic_InfoModal::removeItem( std::unique_ptr<QCustomPlot>& var )
  {
    if( !var ) return;
    ui->horizontalWidgetsLayout->removeWidget( var.get() );
    var->setHidden(true);
    ui->horizontalWidgetsLayout->update();
  }

  // ---- ----

  void Panel_Composition_Basic_InfoModal::on_checkBox_AnglesDiedres_stateChanged( int arg1 )
  {
    autoCorrectCheckBox();
    if( arg1 ) emplaceItem( plot_dihedralAngles, &Panel_Composition_Basic_InfoModal::setupPlot_DihedralAngles );
    else removeItem( plot_dihedralAngles );
  }

  void Panel_Composition_Basic_InfoModal::on_checkBox_Valence_stateChanged( int arg1 )
  {
    autoCorrectCheckBox();
    if( arg1 ) emplaceItem( plot_valence, &Panel_Composition_Basic_InfoModal::setupPlot_Valence );
    else removeItem( plot_valence );
  }

  void Panel_Composition_Basic_InfoModal::on_checkBox_AnglesMaillage_stateChanged( int arg1 )
  {
    autoCorrectCheckBox();
    if( arg1 ) emplaceItem( plot_meshAngles, &Panel_Composition_Basic_InfoModal::setupPlot_MeshAngles );
    else removeItem( plot_meshAngles );
  }


}



