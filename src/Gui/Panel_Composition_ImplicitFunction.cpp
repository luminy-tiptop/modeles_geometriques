#include <Gui/Panel_Composition_ImplicitFunction.hpp>
#include <ui_Panel_Composition_ImplicitFunction.h>

#include <Composition/Basic.hpp>
#include <Composition/ImplicitFunction.hpp>

#include <Math/ImplicitFunction.hpp>

namespace Gui {

  static Ui::Panel_Composition_ImplicitFunction* buildAndSetup_Panel_Composition_ImplicitFunction( Panel_Composition_ImplicitFunction* w )
  {
    Ui::Panel_Composition_ImplicitFunction* const ui( new Ui::Panel_Composition_ImplicitFunction() );
    ui->setupUi( w );
    return ui;
  }

  Panel_Composition_ImplicitFunction::Panel_Composition_ImplicitFunction( ::Composition::ImplicitFunction& composition, QWidget* parent ) :
    ui( buildAndSetup_Panel_Composition_ImplicitFunction( this ) ),
    mComposition( composition )
  {
    mRequestChangeIsLocked = true;
    ui->checkBox_wireframe->setChecked( mComposition.mDrawWireframe );
    ui->checkBox_transparent->setChecked( mComposition.mTransparent );
    ui->checkBox_indicesMode->setChecked( mComposition.mIndicesMode );
    ui->checkBox_changeSeed->setChecked( mComposition.mChangeSeed );
    mRequestChangeIsLocked = false;
  }

  Panel_Composition_ImplicitFunction::~Panel_Composition_ImplicitFunction()
  { delete ui;  }

  // ---- ---- ---- ----

  void Panel_Composition_ImplicitFunction::requestChange( bool force )
  {
    //
    if( mRequestChangeIsLocked )
      return;

    //
    ui->outputInfo->setText( " " );

    //
    if( !force && !ui->checkBox_requestGenerate->isChecked() )
      return;

    //
    using namespace Math;
    const QString functionName( ui->comboBox_function->currentText() );
    ImplicitFunction::func_pf_t function( nullptr );
    if( functionName == "Sphere" )
      function = ImplicitFunction::sphere;
    else if( functionName == "Blob" )
      function = ImplicitFunction::blob;
    else if( functionName == "Torus" )
      function = ImplicitFunction::torus;
    else if( functionName == "Wiffle cube" )
      function = ImplicitFunction::wiffle_cube;

    //
    try {

      //
      if( !function )
        throw std::runtime_error( "Unknown selected function" );

      //
      mComposition.compute(
        function,
        ui->doubleSpinBox_size->value(),
        ui->spinBox_bounds->value(),
        ui->doubleSpinBox_x->value(),
        ui->doubleSpinBox_y->value(),
        ui->doubleSpinBox_z->value(),
        ui->checkBox_useTetrahedral->isChecked(),
        uint( ui->spinBox_limitProcess->value() )
      );

      //
      ui->outputInfo->setText( "Finish" );
      mComposition.mDoPaint = true;

    }
    catch( const std::exception& e )
    {
      ui->outputInfo->setText( QString( e.what() ) );
      mComposition.mDoPaint = false;
    }

    mComposition.mReceiver.repaint();
  }

  // ---- ---- ---- ----

  void Panel_Composition_ImplicitFunction::on_checkBox_wireframe_stateChanged( int v )
  { mComposition.mDrawWireframe = v; mComposition.mReceiver.repaint(); }

  void Panel_Composition_ImplicitFunction::on_checkBox_transparent_stateChanged( int v )
  { mComposition.mTransparent = v; mComposition.mReceiver.repaint(); }

  void Gui::Panel_Composition_ImplicitFunction::on_checkBox_indicesMode_stateChanged( int v )
  { mComposition.mIndicesMode = v; mComposition.mReceiver.repaint(); }

  void Gui::Panel_Composition_ImplicitFunction::on_checkBox_changeSeed_stateChanged( int v )
  { mComposition.mChangeSeed = v; mComposition.mReceiver.repaint(); }


  // ---- ---- -

  void Panel_Composition_ImplicitFunction::on_doubleSpinBox_size_valueChanged( double )
  { requestChange(); }

  void Panel_Composition_ImplicitFunction::on_spinBox_bounds_valueChanged( int )
  { requestChange(); }

  void Panel_Composition_ImplicitFunction::on_spinBox_limitProcess_valueChanged( int  )
  { requestChange(); }

  // ---- ---- ---- ----

  void Panel_Composition_ImplicitFunction::setDefaultPreset( bool forceRefresh )
  {
    mRequestChangeIsLocked = true;

    const QString functionName( ui->comboBox_function->currentText() );
    if( functionName == "Sphere" )
    {
      ui->doubleSpinBox_size->setValue( 0.1 );
      ui->spinBox_bounds->setValue( 20 );
      ui->doubleSpinBox_x->setValue( 0. );
      ui->doubleSpinBox_y->setValue( 0. );
      ui->doubleSpinBox_z->setValue( 0. );
      ui->checkBox_useTetrahedral->setChecked( false );
    }
    else if( functionName == "Torus" || functionName == "Blob" )
    {
      ui->doubleSpinBox_size->setValue( 0.05 );
      ui->spinBox_bounds->setValue( 20 );
      ui->doubleSpinBox_x->setValue( 0. );
      ui->doubleSpinBox_y->setValue( 0. );
      ui->doubleSpinBox_z->setValue( 0. );
      ui->checkBox_useTetrahedral->setChecked( false );
    }
    else if( functionName == "Wiffle cube" )
    {
      ui->doubleSpinBox_size->setValue( 0.15 );
      ui->spinBox_bounds->setValue( 50 );
      ui->doubleSpinBox_x->setValue( 0. );
      ui->doubleSpinBox_y->setValue( 0. );
      ui->doubleSpinBox_z->setValue( 0. );
      ui->checkBox_useTetrahedral->setChecked( false );
    }

    mRequestChangeIsLocked = false;
    requestChange( forceRefresh );
  }

  // ---- ---- ---- ----

  void Panel_Composition_ImplicitFunction::on_comboBox_function_activated( const QString& )
  { setDefaultPreset(); }

  void Panel_Composition_ImplicitFunction::on_pushButton_defaultFunctionPreset_clicked()
  { setDefaultPreset(); }

  // ---- ---- ---- ----

  void Panel_Composition_ImplicitFunction::on_checkBox_useTetrahedral_stateChanged( int arg1 )
  { setDefaultPreset(); }

  // ---- ---- ---- ----

  void Panel_Composition_ImplicitFunction::on_doubleSpinBox_x_valueChanged( double )
  { requestChange(); }

  void Panel_Composition_ImplicitFunction::on_doubleSpinBox_y_valueChanged( double )
  { requestChange(); }

  void Panel_Composition_ImplicitFunction::on_doubleSpinBox_z_valueChanged( double )
  { requestChange(); }

  // ---- ---- ---- ----

  void Panel_Composition_ImplicitFunction::on_pushButton_generate_clicked()
  { requestChange( true ); }

}
