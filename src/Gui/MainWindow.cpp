#include <Gui/MainWindow.hpp>
#include <ui_MainWindow.h>

#include <Composition/CompositionList.hpp>

#include <Composition/Basic.hpp>
#include <Composition/ImplicitFunction.hpp>
#include <Composition/City.hpp>

#include <QVector3D>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>

#include <iostream>
#include <fstream>
#include <debug.hpp>

namespace Gui {

  static Ui::MainWindow* buildAndSetup_UiMainWindow( MainWindow* w )
  {
    Ui::MainWindow* const ui( new Ui::MainWindow() );
    ui->setupUi( w );
    return ui;
  }

  MainWindow::MainWindow( const QApplication& app, QWidget* parent ) :
    QMainWindow( parent ),
    ui( buildAndSetup_UiMainWindow( this ) ),
    mCompositions( Composition::CompositionList::get() )
  {
    setWindowTitle( app.applicationName() + " v" + app.applicationVersion() );
    // mCompositionBasic( ui->widget->addComposition<::Composition::Basic>() )
    //setCompositionToUi( mCompositionBasic );
  }

  MainWindow::~MainWindow()
  {
    delete ui;
  }

  void MainWindow::showEvent( QShowEvent* event )
  {
    // call whatever your base class is!
    if( event->spontaneous() || is_initialized )
      return;

    // When MeshViewerWidget is initialized
    //on_actionOpenInternalBunny_triggered();
    on_actionGenerateCity_triggered();

    //
    is_initialized = true;
  }

  // ---- ---- ---- ----
  void MainWindow::setComposition( Composition::Composition_up c_up )
  {
    mCompositions.clear();
    Composition::AbstractComposition& c( mCompositions.addComposition( std::move( c_up ) ) );
    setActiveCompositionToUi( &c );
    mCompositions.repaint();
  }

  // ---- ---- ---- ----

  void MainWindow::clearComposition()
  {
    mCompositions.clear();
    setActiveCompositionToUi( nullptr );
  }


  void MainWindow::setActiveCompositionToUi( Composition::AbstractComposition* _currentComposition_p  )
  {
    while( QLayoutItem* item = ui->rightSide->takeAt( 0 ) )
      ui->rightSide->removeItem( item );
    ui->rightSide->update();

    currentComposition_p = _currentComposition_p;
    if( currentComposition_p == nullptr )
      return;

    QWidget* widget_p( currentComposition_p->getAttachedWidget() );
    QMenu* menu_p( currentComposition_p->getAttachedMenu() );

    if( widget_p )
    {
      ui->rightSide->addWidget( widget_p );
      // widget_p->setParent( ui->rightSide );
    }

    if( menu_p )
    {
      debug_cout << "Fix menu composition" << std::endl;
      //ui->menuComposition->addMenu( menu_p );
      //menu_p->setParent( this );
    }
  }

  // ---- ---- ---- ----

  void MainWindow::loadExternalMeshByPopup( const bool importTexture )
  {
    const QString filePath_q
    {
      QFileDialog::getOpenFileName(
        this,
        "Load Mesh", "",
        "OBJ file (*.obj)"    /**< @todo ;;STL file (.stl);; ...  */
      )
    };
    if( filePath_q.isNull() )    // no file selection
      return;

    //
    try{

      //
      const std::string path( filePath_q.toStdString() );

      //
      QFileInfo file_info( filePath_q );
      if( !file_info.exists() )
        throw std::runtime_error( "File \"" + path + "\" not exist" );
      if( file_info.isDir() )
        throw std::runtime_error( "\"" + path + "\" it's not a file" );

      //
      const std::string baseMeshDirectory( file_info.dir().path().toStdString() );

      //
      std::cout << "external loading from : " << path << std::endl;
      auto c_up( mCompositions.build<Composition::Basic>() );
      c_up->loadMesh( path, baseMeshDirectory, importTexture );
      setComposition( std::move( c_up ) );
    }
    catch( const std::exception& e )
    {
      QMessageBox::critical(
        this, "Mesh file loading",
        "Error during mesh file " + filePath_q + " loading : \n" + QString( e.what() ),
        QMessageBox::Cancel,
        QMessageBox::Cancel
      );
    }
  }

  // ---- ---- ---- ----

  void MainWindow::on_actionOpenMesh_triggered()
  { loadExternalMeshByPopup( false ); }

  void Gui::MainWindow::on_actionOpenTexturedMesh_triggered()
  { loadExternalMeshByPopup( true ); }

  // ---- ----

  void MainWindow::on_actionOpenInternalBunny_triggered()
  {
    auto c_up( mCompositions.build<Composition::Basic>() );
    c_up->loadInternalMesh( "bunnyLowPoly.obj" );
    setComposition( std::move( c_up ) );
  }

  // ---- ---- ---- ----

  void MainWindow::on_actionComputeInternalTriangle_triggered()
  {
    static const std::vector<QVector3D> triangle_data
    {
      { -1.0f, -1.0f, 0.0f },
      { 1.0f, -1.0f, 0.0f },
      { 0.0f,  1.0f, 0.0f },
    };
    auto c_up( mCompositions.build<Composition::Basic>() );
    c_up->loadMesh( triangle_data );
    setComposition( std::move( c_up ) );
  }


  // ---- ---- ---- ----

  void MainWindow::on_actionGenerateBuilding_triggered()
  {
    auto c_up( mCompositions.build<Composition::City>() );
    c_up->loadOneConstructionDemo();
    setComposition( std::move( c_up ) );
  }

  void MainWindow::on_actionGenerateCity_triggered()
  {
    auto c_up( mCompositions.build<Composition::City>() );
    c_up->loadDefaultDemo();
    setComposition( std::move( c_up ) );
  }

  // ---- ---- ---- ----

  void MainWindow::on_actionGenerateSurfaceImplicit_triggered()
  { setComposition( mCompositions.build<Composition::ImplicitFunction>() ); }

}

