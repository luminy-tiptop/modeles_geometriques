#include <Gui/Panel_Composition_Basic.hpp>
#include <ui_Panel_Composition_Basic.h>

#include <Composition/Basic.hpp>
#include <MeshAlgorithms/SimpleColorIt.hpp>
#include <MeshAlgorithms/MeshSmoothing.hpp>
#include <MeshAlgorithms/MeshRepair.hpp>
#include <MeshAlgorithms/AppIndus.hpp>

#include <Gui/Panel_Composition_Basic_InfoModal.hpp>
#include <Gui/QtTricks.hpp>

#include <debug.hpp>


namespace Gui {

  using RenderMode = ::Composition::Basic::RenderMode;

  // ---- ---- ---- ----

  static Ui::Panel_Composition_Basic* buildAndSetup_Panel_Composition_Basic( Panel_Composition_Basic* w )
  {
    Ui::Panel_Composition_Basic* const ui( new Ui::Panel_Composition_Basic() );
    ui->setupUi( w );
    return ui;
  }

  Panel_Composition_Basic::Panel_Composition_Basic( ::Composition::Basic& composition, QWidget* parent ) :
    QWidget( parent ),
    ui( buildAndSetup_Panel_Composition_Basic( this ) ),
    mComposition( composition )
  {
    //
    Gui::QtTricks::setCollapsedAndCheck( false, ui->groupBox_SmoothLB );
    Gui::QtTricks::setCollapsedAndCheck( false, ui->groupBox_Repair );

    //
    applyTests.emplace( "ColorNeighbour", std::bind( &Panel_Composition_Basic::applyTest, this, MeshAlgorithms::SimpleColorIt::colorNeighbour ) );
    applyTests.emplace( "ColorVertices", std::bind( &Panel_Composition_Basic::applyTest, this, MeshAlgorithms::SimpleColorIt::colorVertices ) );
    applyTests.emplace( "ColorCurves", std::bind( &Panel_Composition_Basic::applyTest, this, MeshAlgorithms::SimpleColorIt::colorCurves ) );

    //
    for( auto& apply : applyTests )
      ui->comboBox_applyTests->addItem( apply.first );
  }

  Panel_Composition_Basic::~Panel_Composition_Basic()
  {
    delete ui;
  }

  // ---- ---- ---- ----

  void Panel_Composition_Basic::refreshUi()
  {
    RenderMode actualMode( mComposition.actualMode() );

    //
    if( actualMode == RenderMode::NoRender )
    { ui->groupBox->setDisabled( true ); return; }

    //
    ui->groupBox->setDisabled( false );

    //
    ui->checkBox_drawWireframe->setChecked( mComposition.drawWireframe() );

    //
    RenderMode compatibleMode( mComposition.compatibleMode() );
    ui->radioButton_shaderVertex->setDisabled( !any( RenderMode::Vertex& compatibleMode ) );
    ui->radioButton_shaderVertexColor->setDisabled( !any( RenderMode::VertexColor& compatibleMode ) );
    ui->radioButton_shaderVertexUv->setDisabled( !any( RenderMode::VertexUvTexture& compatibleMode ) );
  }

  void Panel_Composition_Basic::refreshUiRenderModeChoice()
  {
    refreshUi();
    RenderMode actualMode( mComposition.actualMode() );
    switch( actualMode )
    {
      case RenderMode::Vertex :
        ui->radioButton_shaderVertex->setChecked( true );
        break;
      case RenderMode::VertexColor :
        ui->radioButton_shaderVertexColor->setChecked( true );
        break;
      case RenderMode::VertexUvTexture :
        ui->radioButton_shaderVertexUv->setChecked( true );
        break;
      default :
        ui->radioButton_shaderVertex->setChecked( false );
        ui->radioButton_shaderVertexColor->setChecked( false );
        ui->radioButton_shaderVertexUv->setChecked( false );
    }

  }

  // ---- ---- ---- ----

  void Panel_Composition_Basic::on_checkBox_drawWireframe_stateChanged( int arg1 )
  {
    mComposition.drawWireframe( bool(arg1) );
    mComposition.mReceiver.repaint();
  }

  // ---- ----

  void Panel_Composition_Basic::on_radioButton_shaderVertex_clicked()
  {
    mComposition.setActualMode( RenderMode::Vertex );
    mComposition.reloadMesh();
  }

  void Panel_Composition_Basic::on_radioButton_shaderVertexColor_clicked()
  {
    mComposition.setActualMode( RenderMode::VertexColor );
    mComposition.reloadMesh();
  }

  void Panel_Composition_Basic::on_radioButton_shaderVertexUv_clicked()
  {
    mComposition.setActualMode( RenderMode::VertexUvTexture );
    mComposition.reloadMesh();
  }

  // ---- ---- ---- ----

  void Panel_Composition_Basic::applyTest( const applyActionMesh_t& actionOnMesh )
  {
    //
    Mesh* mesh_p( mComposition.getAttachedOpenMesh() );
    if( !mesh_p ) return;

    //
    actionOnMesh( *mesh_p );

    //
    mComposition.setActualMode( RenderMode::VertexColor );
    mComposition.reloadMesh();
  }


  // ---- ----

  void Panel_Composition_Basic::on_pushButton_applyTest_clicked()
  {
    const QString name( ui->comboBox_applyTests->currentText() );
    auto find_it( applyTests.find( name ) );
    if( find_it == applyTests.end() )
      debug_cout << "Not find Apply : " << name.toStdString() << std::endl;
    else
      find_it->second();
  }

  // ---- ----

  void Panel_Composition_Basic::on_pushButton_Info_clicked()
  {
    Mesh* mesh_p( mComposition.getAttachedOpenMesh() );
    if( !mesh_p ) return;

    info_up = std::make_unique<Panel_Composition_Basic_InfoModal>(
      std::make_unique<MeshAlgorithms::AppIndus::MeshQuality>( *mesh_p ),
      this
    );
    info_up->resize( 1024, 340 );
    info_up->show();

  }

  // ---- ----

  void Panel_Composition_Basic::on_groupBox_SmoothLB_clicked( bool checked )
  { Gui::QtTricks::setCollapsed( checked, ui->groupBox_SmoothLB ); }

  void Panel_Composition_Basic::on_pushButton_SmoothLB_clicked()
  {
    Mesh* mesh_p( mComposition.getAttachedOpenMesh() );
    if( !mesh_p ) return;


    try{
      using namespace MeshAlgorithms::MeshSmoothing;
      LaplaceBeltramiOperateurApproximationCotangentielle op(
        float(ui->doubleSpinBox_coefSmoothLB->value() ),
        float(ui->doubleSpinBox_limitDeltaSmoothLB->value() )
      );
      doSmooth( *mesh_p, op );
      mComposition.reloadMesh();
    }
    catch( const std::exception& e )
    {
      debug_cout << "Error:" << e.what() << std::endl;
    }
  }

  // ---- ----

  void Panel_Composition_Basic::on_groupBox_Repair_clicked( bool checked )
  { Gui::QtTricks::setCollapsed( checked, ui->groupBox_Repair ); }

  void Panel_Composition_Basic::on_pushButton_repair_clicked()
  {
    Mesh* mesh_p( mComposition.getAttachedOpenMesh() );
    if( !mesh_p ) return;

    try{
      MeshAlgorithms::MeshRepair::doRepair( *mesh_p );
      mComposition.reloadMesh();
    }
    catch( const std::exception& e )
    {
      debug_cout << "Error:" << e.what() << std::endl;
    }
  }


}
