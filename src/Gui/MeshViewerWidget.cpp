#include <Gui/MeshViewerWidget.hpp>

#include <Composition/CompositionList.hpp>

#include <stdexcept>

namespace Gui {


  MeshViewerWidget::MeshViewerWidget( QWidget* _parent ) :
    QOpenGLWidget( _parent ),
    mCompositions( Composition::CompositionList::get() )
  {
    mCompositions.setDefaultReceiver( mCompositionReceiver );
    mCamera.reset();

    // Specify an OpenGL 3.3 format using the Core profile.
    // That is, no old-school fixed pipeline functionality
    QSurfaceFormat glFormat;
    glFormat.setVersion( 3, 3 );
    glFormat.setProfile( QSurfaceFormat::CoreProfile ); // Requires >=Qt-4.8.0
    //glFormat.setSampleBuffers( true );
    setFormat( glFormat );

    setMouseTracking( true );
    setFocus();
  }

  // ---- ---- ---- ----

  MeshViewerWidget::Camera::Camera()
  {
    // default View
    {
      Matrices::ViewLookAt defaultView( view.getDefault() );
      defaultView.position.setZ( 25.f );
      defaultView.position.setY( 15.f );
      view.setDefault( defaultView );
      view.reset();
    }
  }

  // ---- ---- ---- ----

  void MeshViewerWidget::initializeGL()
  {
    if( !initializeOpenGLFunctions() )
      throw std::runtime_error( "OpenGL fonctions doesn't initialize properly" );

    //
    glEnable( GL_MULTISAMPLE ); // for Anti-Aliasing
    glEnable( GL_DEPTH_TEST );

    // Configure compositions
    mCompositions.initialiseGL( this, this );

    //
    glClearColor( 1.0, 1.0, 1.0, 1.0 );
  }

  void MeshViewerWidget::resizeGL( int _w, int _h )
  {
    mCamera.projection.aspect = float(_w) / float(_h >= 1.f ? _h : 1.f);
    mCamera.projection.compute();
    glViewport( 0, 0, _w, _h );
    repaint();
  }

  void MeshViewerWidget::paintGL()
  {
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    QMatrix4x4 mvp( mCamera.compute() );

    for( Composition::Composition_up& composition : mCompositions )
    {
      if( composition )
      {
        // if( !composition->isInitializedGL() )
        //   composition->initialiseGL( *this );
        if( composition->doPaint() )
          composition->paintGL( *this, mvp );
      }
    }
  }

  // ---- ---- ---- ----


  void MeshViewerWidget::mousePressEvent( QMouseEvent* event )
  {
    mMouseLastPosition = event->pos();

    // //
    mCamera.modelTrackballTracker_up =
      std::make_unique<Matrices::ModelUtils::TrackBall::Tracker>(
        mCamera.modelTrackball.newTrackPositionFromScreen(
          QVector2D( event->pos() ),
          QVector2D( width(), height() )
        )
      );
  }

  void MeshViewerWidget::mouseMoveEvent( QMouseEvent* event )
  {
    static const float mouse_ratio( 0.005f );

    //
    const QPoint mouse_new_position( event->pos() );

    //
    const QVector2D mouse_distance(
      mouse_new_position.x() - mMouseLastPosition.x(),
      mouse_new_position.y() - mMouseLastPosition.y()
    );

    //
    mMouseLastPosition = mouse_new_position;

    if( ( event->buttons() == ( Qt::LeftButton + Qt::MidButton ) ) || ( event->buttons() == Qt::LeftButton && event->modifiers() == Qt::ControlModifier ) )
    {
      const float h( height() );
      const float value_y( mouse_distance.y() * 3.f / h );
      mCamera.view.position += QVector3D( 0.f, 0.f, value_y );
      mCamera.view.compute();
    }
    else if( ( event->buttons() == Qt::MidButton ) || ( event->buttons() == Qt::LeftButton && event->modifiers() == Qt::AltModifier ) )
    {
      mCamera.model.position += QVector3D( mouse_distance.x() * mouse_ratio, -mouse_distance.y() * mouse_ratio, 0.f );
      mCamera.model.compute();
      /**< @todo implement real mouse 3d mouvement */
    }
    else if( event->buttons() == Qt::RightButton )
    {
      // right click move ?
    }
    else if( event->buttons() == Qt::LeftButton )
    {
      if( mCamera.modelTrackballTracker_up )
        mCamera.modelTrackballTracker_up->move( QVector2D( event->pos() ) );

      // axes vector rotation :
      //      mCamera.model.rotation += QVector3D( mouse_distance.y() * mouse_ratio, mouse_distance.x() * mouse_ratio, 0.f );
      //      mCamera.model.compute();
    }
    repaint();
  }

  void MeshViewerWidget::mouseReleaseEvent( QMouseEvent* /* _event */ )
  {
    mCamera.modelTrackballTracker_up.release();
  }

  void MeshViewerWidget::wheelEvent( QWheelEvent* _event )
  {
    float d = -float(_event->delta() ) / 120.f * 0.2f;
    mCamera.view.position += QVector3D( 0.f, 0.f, d );
    mCamera.view.compute();
    repaint();
    _event->accept();
  }


}
