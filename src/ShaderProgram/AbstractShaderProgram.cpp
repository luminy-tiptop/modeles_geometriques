#include <ShaderProgram/AbstractShaderProgram.hpp>

namespace ShaderProgram {

  void AbstractShaderProgram::buildProgram( const QString & vert_path, const QString & frag_path )
  {
    //
    program.create();

    // Compile vertex shader
    if ( !program.addShaderFromSourceFile( QOpenGLShader::Vertex, vert_path ) )
      throw std::runtime_error( ( "Can't compile vertex file : " + vert_path ).toStdString() );

    // Compile fragment shader
    if ( !program.addShaderFromSourceFile( QOpenGLShader::Fragment, frag_path ) )
      throw std::runtime_error( ( "Can't compile vertex file : " + frag_path ).toStdString() );

    // Link shader pipeline
    if ( !program.link() )
      throw std::runtime_error( ( "Can't link program of vertex(" + vert_path + ") frag(" + frag_path + ")" ).toStdString() );

    // Bind shader pipeline for use
    if ( !program.bind() )
      throw std::runtime_error( ( "Can't bind program of vertex(" + vert_path + ") frag(" + frag_path + ")" ).toStdString() );

  }

  // ---- ----

  void AbstractShaderProgram::bind()
  {
    program.bind();
    vao.bind();
  }

  void AbstractShaderProgram::release()
  {
    vao.release();
    program.release();
  }


}
