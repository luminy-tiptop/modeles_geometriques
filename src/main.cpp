#include <Gui/MainWindow.hpp>

#include <QApplication>
#include <QSurfaceFormat>

#include <stdexcept>
#include <cstdlib>
#include <iostream>

int main( int argc, char * argv[] )
{
  try
  {
    //
    QApplication app( argc, argv );

    //
    app.setApplicationName( "ModelesGeometriques" );
    app.setApplicationVersion( "0.1" );

    //
    Gui::MainWindow w(app);
    w.show();

    return app.exec();
  }
  catch ( const std::exception & e )
  {
    std::cerr << "Error : " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
}
