#version 330 core

layout(location = 0) in vec3 vertexPosition_modelsspace;

void main(){
    gl_Position.xyz = vertexPosition_modelsspace;
    gl_Position.w = 1.0;
}