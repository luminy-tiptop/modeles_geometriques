#version 330 core

// Input vertex data, different for all executions of this shader.
layout( location = 0 ) in vec3 vertex_from_buffer;
layout (location = 1) in vec3 aNormal;

// Uniforms :
uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;

// Output data :
out vec3 vertex;
out vec3 normal;

// Main
void main()
{
  vertex = vertex_from_buffer;

  // Output position of the vertex, in clip space : MVP * position
  gl_Position =  MVP * vec4( vertex_from_buffer, 1 );
  fragPos = vec3(M * vec4(vertex_from_buffer, 1.0));
  normal = mat3(transpose(inverse(M))) * aNormal;

}
