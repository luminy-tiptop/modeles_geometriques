//
// Created by DoubleVV on 29/10/2018.
//

#include <glm/gtc/matrix_transform.hpp>
#include "../../Include/Utility.hpp"

// position
glm::vec3 position = glm::vec3( 0, 22, 10 );
// horizontal angle : toward -Z
float horizontalAngle = 3.14f;
// vertical angle : 0, look at the horizon
float verticalAngle = -1.1f;
// Initial Field of View
float initialFoV = 45.0f;

float speed = 10.f; // 3 units / second
float mouseSpeed = 1.f;

double lastTime = 0.f;

glm::mat4 ProjectionMatrix;
glm::mat4 ViewMatrix;

void computeMatricesFromInputs( GLFWwindow * window, bool compute_mouse )
{
  double currentTime = glfwGetTime();
  auto deltaTime = float( currentTime - lastTime );
  lastTime = currentTime;

  // Get mouse position
  double xpos, ypos;
  glfwGetCursorPos( window, &xpos, &ypos );

  int width, height;
  glfwGetWindowSize( window, &width, &height );

  if ( compute_mouse )
  {
    glfwSetCursorPos( window, width / 2, height / 2 );

    // Compute new orientation
    horizontalAngle += mouseSpeed * deltaTime * float( width / 2 - xpos );
    verticalAngle   += mouseSpeed * deltaTime * float( height / 2 - ypos );
  }

  // Direction : Spherical coordinates to Cartesian coordinates conversion
  glm::vec3 direction(
    cos( verticalAngle ) * sin( horizontalAngle ),
    sin( verticalAngle ),
    cos( verticalAngle ) * cos( horizontalAngle )
  );

  // Right vector
  glm::vec3 right = glm::vec3(
                      sin( horizontalAngle - 3.14f / 2.0f ),
                      0,
                      cos( horizontalAngle - 3.14f / 2.0f )
                    );

  // Up vector : perpendicular to both direction and right
  glm::vec3 up = glm::cross( right, direction );

  // Move forward
  if ( glfwGetKey( window, GLFW_KEY_UP ) == GLFW_PRESS )
    position += direction * deltaTime * speed;
  // Move backward
  if ( glfwGetKey( window, GLFW_KEY_DOWN ) == GLFW_PRESS )
    position -= direction * deltaTime * speed;
  // Strafe right
  if ( glfwGetKey( window, GLFW_KEY_RIGHT ) == GLFW_PRESS )
    position += right * deltaTime * speed;
  // Strafe left
  if ( glfwGetKey( window, GLFW_KEY_LEFT ) == GLFW_PRESS )
    position -= right * deltaTime * speed;
  if ( glfwGetKey( window, GLFW_KEY_P ) == GLFW_PRESS )
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
  if ( glfwGetKey( window, GLFW_KEY_O ) == GLFW_PRESS )
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );


  float FoV = initialFoV;

  // Projection matrix : 45&deg; Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
  ProjectionMatrix = glm::perspective( glm::radians( FoV ), 4.0f / 3.0f, 0.1f, 10000.0f );
  // Camera matrix
  ViewMatrix       = glm::lookAt(
                       position,           // Camera is here
                       position + direction, // and looks here : at the same position, plus "direction"
                       up                  // Head is up (set to 0,-1,0 to look upside-down)
                     );
}

glm::mat4 getProjectionMatrix()
{
  return ProjectionMatrix;
}
glm::mat4 getViewMatrix()
{
  return ViewMatrix;
}
