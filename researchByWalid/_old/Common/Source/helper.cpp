//
// Created by DoubleVV on 18/12/2018.
//

#include "helper.hpp"

double distance2Points(glm::vec3 p1, glm::vec3 p2){
    return sqrt(((p1.x - p2.x) * (p1.x - p2.x)) + ((p1.y - p2.y) * (p1.y - p2.y)) + ((p1.z - p2.z) * (p1.z - p2.z)));
}

bool isInEllipse(float coord1, float coord2, float coord3, float axis1, float axis2, float axis3){
    return (((coord1 * coord1)/ (axis1 * axis1)) + (((coord2 * coord2)/ (axis2 * axis2))) + (((coord3 * coord3)/ (axis3 * axis3)))) <= 1;
}

int solve_deg2(double a, double b, double c, double & x1, double & x2)
{
    double delta = b * b - 4 * a * c;

    if (delta < 0) return 0;

    double inv_2a = 0.5 / a;

    if (delta == 0) {
        x1 = -b * inv_2a;
        x2 = x1;
        return 1;
    }

    double sqrt_delta = sqrt(delta);
    x1 = (-b + sqrt_delta) * inv_2a;
    x2 = (-b - sqrt_delta) * inv_2a;
    return 2;
}

/// Reference : Eric W. Weisstein. "Cubic Equation." From MathWorld--A Wolfram Web Resource.
/// http://mathworld.wolfram.com/CubicEquation.html
/// \return Number of real roots found.
int solve_deg3(double a, double b, double c, double d,
               double & x0, double & x1, double & x2)
{
    if (a == 0) {
        // Solve second order system
        if (b == 0)	{
            // Solve first order system
            if (c == 0)
                return 0;

            x0 = -d / c;
            return 1;
        }

        x2 = 0;
        return solve_deg2(b, c, d, x0, x1);
    }

    // Calculate the normalized form x^3 + a2 * SQR(x) + a1 * x + a0 = 0
    double inv_a = 1. / a;
    double b_a = inv_a * b, b_a2 = b_a * b_a;
    double c_a = inv_a * c;
    double d_a = inv_a * d;

    // Solve the cubic equation
    double Q = (3 * c_a - b_a2) / 9;
    double R = (9 * b_a * c_a - 27 * d_a - 2 * b_a * b_a2) / 54;
    double Q3 = Q * Q * Q;
    double D = Q3 + R * R;
    double b_a_3 = (1. / 3.) * b_a;

    if (Q == 0) {
        if(R == 0) {
            x0 = x1 = x2 = - b_a_3;
            return 3;
        }
        else {
            x0 = pow(2 * R, 1 / 3.0) - b_a_3;
            return 1;
        }
    }

    if (D <= 0) {
        // Three real roots
        double theta = acos(R / sqrt(-Q3));
        double sqrt_Q = sqrt(-Q);
        x0 = 2 * sqrt_Q * cos(theta             / 3.0) - b_a_3;
        x1 = 2 * sqrt_Q * cos((theta + 2 * M_PI)/ 3.0) - b_a_3;
        x2 = 2 * sqrt_Q * cos((theta + 4 * M_PI)/ 3.0) - b_a_3;

        return 3;
    }

    // D > 0, only one real root
    double AD = pow(fabs(R) + sqrt(D), 1.0 / 3.0) * (R > 0 ? 1 : (R < 0 ? -1 : 0));
    double BD = (AD == 0) ? 0 : -Q / AD;

    // Calculate the only real root
    x0 = AD + BD - b_a_3;

    return 1;
}

float Remap(float x, float t1, float t2, float s1, float s2)
{
    // "Yellow" is a "normalized" value between 0 and 1
    float yellow = (x - t1)/(t2 - t1);

    // "Green" is the result!
    float green = yellow*(s2 - s1) + s1;

    return green;
}

float RemapClamp(float x, float t1, float t2, float s1, float s2)
{
    if (x < t1)
        return s1;
    if (x > t2)
        return s2;

    return Remap(x, t1, t2, s1, s2);
}



// ----------------------------------------------------------------------------
inline void dsytrd3(glm::mat3 A, glm::mat3 Q, glm::vec3 d, glm::vec3 e)
// ----------------------------------------------------------------------------
// Reduces a symmetric 3x3 matrix to tridiagonal form by applying
// (unitary) Householder transformations:
//            [ d[0]  e[0]       ]
//    A = Q . [ e[0]  d[1]  e[1] ] . Q^T
//            [       e[1]  d[2] ]
// The function accesses only the diagonal and upper triangular parts of
// A. The access is read-only.
// ---------------------------------------------------------------------------
{
    const int n = 3;
    double u[n], q[n];
    double omega, f;
    double K, h, g;

    // Initialize Q to the identitity matrix
#ifndef EVALS_ONLY
    for (int i=0; i < n; i++)
    {
        Q[i][i] = 1.0;
        for (int j=0; j < i; j++)
            Q[i][j] = Q[j][i] = 0.0;
    }
#endif

    // Bring first row and column to the desired form
    h = SQR(A[0][1]) + SQR(A[0][2]);
    if (A[0][1] > 0)
        g = -sqrt(h);
    else
        g = sqrt(h);
    e[0] = g;
    f    = g * A[0][1];
    u[1] = A[0][1] - g;
    u[2] = A[0][2];

    omega = h - f;
    if (omega > 0.0)
    {
        omega = 1.0 / omega;
        K     = 0.0;
        for (int i=1; i < n; i++)
        {
            f    = A[1][i] * u[1] + A[i][2] * u[2];
            q[i] = omega * f;                  // p
            K   += u[i] * f;                   // u* A u
        }
        K *= 0.5 * SQR(omega);

        for (int i=1; i < n; i++)
            q[i] = q[i] - K * u[i];

        d[0] = A[0][0];
        d[1] = A[1][1] - 2.0*q[1]*u[1];
        d[2] = A[2][2] - 2.0*q[2]*u[2];

        // Store inverse Householder transformation in Q
#ifndef EVALS_ONLY
        for (int j=1; j < n; j++)
        {
            f = omega * u[j];
            for (int i=1; i < n; i++)
                Q[i][j] = Q[i][j] - f*u[i];
        }
#endif

        // Calculate updated A[1][2] and store it in e[1]
        e[1] = A[1][2] - q[1]*u[2] - u[1]*q[2];
    }
    else
    {
        for (int i=0; i < n; i++)
            d[i] = A[i][i];
        e[1] = A[1][2];
    }
}

int dsyevv3(glm::mat3 A, glm::mat3 Q, glm::vec3 w)
// ----------------------------------------------------------------------------
// Calculates the eigenvalues and normalized eigenvectors of a symmetric 3x3
// matrix A using the QL algorithm with implicit shifts, preceded by a
// Householder reduction to tridiagonal form.
// The function accesses only the diagonal and upper triangular parts of A.
// The access is read-only.
// ----------------------------------------------------------------------------
// Parameters:
//   A: The symmetric input matrix
//   Q: Storage buffer for eigenvectors
//   w: Storage buffer for eigenvalues
// ----------------------------------------------------------------------------
// Return value:
//   0: Success
//  -1: Error (no convergence)
// ----------------------------------------------------------------------------
// Dependencies:
//   dsytrd3()
// ----------------------------------------------------------------------------
{
    const int n = 3;
    glm::vec3 e;                   // The third element is used only as temporary workspace
    double g, r, p, f, b, s, c, t; // Intermediate storage
    int nIter;
    int m;

    // Transform A to real tridiagonal form by the Householder method
    dsytrd3(A, Q, w, e);

    // Calculate eigensystem of the remaining real symmetric tridiagonal matrix
    // with the QL method
    //
    // Loop over all off-diagonal elements
    for (int l=0; l < n-1; l++)
    {
        nIter = 0;
        while (1)
        {
            // Check for convergence and exit iteration loop if off-diagonal
            // element e(l) is zero
            for (m=l; m <= n-2; m++)
            {
                g = fabs(w[m])+fabs(w[m+1]);
                if (fabs(e[m]) + g == g)
                    break;
            }
            if (m == l)
                break;

            if (nIter++ >= 30)
                return -1;

            // Calculate g = d_m - k
            g = (w[l+1] - w[l]) / (e[l] + e[l]);
            r = sqrt(SQR(g) + 1.0);
            if (g > 0)
                g = w[m] - w[l] + e[l]/(g + r);
            else
                g = w[m] - w[l] + e[l]/(g - r);

            s = c = 1.0;
            p = 0.0;
            for (int i=m-1; i >= l; i--)
            {
                f = s * e[i];
                b = c * e[i];
                if (fabs(f) > fabs(g))
                {
                    c      = g / f;
                    r      = sqrt(SQR(c) + 1.0);
                    e[i+1] = f * r;
                    c     *= (s = 1.0/r);
                }
                else
                {
                    s      = f / g;
                    r      = sqrt(SQR(s) + 1.0);
                    e[i+1] = g * r;
                    s     *= (c = 1.0/r);
                }

                g = w[i+1] - p;
                r = (w[i] - g)*s + 2.0*c*b;
                p = s * r;
                w[i+1] = g + p;
                g = c*r - b;

                // Form eigenvectors
#ifndef EVALS_ONLY
                for (int k=0; k < n; k++)
                {
                    t = Q[k][i+1];
                    Q[k][i+1] = s*Q[k][i] + c*t;
                    Q[k][i]   = c*Q[k][i] - s*t;
                }
#endif
            }
            w[l] -= p;
            e[l]  = g;
            e[m]  = 0.0;
        }
    }

    return 0;
}



