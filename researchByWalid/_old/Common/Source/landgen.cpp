
#include <landgen.h>

#include "landgen.h"

landGen::landGen(int h ){
    this->h = h;

    mmin = h;
    mmax = -h;
    lissage = 1.0f;

    tab = new int**[FACE_COUNT];

    for(int face = 0; face < FACE_COUNT; face++) {
        tab[face] = new int*[h];
        for(int i = 0; i < h; i++)
        {
            tab[face][i] = new int[h];
            for(int j = 0; j < h; j++)
                tab[face][i][j] = 0;
        }
    }
}

int randint(int range) {
    /*auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed1);
    std::uniform_int_distribution<int> dis(-h, h);*/
    return rand() % (range<<1) - range;
}

int landGen::mod_step(int a)
{
    if(a <= 0)
        return a + h - 1;
    if(a >= h - 1)
        return a - h + 1;
    return a;
}

static int topology[FACE_COUNT][SIDE_COUNT][2] = {
    { // TOP
        {FAR, DOWN_SIDE},
        {RIGHT, LEFT_SIDE},
        {NEAR, UP_SIDE},
        {LEFT, RIGHT_SIDE}
    },
    { // NEAR
        {TOP, DOWN_SIDE},
        {RIGHT, DOWN_SIDE},
        {BOTTOM, UP_SIDE},
        {LEFT, DOWN_SIDE}
    },
    { // LEFT
        {FAR, LEFT_SIDE},
        {TOP, LEFT_SIDE},
        {NEAR, LEFT_SIDE},
        {BOTTOM, LEFT_SIDE}
    },
    { // RIGHT
        {FAR, RIGHT_SIDE},
        {BOTTOM, RIGHT_SIDE},
        {NEAR, RIGHT_SIDE},
        {TOP, RIGHT_SIDE}
    },
    { // FAR
        {BOTTOM, DOWN_SIDE},
        {RIGHT, UP_SIDE},
        {TOP, UP_SIDE},
        {LEFT, UP_SIDE}
    },
    { // BOTTOM
        {NEAR, DOWN_SIDE},
        {RIGHT, RIGHT_SIDE},
        {FAR, UP_SIDE},
        {LEFT, LEFT_SIDE}
    }
};

int landGen::getCrossProduct(int from_side, int to_side) {
    static int directions[SIDE_COUNT][2] = {{-1,0}, {0,1}, {1,0}, {0,-1}};
    return directions[from_side][0] * directions[to_side][1] - directions[to_side][0] * directions[from_side][1];
}

int landGen::getCaseOnOtherFace(int & from_face, int from_side, int x, int y) {
    int to_face = topology[from_face][from_side][0];
    int to_side = topology[from_face][from_side][1];

    //std::cout << "( " << to_face << ", " << to_side << " ) ";

    int cross_product = getCrossProduct(from_side, to_side);

    if(cross_product == 0) {
        if(from_side != to_side) {
            return getCaseFrom(to_face, x, y); 
        } else {
            return getCaseFrom(to_face, h-x-1, h-y-1);
        }
    } else if(cross_product > 0) {
        return getCaseFrom(to_face, h-y-1, x);
    }

    return getCaseFrom(to_face, y, h-x-1);
}

int landGen::getCaseFrom(int face, int x, int y) {
    if(x < 0)
        return getCaseOnOtherFace(face, LEFT_SIDE, mod_step(x), y);
    if(x >= h)
        return getCaseOnOtherFace(face, RIGHT_SIDE, mod_step(x), y);
    if (y < 0)
        return getCaseOnOtherFace(face, UP_SIDE, x, mod_step(y));
    if (y >= h)
        return getCaseOnOtherFace(face, DOWN_SIDE, x, mod_step(y));

    return tab[face][x][y];
}

void landGen::setCaseOnOtherFace(int & from_face, int from_side, int x, int y, int value) {    
    int to_face = topology[from_face][from_side][0];
    int to_side = topology[from_face][from_side][1];

    int cross_product = getCrossProduct(from_side, to_side);

    if(cross_product == 0) {
        if(from_side != to_side) {
            setDualCases(to_face, x, y, value);
            return ;
        } else {
            setDualCases(to_face, h-x-1, h-y-1, value);
            return ;
        }
    } else if(cross_product > 0) {
        setDualCases(to_face, h-y-1, x, value);
        return ; 
    }
    setDualCases(to_face, y, h-x-1, value);
    return ;
}


void landGen::setDualCases(int face, int x, int y, int value) {
    if(x >= 0 && x < h && y >= 0 && y < h) {
        if(tab[face][x][y] == value)
            return ;
        tab[face][x][y] = value;
    }

    if(x == 0) {
        setCaseOnOtherFace(face, LEFT_SIDE, mod_step(x), y, value);
    } else if(x == h-1) {
        setCaseOnOtherFace(face, RIGHT_SIDE, mod_step(x), y, value);
    }
    if (y == 0) {
        setCaseOnOtherFace(face, UP_SIDE, x, mod_step(y), value);
    }
    else if (y == h-1) {
        setCaseOnOtherFace(face, DOWN_SIDE, x, mod_step(y), value);
    }
}



int landGen::moyenne_carre(int & face, int & x, int & y, int & halfSide) {
    int moyenne = 
            (tab[face][x-halfSide][y-halfSide] + 
             tab[face][x-halfSide][y+halfSide] + 
             tab[face][x+halfSide][y-halfSide] + 
             tab[face][x+halfSide][y+halfSide]) / 4;
    return moyenne + randint(halfSide) * lissage;
}

int landGen::moyenne_diamant(int & face, int & x, int & y, int & halfSide){
    return (getCaseFrom(face, x-halfSide, y) +
            getCaseFrom(face, x+halfSide, y) +
            getCaseFrom(face, x, y-halfSide) +
            getCaseFrom(face, x, y+halfSide)) / 4 + randint(halfSide) * lissage;
}

void landGen::diamantcarre() {
    int s = h - 1;
//*
    setDualCases(TOP, 0, 0, randint(h));
    setDualCases(TOP, 0, s, randint(h));
    setDualCases(TOP, s, 0, randint(h));
    setDualCases(TOP, s, s, randint(h));

    setDualCases(BOTTOM, 0, 0, randint(h));
    setDualCases(BOTTOM, 0, s, randint(h));
    setDualCases(BOTTOM, s, 0, randint(h));
    setDualCases(BOTTOM, s, s, randint(h));

    int halfSide;

    for(int side = h-1; side > 1; side=halfSide) {
        halfSide = side>>1;
        for(int face = 0; face < FACE_COUNT; face++) {
            // CARRE
            for(int x = halfSide ; x < h ; x+=side)
                for (int y = halfSide ; y < h ; y+=side) {
                    tab[face][x][y]= moyenne_carre(face,x, y, halfSide);
                }
        }
        for(int face = 0; face < FACE_COUNT; face++) {
            // DIAMANT
            for (int x = 0 ; x < h ; x += side)
                for (int y = halfSide ; y < h ; y += side) {
                    setDualCases(face, x, y, moyenne_diamant(face, x, y, halfSide));
                    setDualCases(face, y, x, moyenne_diamant(face, y, x, halfSide));
                }
        }
    }
/*/
    setDualCases(TOP, 0, 0, -1);
    setDualCases(TOP, 0, s, -2);
    setDualCases(TOP, s, 0, -3);
    setDualCases(TOP, s, s, -4);

    setDualCases(BOTTOM, 0, 0, -1);
    setDualCases(BOTTOM, 0, s, -2);
    setDualCases(BOTTOM, s, 0, -3);
    setDualCases(BOTTOM, s, s, -4);

    int face = TOP;
    for(int i = 0; i < 2*h-1; i++) {
        std::cout << i << "  ";

        setDualCases(BOTTOM, i, h/4, i);

        std::cout << std::endl;
    }

//*/


    return ;
}

int ***landGen::getTab() {
    return tab;
}