#ifndef LANDGEN_H
#define LANDGEN_H

#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <random>
#include <chrono>
#include <thread>


enum Face {
    TOP,
    NEAR,
    LEFT,
    RIGHT,
    FAR,
    BOTTOM,
    FACE_COUNT
};

enum Side {
    UP_SIDE,
    RIGHT_SIDE,
    DOWN_SIDE,
    LEFT_SIDE,
    SIDE_COUNT
};

class landGen
{
public:
    landGen(int h);
    void diamantcarre();

    int moyenne_diamant(int & face, int & x, int & y, int & d);
    int moyenne_carre(int & face, int & x, int & y, int & d);
    int getCrossProduct(int from_side, int to_side);
    int mod_step(int a);
    int getCaseFrom(int face, int x, int y);
    int getCaseOnOtherFace(int & from_face, int from_side, int x, int y);
    void setDualCases(int face, int x, int y, int value);
    void setCaseOnOtherFace(int & from_face, int from_side, int x, int y, int value);

    int*** getTab();

private:
    int *** tab;
    std::vector<std::array<int,2>> directions = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

    double lissage;
    int h;
    //test : rectif alex
    double mmin, mmax;

};

#endif // LANDGEN_H
