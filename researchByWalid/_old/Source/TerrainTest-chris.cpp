// Inclut les en-têtes standards
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <stdexcept>
#include <string>

// Inclut GLEW. Toujours l'inclure avant gl.h et glfw.h, car c'est un peu magique.
#include <GL/glew.h>

// Inclut GLFW
#include <GLFW/glfw3.h>

// Inclut GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

//Include LoadShader function
#include "../Include/Utility.hpp"

#include <landgen.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>


// Test debug :
#include <chrono>
#include <random>
#include <iostream>

static std::uniform_real_distribution<float> positive_normal_distribution( 0.f, 0.01f );
static std::ranlux48_base rand_generator( unsigned( std::chrono::system_clock::now().time_since_epoch().count() ) );



using namespace glm;

using uint = unsigned int;

typedef std::vector<glm::vec3> VerticesArray;
typedef std::vector<glm::vec3> ColorArray;
typedef std::vector<glm::vec2> UVArray;

//
//enum Face {
//    TOP,
//    NEAR,
//    FAR,
//    LEFT,
//    BOTTOM,
//    RIGHT,
//    FACE_COUNT
//};

enum Edge {
    TOP_EDGE,
    RIGHT_EDGE,
    BOTTOM_EDGE,
    LEFT_EDGE,
    EDGE_COUNT
};

struct Direction{
    int face;
    Edge edge;

    Direction(int f = TOP, Edge e = TOP_EDGE){
        face = f;
        edge = e;
    }
};

int debug = 00;

std::vector<std::vector<Direction>> topologie;

std::vector<std::vector<double>> diamantCarre(int rows, const int cols, int height){

  std::vector<std::vector<double>> t(rows, std::vector<double>(cols, 0));

  std::cout << "Début du diamant carré" << std::endl;

  int h = rows/2;

  auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();
  std::mt19937 gen(seed1);
  std::uniform_real_distribution<double> dis(0, 1);
  std::uniform_int_distribution<int> dis2(-rows, rows);

//  t[0][0] = dis2(gen);
//  t[0][rows - 1] = dis2(gen);
//  t[rows - 1][rows - 1] = dis2(gen);
//  t[rows - 1][0] = dis2(gen);

  t[0][0] = 0;
  t[0][rows - 1] = 0;
  t[rows - 1][rows - 1] = 0;
  t[rows - 1][0] = 0;

  for(int sideLength = rows - 1; sideLength >= 2; sideLength/=2, h/=2){
    int halfSide = sideLength/2;

    for(int x = 0; x < rows-1; x+= sideLength){
      for(int y = 0; y<rows-1; y+=sideLength){
        double avg = t[x][y] +
                     t[x+sideLength][y]+
                     t[x][y+sideLength]+
                     t[x+sideLength][y+sideLength];
        avg /= 4.0;

        t[x+halfSide][y+halfSide] = avg + (dis(gen)*2*h) - h;
      }
    }

    for(int x=0; x<rows-1; x+=halfSide){
      for(int y=(x+halfSide)%sideLength; y<rows-1; y+=sideLength){
        double avg =
                t[(x-halfSide+rows)%rows][y] +
                t[(x+halfSide)%rows][y] +
                t[x][(y+halfSide)%rows] +
                t[x][(y-halfSide+rows)%rows];
        avg /= 4.0;

        avg = avg + (dis(gen)*2*h) - h;

        t[x][y] = avg;

        if(x == 0)
          t[rows-1][y] = avg;
        if(y == 0){
          t[x][rows-1] = avg;
        }
      }
    }
  }
  return t;
}

std::vector<std::vector<std::vector<double>>> diamantCarreCube(int size, int height){



    std::vector<std::vector<std::vector<double>>> cube;

    for(int i = 0; i < FACE_COUNT; i++){
        cube.emplace_back(std::vector<std::vector<double>>(size, std::vector<double>(size, 0)));
    }

//    std::vector<std::vector<double>> t(size, std::vector<double>(size, 0));

    std::cout << "Début du diamant carré" << std::endl;

    auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed1);
    std::uniform_real_distribution<double> dis(0, 1);
    std::uniform_int_distribution<int> dis2(-height, height);

//  t[0][0] = dis2(gen);
//  t[0][size - 1] = dis2(gen);
//  t[size - 1][size - 1] = dis2(gen);
//  t[size - 1][0] = dis2(gen);

    for(int face = 0; face < FACE_COUNT; face++){
        int h = size/2;

        cube[face][0][0] = 0;
        cube[face][0][size - 1] = 0;
        cube[face][size - 1][size - 1] = 0;
        cube[face][size - 1][0] = 0;

        for(int sideLength = size - 1; sideLength >= 2; sideLength/=2, h/=2){
            int halfSide = sideLength/2;

            for(int x = 0; x < size-1; x+= sideLength){
                for(int y = 0; y<size-1; y+=sideLength){
                    double avg = cube[face][x][y] +
                                 cube[face][x+sideLength][y]+
                                 cube[face][x][y+sideLength]+
                                 cube[face][x+sideLength][y+sideLength];
                    avg /= 4.0;

                    cube[face][x+halfSide][y+halfSide] = avg + (dis(gen)*2*h) - h;
                }
            }

            for(int x=0; x<size-1; x+=halfSide){
                for(int y=(x+halfSide)%sideLength; y<size-1; y+=sideLength){
                    double avg =
                            cube[face][(x-halfSide+size)%size][y] +
                            cube[face][(x+halfSide)%size][y] +
                            cube[face][x][(y+halfSide)%size] +
                            cube[face][x][(y-halfSide+size)%size];
                    avg /= 4.0;

                    avg = avg + (dis(gen)*2*h) - h;

                    cube[face][x][y] = avg;

                    if(x == 0)
                        cube[face][size-1][y] = avg;
                    if(y == 0){
                        cube[face][x][size-1] = avg;
                    }
                }
            }

            for(int x = 0; x < size; x++){
                cube[face][x][0] = 0;
                cube[face][x][size - 1] = 0;
                cube[face][0][x] = 0;
                cube[face][size - 1][x] = 0;
            }
        }
    }


    return cube;
}

std::vector<std::vector<Direction>> initDirection(){
    std::vector<std::vector<Direction>> direction(FACE_COUNT, std::vector<Direction>(EDGE_COUNT));

    direction[TOP][TOP_EDGE] = Direction(FAR, BOTTOM_EDGE);
    direction[TOP][LEFT_EDGE] = Direction(LEFT, RIGHT_EDGE);
    direction[TOP][BOTTOM_EDGE] = Direction(NEAR, TOP_EDGE);
    direction[TOP][RIGHT_EDGE] = Direction(RIGHT, LEFT_EDGE);

    direction[NEAR][TOP_EDGE] = Direction(TOP, BOTTOM_EDGE);
    direction[NEAR][LEFT_EDGE] = Direction(LEFT, BOTTOM_EDGE);
    direction[NEAR][BOTTOM_EDGE] = Direction(BOTTOM, TOP_EDGE);
    direction[NEAR][RIGHT_EDGE] = Direction(RIGHT, BOTTOM_EDGE);

    direction[BOTTOM][TOP_EDGE] = Direction(NEAR, BOTTOM_EDGE);
    direction[BOTTOM][LEFT_EDGE] = Direction(LEFT, LEFT_EDGE);
    direction[BOTTOM][BOTTOM_EDGE] = Direction(FAR, TOP_EDGE);
    direction[BOTTOM][RIGHT_EDGE] = Direction(RIGHT, RIGHT_EDGE);

    direction[FAR][TOP_EDGE] = Direction(BOTTOM, BOTTOM_EDGE);
    direction[FAR][LEFT_EDGE] = Direction(LEFT, TOP_EDGE);
    direction[FAR][BOTTOM_EDGE] = Direction(TOP, TOP_EDGE);
    direction[FAR][RIGHT_EDGE] = Direction(RIGHT, TOP_EDGE);

    direction[LEFT][TOP_EDGE] = Direction(FAR, LEFT_EDGE);
    direction[LEFT][LEFT_EDGE] = Direction(BOTTOM, LEFT_EDGE);
    direction[LEFT][BOTTOM_EDGE] = Direction(NEAR, LEFT_EDGE);
    direction[LEFT][RIGHT_EDGE] = Direction(TOP, LEFT_EDGE);

    direction[RIGHT][TOP_EDGE] = Direction(FAR, RIGHT_EDGE);
    direction[RIGHT][LEFT_EDGE] = Direction(TOP, RIGHT_EDGE);
    direction[RIGHT][BOTTOM_EDGE] = Direction(NEAR, RIGHT_EDGE);
    direction[RIGHT][RIGHT_EDGE] = Direction(BOTTOM, RIGHT_EDGE);

    return direction;
}

int norm(int x, int size){
    if(x >= size){
        x = x - size + 1;
    }
    if(x < 0){
        x = x + size - 1;
    }
    if(x < 0){
        x = x + size - 1;
    }

    return x;
}

double getValueNextFace(Direction in, Direction out, int x, int y, int size,
                        std::vector<std::vector<std::vector<double>>> &cube){
    std::vector<glm::vec3> movementVector{glm::vec3(-1,0,0), glm::vec3(0,1,0), glm::vec3(1,0,0), glm::vec3(0,-1,0)};

    auto crossProduct = glm::cross(movementVector[in.edge], movementVector[out.edge]).z;

    int xtest = 0;
    int ytest = 0;

    double cubetest = 0;

//    if(crossProduct == 0){
//        if(in.edge == out.edge){
//            return cube[in.face][norm(x, size)][norm(y, size)];
//        }
//        else{
//            if(movementVector[in.edge].x == 0){
//                return cube[in.face][norm(x, size)][norm(y,size)];
//            }
//            else{
//                return cube[in.face][norm(x, size)][norm(y, size)];
//            }
//        }
//    }
//    if(crossProduct < 0){
//        return cube[in.face][norm((-y), size)][norm(x, size)];
//    }
//
//    return cube[in.face][norm(y, size)][norm((-x), size)];

    if(crossProduct == 0){
        xtest = norm(x, size);
        ytest = norm(y, size);

        if(in.edge == out.edge){
            return cube[in.face][norm(y, size)][norm(x, size)];
        }
        else{
            if(movementVector[in.edge].x == 0){
                return cube[in.face][norm(y, size)][norm(x,size)];
            }
            else{
                return cube[in.face][norm(y, size)][norm(x, size)];
            }
        }
    }
    if(crossProduct > 0){
        xtest = norm(y, size);
        ytest = norm(-x, size);

        cubetest = cube[in.face][norm((y), size)][norm(-x, size)];

        return cube[in.face][norm((-x), size)][norm(y, size)];
    }

    xtest = norm(y, size);
    ytest = norm(-x, size);

    cubetest = cube[in.face][norm(-y, size)][norm((-x), size)];


    return cube[in.face][norm(x, size)][norm((-y), size)];
}

double getPointValueFace(std::vector<std::vector<Direction>> topologie, int x, int y, int face, int size,
                         std::vector<std::vector<std::vector<double>>> &cube){

    if(x >= size) {
        debug++;
        return getValueNextFace(topologie[face][RIGHT_EDGE], Direction(face, RIGHT_EDGE), x, y, size, cube);
    }
    if(x < 0) {
        debug++;
        return getValueNextFace(topologie[face][LEFT_EDGE], Direction(face, LEFT_EDGE), x, y, size, cube);
    }
    if (y >= size) {
        debug++;
        return getValueNextFace(topologie[face][BOTTOM_EDGE], Direction(face, BOTTOM_EDGE), x, y, size, cube);
    }
    if (y < 0) {
        debug++;
        return getValueNextFace(topologie[face][TOP_EDGE], Direction(face, TOP_EDGE), x, y, size, cube);
    }

    return cube[face][x][y];
}

int normCoord(int x, int size){
//    if(x == 0){
//        x = size - 1;
//    }
//    else if(x == size - 1){
//        x = 0;
//    }
//    if(x >= size){
//        x = x - size + 1;
//    }
//    if(x < 0){
//        x = x + size - 1;
//    }
//    if(x < 0){
//        x = x + size - 1;
//    }

    if(x >= size - 1){
        return x - size + 1;
    }
    if(x <= 0){
        x = x + size - 1;
    }
    if(x <= 0){
        x = x + size - 1;
    }

    return x;
}

glm::ivec2 getCoordNextFace(Direction in, Direction out, int x, int y, int size,
                        std::vector<std::vector<std::vector<double>>> &cube){
    std::vector<glm::vec3> movementVector{glm::vec3(-1,0,0), glm::vec3(0,1,0), glm::vec3(1,0,0), glm::vec3(0,-1,0)};

    int xtest = 0;
    int ytest = 0;

    auto crossProduct = glm::cross(movementVector[in.edge], movementVector[out.edge]).z;

    if(crossProduct == 0){
        if(in.edge == out.edge){
            return glm::ivec2(x, normCoord(-y, size));
        }
        else{
            if(movementVector[in.edge].x == 0){
                return glm::ivec2(normCoord(x, size), normCoord(y,size));
            }
            else{
                return glm::ivec2(normCoord(x, size), normCoord(y, size));
            }
        }
    }
    if(crossProduct > 0){
        xtest = normCoord(y, size);
        ytest = normCoord(x, size);
        switch(in.edge){
            case BOTTOM_EDGE :
                return glm::ivec2(normCoord(y, size), x);
            case RIGHT_EDGE :
                return glm::ivec2(normCoord(y, size), normCoord(-x, size));
            case TOP_EDGE :
                return glm::ivec2(normCoord(y, size), x);
            case LEFT_EDGE :
                return glm::ivec2(normCoord(y, size), normCoord(-x, size));

        }

//        return glm::ivec2(normCoord(y, size), normCoord(x, size));
    }

    xtest = normCoord(y, size);
    ytest = normCoord(-x, size);

    switch(in.edge){
        case BOTTOM_EDGE :
            return glm::ivec2(normCoord(-y, size), normCoord(x, size));
        case RIGHT_EDGE :
            return glm::ivec2(y, normCoord(x, size));
        case TOP_EDGE :
            return glm::ivec2(normCoord(-y, size), normCoord(x, size));
        case LEFT_EDGE :
            return glm::ivec2(y, normCoord(x, size));

    }

//    return glm::ivec2(normCoord(y, size), normCoord(-x, size));
}

glm::ivec2 getPointCoordFace(std::vector<std::vector<Direction>> topologie, int x, int y, int face, int size,
                         std::vector<std::vector<std::vector<double>>> &cube){

    if(x >= size) {
        debug++;
        return getCoordNextFace(topologie[face][RIGHT_EDGE], Direction(face, RIGHT_EDGE), x, y, size, cube);
    }
    if(x < 0) {
        debug++;
        return getCoordNextFace(topologie[face][LEFT_EDGE], Direction(face, LEFT_EDGE), x, y, size, cube);
    }
    if (y >= size) {
        debug++;
        return getCoordNextFace(topologie[face][BOTTOM_EDGE], Direction(face, BOTTOM_EDGE), x, y, size, cube);
    }
    if (y < 0) {
        debug++;
        return getCoordNextFace(topologie[face][TOP_EDGE], Direction(face, TOP_EDGE), x, y, size, cube);
    }

    return glm::ivec2(x, y);
}

double moyenne_carre(int x, int y, int d, std::vector<std::vector<std::vector<double>>>& cube, int face, double coef, float gen){
    d >>= 1;

    double moyenne = (cube[face][y-d][x-d] + cube[face][y-d][x+d] + cube[face][y+d][x+d] + cube[face][y+d][x-d])/4;

    std::uniform_real_distribution<float> dis(-d, d);
    return moyenne+ (gen * coef);
}

double moyenne_diamant(int x, int y, int d, int size, std::vector<std::vector<std::vector<double>>>& cube, int face, int coef,
                       float gen){
    double somme = 0;
    double somme1 = 0;
    double somme2 = 0;
    double somme3 = 0;
    double somme4 = 0;
    int n = 4;

    d >>= 1;

//    if(x == 0){
//        somme1 += getPointValueFace(topologie, x - d, y, face, size, cube);
//    }
//    else{
//        somme1 += cube[face][y][x - d];
//    }
//    if(y == 0){
//        somme2 += getPointValueFace(topologie, x, y - d, face, size, cube);
//    }
//    else{
//        somme2 += cube[face][y - d][x];
//    }
//    if(x == size - 1){
//        somme3 += getPointValueFace(topologie, x + d, y, face, size, cube);
//    }
//    else{
//        somme3 += cube[face][y][x + d];
//    }
//    if(y == size - 1){
//        somme4 += getPointValueFace(topologie, x, y + d, face, size, cube);
//    }
//    else{
//        somme4 += cube[face][y + d][x];
//    }

    somme += getPointValueFace(topologie, x - d, y, face, size, cube);
    somme += getPointValueFace(topologie, x, y - d, face, size, cube);
    somme += getPointValueFace(topologie, x + d, y, face, size, cube);
    somme += getPointValueFace(topologie, x, y + d, face, size, cube);

//    somme = somme1 + somme2 + somme3 + somme4;

    std::uniform_int_distribution<int> dis(-d, d);
    return ((somme / n) + (gen * coef));
}



std::vector<std::vector<std::vector<double>>> diamantCarreCubeAlex(int size, double coef){
    std::vector<std::vector<std::vector<double>>> cube;

    auto generator = landGen(size);
    generator.diamantcarre();

    auto tab = generator.getTab();

    for(int i = 0; i < FACE_COUNT; i++){
        cube.emplace_back(std::vector<std::vector<double>>(size, std::vector<double>(size, 0)));
    }

//    for(int face = 0; face < FACE_COUNT; face++){
//        for(int y = 0; y < size - 1; y++){
//            for(int x = 0; x < size - 1; x++){
//                cube[face][y][x] = tab[face][y][x];
//            }
//        }
//    }
//
//    return cube;

//    std::vector<std::vector<double>> t(size, std::vector<double>(size, 0));

    std::cout << "Début du diamant carré" << std::endl;


    auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed1);
    std::uniform_real_distribution<float> dis(-size, size);

    std::cout << "génération ... " << std::endl;

    topologie = initDirection();

    int s = size - 1;

    cube[TOP][0][0] = dis(gen);
    cube[TOP][0][s] = dis(gen); // FAR, LEFT
    cube[TOP][s][0] = dis(gen); // LEFT, NEAR
    cube[TOP][s][s] = dis(gen); // RIGHT, NEAR

    cube[BOTTOM][0][0] = dis(gen); // FAR, RIGHT
    cube[BOTTOM][0][s] = dis(gen); // FAR, LEFT
    cube[BOTTOM][s][0] = dis(gen); // LEFT, NEAR
    cube[BOTTOM][s][s] = dis(gen); // RIGHT, NEAR

    cube[LEFT][0][0] = cube[BOTTOM][s][0]; // FAR, RIGHT
    cube[LEFT][0][s] = cube[TOP][0][0]; // FAR, LEFT
    cube[LEFT][s][0] = cube[BOTTOM][0][0]; // LEFT, NEAR
    cube[LEFT][s][s] = cube[TOP][s][0]; // RIGHT, NEAR

    cube[RIGHT][0][0] = cube[TOP][0][s]; // FAR, RIGHT
    cube[RIGHT][0][s] = cube[BOTTOM][s][s]; // FAR, LEFT
    cube[RIGHT][s][0] = cube[TOP][s][s]; // LEFT, NEAR
    cube[RIGHT][s][s] = cube[BOTTOM][0][s]; // RIGHT, NEAR

    cube[NEAR][0][0] = cube[TOP][s][0]; // FAR, RIGHT
    cube[NEAR][0][s] = cube[TOP][s][s]; // FAR, LEFT
    cube[NEAR][s][0] = cube[BOTTOM][0][0]; // LEFT, NEAR
    cube[NEAR][s][s] = cube[BOTTOM][0][s]; // RIGHT, NEAR

    cube[FAR][0][0] = cube[BOTTOM][s][0]; // FAR, RIGHT
    cube[FAR][0][s] = cube[BOTTOM][s][s]; // FAR, LEFT
    cube[FAR][s][0] = cube[TOP][0][0]; // LEFT, NEAR
    cube[FAR][s][s] = cube[TOP][0][s]; // RIGHT, NEAR


    // TOP,NEAR,FAR,LEFT,BOTTOM,RIGHT.
    // Respecte l'ordre shlagos

//    for(int face = 0; face < FACE_COUNT; face++) {
//        switch (face) {
//            case TOP :
////                cube[face][0][0] = dis(gen);
////                cube[face][0][size - 1] = dis(gen);
////                cube[face][size - 1][size - 1] = dis(gen);
////                cube[face][size - 1][0] = dis(gen);
//                cube[face][0][0] = dis(gen); // FAR, RIGHT
//                cube[face][0][size - 1] = dis(gen); // FAR, LEFT
//                cube[face][size - 1][0] = dis(gen); // LEFT, NEAR
//                cube[face][size - 1][size - 1] = dis(gen); // RIGHT, NEAR
//                break;
//            case NEAR :
//                cube[face][0][0] = dis(gen); // LEFT, BOTTOM
//                cube[face][0][size - 1] = cube[TOP][size - 1][0]; // LEFT, TOP
//                cube[face][size - 1][0] = dis(gen); // RIGHT, BOTTOM
//                cube[face][size - 1][size - 1] = cube[TOP][size - 1][size - 1]; // RIGHT, TOP
//                break;
//            case FAR :
//                cube[face][0][0] = dis(gen); // LEFT, TOP
//                cube[face][0][size - 1] = dis(gen); // RIGHT, BOTTOM
//                cube[face][size - 1][0] = cube[TOP][0][0]; // LEFT, BOTTOM
//                cube[face][size - 1][size - 1] = cube[TOP][0][size - 1]; // RIGHT, TOP
//                break;
//            case LEFT :
//                cube[face][0][0] = cube[FAR][0][0]; // LEFT, TOP
//                cube[face][0][size - 1] = cube[TOP][0][0]; // RIGHT, BOTTOM
//                cube[face][size - 1][0] = cube[NEAR][0][0]; // LEFT, BOTTOM
//                cube[face][size - 1][size - 1] = cube[TOP][size - 1][0]; // RIGHT, TOP
//                break;
//            case BOTTOM :
//                cube[face][0][0] = cube[FAR][0][0]; // LEFT, TOP
//                cube[face][0][size - 1] = cube[NEAR][0][0]; // RIGHT, BOTTOM
//                cube[face][size - 1][0] = cube[FAR][0][size - 1]; // LEFT, BOTTOM
//                cube[face][size - 1][size - 1] = cube[NEAR][size - 1][0]; // RIGHT, TOP
//                break;
//            case RIGHT :
//                cube[face][0][0] = cube[FAR][0][size - 1]; // LEFT, TOP
//                cube[face][0][size - 1] = cube[NEAR][size - 1][0]; // RIGHT, BOTTOM
//                cube[face][size - 1][0] = cube[FAR][size - 1][size - 1]; // LEFT, BOTTOM
//                cube[face][size - 1][size - 1] = cube[NEAR][size - 1][size - 1]; // RIGHT, TOP
//                break;
//        }
//    }

//    return cube;


        for(int d = size-1; d > 1; d>>=1) {
            std::uniform_real_distribution<float> dis2(-d, d);
            for(int face = 0; face < FACE_COUNT; face++) {

                    /* CARRE */
                    for (int x = d / 2; x < size; x += d)
                        for (int y = d / 2; y < size; y += d) {
                            cube[face][y][x] = moyenne_carre(x, y, d, cube, face, coef, dis2(gen));
                        }
                }
            for(int face = 0; face < FACE_COUNT; face++) {
                /* DIAMANT */
                    for (int x = 0; x < size; x += d)
                        for (int y = d / 2; y < size; y += d) {
                            if(cube[face][x][y] == 0){
                                cube[face][x][y] = moyenne_diamant(y, x, d, size, cube, face, coef, dis2(gen));
                                if(y == 0){
                                    auto adjacentDirection = topologie[face][LEFT_EDGE];
                                    auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, LEFT_EDGE), x, y, size, cube);

                                    cube[adjacentDirection.face][adjacentCood.x][adjacentCood.y] = cube[face][x][y];
                                }
                                if(y == size - 1){
                                    auto adjacentDirection = topologie[face][RIGHT_EDGE];
                                    auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, RIGHT_EDGE), x, y, size, cube);

                                    cube[adjacentDirection.face][adjacentCood.x][adjacentCood.y] = cube[face][x][y];
                                }
                                if(x == 0){
                                    auto adjacentDirection = topologie[face][TOP_EDGE];
                                    auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, TOP_EDGE), x, y, size, cube);

                                    cube[adjacentDirection.face][adjacentCood.x][adjacentCood.y] = cube[face][x][y];
                                }
                                if(x == size - 1){
                                    auto adjacentDirection = topologie[face][BOTTOM_EDGE];
                                    auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, BOTTOM_EDGE), x, y, size, cube);

                                    cube[adjacentDirection.face][adjacentCood.x][adjacentCood.y] = cube[face][x][y];
                                }
                            }
                            if(cube[face][y][x] == 0){
                                cube[face][y][x] = moyenne_diamant(x, y, d, size, cube, face, coef, dis2(gen));
                                if(x == 0){
                                    auto adjacentDirection = topologie[face][LEFT_EDGE];
                                    auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, LEFT_EDGE), 0, y, size, cube);

                                    cube[adjacentDirection.face][adjacentCood.y][adjacentCood.x] = cube[face][y][x];
                                }
                                if(x == size - 1){
                                    auto adjacentDirection = topologie[face][RIGHT_EDGE];
                                    auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, RIGHT_EDGE), size - 1, y, size, cube);

                                    cube[adjacentDirection.face][adjacentCood.y][adjacentCood.x] = cube[face][y][x];
                                }
                                if(y == 0){
                                    auto adjacentDirection = topologie[face][TOP_EDGE];
                                    auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, TOP_EDGE), x, 0, size, cube);

                                    cube[adjacentDirection.face][adjacentCood.y][adjacentCood.x] = cube[face][y][x];
                                }
                                if(y == size - 1){
                                    auto adjacentDirection = topologie[face][BOTTOM_EDGE];
                                    auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, BOTTOM_EDGE), x, size - 1, size, cube);

                                    cube[adjacentDirection.face][adjacentCood.y][adjacentCood.x] = cube[face][y][x];
                                }
                            }
                        }
                }
//        for(int x = 1; x < size - 1; x++){
//            cube[face][x][0] = 0;
//            cube[face][x][size - 1] = 0;
//            cube[face][0][x] = 0;
//            cube[face][size - 1][x] = 0;
//        }

        std::cout << "done" << std::endl;
    }

    std::cout << debug << std::endl;

    // Correction du terrain
    float quarterSize = size/4;
    double moy;
    for(int x = 0; x < size/2; x++){

        for(int interpolation = 0; interpolation < x && interpolation < quarterSize; interpolation++){
            //
            // ARETES
            //

//            // NEAR, TOP, LEFT
//            moy = ((cube[NEAR][x][size - 1 - interpolation] + cube[TOP][size - 1 - interpolation][x] + cube[LEFT][size - 1 - x][size - 1 - interpolation])/3);
//            cube[NEAR][x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[NEAR][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[TOP][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[LEFT][size - 1 - x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//
//            moy = ((cube[TOP][size - 1 - x][0 + interpolation] + cube[LEFT][size - 1 - interpolation][size - 1 - x] + cube[NEAR][0 + interpolation][size - 1 - x])/3);
//            cube[TOP][size - 1 - x][0 + interpolation] = (interpolation / (quarterSize)) * cube[TOP][size - 1 - x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[LEFT][size - 1 - interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//            cube[NEAR][0 + interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[NEAR][0 + interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);

//
//            // NEAR, TOP, RIGHT
//            moy = ((cube[NEAR][size - 1 - x][size - 1 - interpolation] + cube[TOP][size - 1 - interpolation][size - 1 - x])/2);
//            cube[NEAR][size - 1 - x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[NEAR][size - 1 - x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][size - 1 - interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[TOP][size - 1 - interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//
//            // FAR, TOP, LEFT
//            moy = ((cube[FAR][size - 1 - interpolation][x] + cube[TOP][0 + interpolation][x])/2);
//            cube[FAR][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[FAR][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][0 + interpolation][x] = (interpolation / (quarterSize)) * cube[TOP][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);
//
//            // FAR, TOP, RIGHT
//            moy = ((cube[FAR][size - 1 - interpolation][size - 1 - x] + cube[TOP][0 + interpolation][size - 1 - x])/2);
//            cube[FAR][size - 1 - interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[FAR][size - 1 - interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][0 + interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[TOP][0 + interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, TOP, FAR
//            moy = (cube[TOP][x][0 + interpolation] + cube[LEFT][x][size - 1 - interpolation])/2;
//            cube[LEFT][x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[LEFT][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][x][0 + interpolation] = (interpolation / (quarterSize)) * cube[TOP][x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, TOP, NEAR
//            moy = (cube[TOP][size - 1 - x][0 + interpolation] + cube[LEFT][size - 1 - x][size - 1 - interpolation])/2;
//            cube[LEFT][size - 1 - x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][size - 1 - x][0 + interpolation] = (interpolation / (quarterSize)) * cube[TOP][size - 1 - x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, NEAR, BOTTOM
//            moy = (cube[NEAR][0 + interpolation][x] + cube[LEFT][size - 1 - interpolation][x])/2;
//            cube[LEFT][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[NEAR][0 + interpolation][x] = (interpolation / (quarterSize)) * cube[NEAR][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, NEAR, TOP
//            moy = (cube[NEAR][0 + interpolation][size - 1 - x] + cube[LEFT][size - 1 - interpolation][size - 1 - x])/2;
//            cube[LEFT][size - 1 - interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//            cube[NEAR][0 + interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[NEAR][0 + interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, FAR, TOP
//            moy = (cube[FAR][size - 1 - x][0 + interpolation] + cube[LEFT][0 + interpolation][size - 1 - x])/2;
//            cube[LEFT][0 + interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[LEFT][0 + interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//            cube[FAR][size - 1 - x][0 + interpolation] = (interpolation / (quarterSize)) * cube[FAR][size - 1 - x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, FAR, BOTTOM
//            moy = (cube[FAR][x][0 + interpolation] + cube[LEFT][0 + interpolation][x])/2;
//            cube[LEFT][0 + interpolation][x] = (interpolation / (quarterSize)) * cube[LEFT][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[FAR][x][0 + interpolation] = (interpolation / (quarterSize)) * cube[FAR][x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, NEAR, BOTTOM
//            moy = (cube[BOTTOM][0 + interpolation][x] + cube[LEFT][x][0 + interpolation])/2;
//            cube[LEFT][x][0 + interpolation] = (interpolation / (quarterSize)) * cube[LEFT][x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[BOTTOM][0 + interpolation][x] = (interpolation / (quarterSize)) * cube[BOTTOM][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, FAR, BOTTOM
//            moy = (cube[BOTTOM][0 + interpolation][size - 1 - x] + cube[LEFT][size - 1 - x][0 + interpolation])/2;
//            cube[LEFT][size - 1 - x][0 + interpolation] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[BOTTOM][0 + interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[BOTTOM][0 + interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, NEAR, BOTTOM
//            moy = (cube[BOTTOM][size - 1 - x][size - 1 - interpolation] + cube[NEAR][size - 1 - x][0 + interpolation])/2;
//            cube[NEAR][size - 1 - x][0 + interpolation] = (interpolation / (quarterSize)) * cube[NEAR][size - 1 - x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[BOTTOM][size - 1 - x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[BOTTOM][size - 1 - x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, NEAR, BOTTOM
//            moy = (cube[BOTTOM][x][size - 1 - interpolation] + cube[NEAR][x][0 + interpolation])/2;
//            cube[NEAR][x][0 + interpolation] = (interpolation / (quarterSize)) * cube[NEAR][x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[BOTTOM][x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[BOTTOM][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, FAR, BOTTOM
//            moy = (cube[BOTTOM][x][0 + interpolation] + cube[FAR][0 + interpolation][x])/2;
//            cube[FAR][0 + interpolation][x]= (interpolation / (quarterSize)) * cube[FAR][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[BOTTOM][x][0 + interpolation] = (interpolation / (quarterSize)) * cube[BOTTOM][x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, FAR, BOTTOM
//            moy = (cube[BOTTOM][size - 1 - x][0 + interpolation] + cube[FAR][0 + interpolation][size - 1 - x])/2;
//            cube[FAR][0 + interpolation][size - 1 - x]= (interpolation / (quarterSize)) * cube[FAR][0 + interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//            cube[BOTTOM][size - 1 - x][0 + interpolation] = (interpolation / (quarterSize)) * cube[BOTTOM][size - 1 - x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, FAR, TOP
//            moy = (cube[TOP][x][size - 1 - interpolation] + cube[RIGHT][size - 1 - interpolation][x])/2;
//            cube[RIGHT][size - 1 - interpolation][x]= (interpolation / (quarterSize)) * cube[RIGHT][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[TOP][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, NEAR, TOP
//            moy = (cube[TOP][size - 1 - x][size - 1 - interpolation] + cube[RIGHT][size - 1 - interpolation][size - 1 - x])/2;
//            cube[RIGHT][size - 1 - interpolation][size - 1 - x]= (interpolation / (quarterSize)) * cube[RIGHT][size - 1 - interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][size - 1 - x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[TOP][size - 1 - x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, NEAR, BOTTOM
//            moy = (cube[NEAR][size - 1 - interpolation][x] + cube[RIGHT][x][size - 1 - interpolation])/2;
//            cube[RIGHT][x][size - 1 - interpolation]= (interpolation / (quarterSize)) * cube[RIGHT][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[NEAR][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[NEAR][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, NEAR, TOP
//            moy = (cube[NEAR][size - 1 - interpolation][size - 1 - x] + cube[RIGHT][size - 1 - x][size - 1 - interpolation])/2;
//            cube[RIGHT][size - 1 - x][size - 1 - interpolation]= (interpolation / (quarterSize)) * cube[RIGHT][size - 1 - x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[NEAR][size - 1 - interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[NEAR][size - 1 - interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, NEAR, BOTTOM
//            moy = (cube[BOTTOM][size - 1 - interpolation][size - 1 - x] + cube[RIGHT][0 + interpolation][size - 1 - x])/2;
//            cube[RIGHT][0 + interpolation][size - 1 - x]= (interpolation / (quarterSize)) * cube[RIGHT][0 + interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//            cube[BOTTOM][size - 1 - interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[BOTTOM][size - 1 - interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, FAR, BOTTOM
//            moy = (cube[BOTTOM][size - 1 - interpolation][x] + cube[RIGHT][0 + interpolation][x])/2;
//            cube[RIGHT][0 + interpolation][x]= (interpolation / (quarterSize)) * cube[RIGHT][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[BOTTOM][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[BOTTOM][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, FAR, BOTTOM
//            moy = (cube[FAR][x][size - 1 - interpolation] + cube[RIGHT][x][0 + interpolation])/2;
//            cube[RIGHT][x][0 + interpolation]= (interpolation / (quarterSize)) * cube[RIGHT][x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[FAR][x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[FAR][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // RIGHT, FAR, TOP
//            moy = (cube[FAR][size - 1 - x][size - 1 - interpolation] + cube[RIGHT][size - 1 - x][0 + interpolation])/2;
//            cube[RIGHT][size - 1 - x][0 + interpolation]= (interpolation / (quarterSize)) * cube[RIGHT][size - 1 - x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[FAR][size - 1 - x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[FAR][size - 1 - x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);

//
//            // LEFT, NEAR
//            moy = (cube[NEAR][0 + interpolation][x] + cube[LEFT][size - 1 - interpolation][x])/2;
//            cube[LEFT][size - 1 - interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - interpolation][size - 1 - x] + moy * (1 - interpolation/quarterSize);
//            cube[NEAR][0 + interpolation][x] = (interpolation / (quarterSize)) * cube[NEAR][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);

            // NEAR, TOP
//            moy = ((cube[NEAR][x][size - 1 - interpolation] + cube[TOP][size - 1 - interpolation][x] + cube[LEFT][size - 1 - interpolation][size - 1 - interpolation])/3);
//            cube[NEAR][x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[NEAR][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[TOP][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//
//            if(interpolation <= x){
//                cube[LEFT][size - 1 - x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - x][size - 1 - interpolation] + cube[TOP][size - 1 - x][0 + interpolation]  * (1 - interpolation/quarterSize);
//                cube[LEFT][size - 1 - interpolation][size - 1 - x] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - interpolation][size - 1 - x] + cube[NEAR][0 + interpolation][size - 1 - x] * (1 - interpolation/quarterSize);
//            }
//
//            // LEFT, TOP
//            moy = (cube[TOP][x][0 + interpolation] + cube[LEFT][x][size - 1 - interpolation])/2;
//            cube[LEFT][x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[LEFT][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][x][0 + interpolation] = (interpolation / (quarterSize)) * cube[TOP][x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, NEAR
//            moy = (cube[NEAR][0 + interpolation][x] + cube[LEFT][size - 1 - interpolation][x])/2;
//            cube[LEFT][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[NEAR][0 + interpolation][x] = (interpolation / (quarterSize)) * cube[NEAR][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);


            // CODE ORIGINAL
//            // NEAR, TOP
//            moy = ((cube[NEAR][x][size - 1 - interpolation] + cube[TOP][size - 1 - interpolation][x])/2);
//            cube[NEAR][x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[NEAR][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[TOP][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//
//            // FAR, TOP
//            moy = ((cube[FAR][size - 1 - interpolation][x] + cube[TOP][0 + interpolation][x])/2);
//            cube[FAR][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[FAR][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][0 + interpolation][x] = (interpolation / (quarterSize)) * cube[TOP][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, TOP
//            moy = (cube[TOP][x][0 + interpolation] + cube[LEFT][x][size - 1 - interpolation])/2;
//            cube[LEFT][x][size - 1 - interpolation] = (interpolation / (quarterSize)) * cube[LEFT][x][size - 1 - interpolation] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][x][0 + interpolation] = (interpolation / (quarterSize)) * cube[TOP][x][0 + interpolation] + moy * (1 - interpolation/quarterSize);
//
//            // LEFT, NEAR
//            moy = (cube[NEAR][0 + interpolation][x] + cube[LEFT][size - 1 - interpolation][x])/2;
//            cube[LEFT][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[NEAR][0 + interpolation][x] = (interpolation / (quarterSize)) * cube[NEAR][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);



        }
//        cube[NEAR][0][x] = -1000;
//        cube[LEFT][size - 1][x] = 1000;
//        cube[TOP][size - 1][x] = cube[NEAR][x][size - 1] = cube[TOP][size - 1][x] + cube[NEAR][x][size - 1];
    }

//    for(int i = 1; i < quarterSize; i++){
//        for(int face = 0; face < FACE_COUNT; face++){
//            cube[face][i][i] = (cube[face][i + 1][i - 1] + cube[face][i - 1][i + 1])/2;
//            cube[face][size - 1 - i][i] = (cube[face][size - 2 - i][i - 1] + cube[face][size - i][i + 1])/2;
//            cube[face][i][size - 1 - i] = (cube[face][i + 1][size - i] + cube[face][i - 1][size - 2 - i])/2;
//            cube[face][size - 1 - i][size - 1 - i] = (cube[face][size - i][size - 2 - i] + cube[face][size - 2 - i][size - i])/2;
//        }
//    }
//
//    for(int i = 0; i < quarterSize; i++){
//        for(int face = 0; face < FACE_COUNT; face++){
//            cube[face][i][size/2] = (cube[face][i][size/2 + 1] + cube[face][i][size/2 - 1])/2;
//            cube[face][size - 1 - i][size/2] = (cube[face][size - 1 - i][size/2 + 1] + cube[face][size - 1 - i][size/2 - 1])/2;
//            cube[face][size/2][i] = (cube[face][size/2 + 1][i] + cube[face][size/2 - 1][i])/2;
//            cube[face][size/2][size - 1 - i] = (cube[face][size/2 + 1][size - 1 - i] + cube[face][size/2 - 1][size - 1 - i])/2;
//        }
//    }

//    for(int x = 0; x < size / 2; x++){
//
//        for(int interpolation = 0; interpolation < quarterSize; interpolation++){
            //
            // POINT
            //
//            for(int i = 0; i <= interpolation; i++){
//                moy = ((cube[LEFT][size - 1 - i][x] + cube[TOP][size - 1 - i][x])/2);
//                cube[LEFT][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[LEFT][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//
//            }
//            moy = ((cube[LEFT][size - 1 - interpolation][x] + cube[NEAR][0 + interpolation][x] + cube[TOP][size - 1 - interpolation][x])/3);
//            cube[NEAR][0 + interpolation][x] = (interpolation / (quarterSize)) * cube[NEAR][0 + interpolation][x] + moy * (1 - interpolation/quarterSize);
//            cube[TOP][size - 1 - interpolation][x] = (interpolation / (quarterSize)) * cube[TOP][size - 1 - interpolation][x] + moy * (1 - interpolation/quarterSize);
//        }
//    }

    return cube;
}

void spherifyPoint(float radius, glm::vec3& point, double amplitude){
    auto length = sqrt((point.x * point.x) + (point.y * point.y) + (point.z * point.z));
    point.x = point.x/length;
    point.y = point.y/length;
    point.z = point.z/length;

    point *= (radius + amplitude/65);
}

void generatePlanet( uint size, uint detail, VerticesArray & verticesArray, std::vector<uint> & indices){
    if ( detail < 2 )
        throw std::runtime_error( std::string( __func__ ) + "Need size > 3" );

    //
    verticesArray.clear();
    indices.clear();

    auto cube = diamantCarreCubeAlex(detail, 1);
    const uint square( detail * detail );
    const uint maxFace(FACE_COUNT);

    glm::vec3 center(0.f, 0.f, 0.f);
    float radius = 10;

    // Finding corner vertices
//    verticesArray.emplace_back(cube[NEAR][0][0]);
//    verticesArray.emplace_back(cube[NEAR][0][size - 1]);
//    verticesArray.emplace_back(cube[NEAR][size - 1][size - 1]);
//    verticesArray.emplace_back(cube[NEAR][size - 1][0]);
//    verticesArray.emplace_back(cube[FAR][0][0]);
//    verticesArray.emplace_back(cube[FAR][0][size - 1]);
//    verticesArray.emplace_back(cube[FAR][size - 1][size - 1]);
//    verticesArray.emplace_back(cube[FAR][size - 1][0]);
//
//    // Finding edges without corners
//    for(uint edgeVertex = 1; edgeVertex < size - 2; edgeVertex++){
//        verticesArray.emplace_back(cube[NEAR][edgeVertex][0]);
//        verticesArray.emplace_back(cube[NEAR][edgeVertex][size - 1]);
//        verticesArray.emplace_back(cube[NEAR][0][edgeVertex]);
//        verticesArray.emplace_back(cube[NEAR][size - 1][edgeVertex]);
//    }

    float halfSize = float(size)/2;
    double step = double(size)/(detail-1);

    for(int currentFace = 0; currentFace < maxFace; currentFace++){
        for ( uint i( 0 ); i < square; ++i ) {
            switch (currentFace) {
                case TOP :
                    verticesArray.emplace_back(
                            glm::vec3(((i % (detail)) * step) - halfSize,
                                      (0) + (center.y + halfSize),
                                      (int(i / (detail)) * step) - halfSize));
                    spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % detail][i / detail]);
                    break;
                case BOTTOM :
                    verticesArray.emplace_back(
                            glm::vec3(((i % (detail)) * step) - halfSize,
                                      -(0) + (center.y - halfSize),
                                      -((i / (detail)) * step) + halfSize));
                    spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % detail][i / detail]);
                    break;
                case LEFT :
                    verticesArray.emplace_back(
                            glm::vec3(-(0) + (center.x - halfSize),
                                      ((i % detail) * step) - halfSize, ((i / detail) * step) - halfSize));
                    spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % detail][i / detail]);
                    break;
                case RIGHT :
                    verticesArray.emplace_back(
                            glm::vec3((0) + (center.x + halfSize),
                                      (halfSize - (i % detail) * step), ((i / detail) * step) - halfSize));
                    spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % detail][i / detail]);
                    break;
                case NEAR :
                    verticesArray.emplace_back(
                            glm::vec3(((i % detail) * step) - halfSize, halfSize - ((i / detail) * step),
                                      (0) + (center.z + halfSize)));
                    spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % detail][i / detail]);
                    break;
                case FAR :
                    verticesArray.emplace_back(
                            glm::vec3(((i % detail) * step) - halfSize, ((i / detail) * step) - halfSize,
                                      -(0) + (center.z - halfSize)));
                    spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % detail][i / detail]);
                    break;
                default :
                    break;
            }
        }
    }

    for(int currentFace = 0; currentFace < maxFace; currentFace++){
        for(int j = 0; j < (size - 1); j++){
            for(int i = 0; i < size - 1; i++){
                indices.emplace_back(i + (j*(size)) + (currentFace * (size * (size))));
                indices.emplace_back(i + (j+1)*(size) + (currentFace * (size * (size))));
                indices.emplace_back(i+1 + (j+1)*(size) + (currentFace * (size * (size))));

                indices.emplace_back(i + j*(size) + (currentFace * (size * (size))));
                indices.emplace_back(i+1 + (j+1)*(size) + (currentFace * (size * (size))));
                indices.emplace_back(i+1 + (j*(size)) + (currentFace * (size * (size))));
            }
        }
    }
}

void DiamondSquare(std::vector<std::vector<float>>& terrain, unsigned x1, unsigned y1, unsigned x2, unsigned y2, float range, unsigned level) {
    if (level < 1) return;

    auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed1);
    std::uniform_real_distribution<float> dis(0, 1);

    // diamonds
    for (unsigned i = x1 + level; i < x2; i += level)
        for (unsigned j = y1 + level; j < y2; j += level) {
            float a = terrain[i - level][j - level];
            float b = terrain[i][j - level];
            float c = terrain[i - level][j];
            float d = terrain[i][j];
            float e = terrain[i - level / 2][j - level / 2] = (a + b + c + d) / 4 + dis(gen) * range;
        }

    // squares
    for (unsigned i = x1 + 2 * level; i < x2; i += level)
        for (unsigned j = y1 + 2 * level; j < y2; j += level) {
            float a = terrain[i - level][j - level];
            float b = terrain[i][j - level];
            float c = terrain[i - level][j];
            float d = terrain[i][j];
            float e = terrain[i - level / 2][j - level / 2];

            float f = terrain[i - level][j - level / 2] = (a + c + e + terrain[i - 3 * level / 2][j - level / 2]) / 4 + dis(gen) * range;
            float g = terrain[i - level / 2][j - level] = (a + b + e + terrain[i - level / 2][j - 3 * level / 2]) / 4 + dis(gen) * range;
        }

    DiamondSquare(terrain, x1, y1, x2, y2, range / 2, level / 2);
}

void generateTerrain( uint size, VerticesArray & verticesArray, std::vector<uint> & indices )
{
  //
  if ( size < 3 )
    throw std::runtime_error( std::string( __func__ ) + "Need size > 3" );

  //
  verticesArray.clear();
  indices.clear();

  //
  const uint square( size * size );
  const float half_size( float( size ) / 2.f );

    auto t = diamantCarre(size, size, 255);


  //
  for ( uint i( 0 ); i < square; ++i )
    verticesArray.emplace_back(
//      glm::vec3(
//        ( float( i % size ) / half_size ) - 0.5f,
//        (t[i/size][i%size]+size/2)/(size), // <- To test
//        ( float( i / size ) / half_size ) - 0.5f )
    glm::vec3(
            i%size,
//            (t[i/size][i%size]+size/2)/(size), // <- To test
            (t[i/size][i%size]), // <- To test
            ( i/size))
    );

  //
  const int size_i( static_cast<int>( size ) );
  for ( int j( 0 ); j < ( size_i - 1 ); ++j )
  {
    //
    const int line_0( j * size_i ), line_1( ( j + 1 ) * size_i );

    //
    if ( ( j & 1 ) == 0 )
    {
      for ( int i( 0 ); i < size_i; ++i )
      {
        indices.emplace_back( i + line_0 );
        indices.emplace_back( i + line_1 );
      }
    }
    else
    {
      for ( int i( size_i - 1 ); i > 0; --i )
      {
        indices.emplace_back( i + line_1 );
        indices.emplace_back( ( i - 1 ) + line_0 );
      }
    }
  }

  // add finale vertice if  reverse loop has been executed at the end
  if ( ( size & 1 ) == 1 )
    indices.emplace_back( ( size - 1 ) * size );

}

int main()
{
  // Initialise GLFW
  if ( !glfwInit() )
  {
    fprintf( stderr, "Failed to initialize GLFW\n" );
    return -1;
  }

  glfwWindowHint( GLFW_SAMPLES, 4 ); // 4x antialiasing
  glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 ); // On veut OpenGL 3.3
  glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
  glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE ); // Pour rendre MacOS heureux ; ne devrait pas être nécessaire
  glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE ); // On ne veut pas l'ancien OpenGL

  GLuint width = 1024;
  GLuint height = 768;

  // Ouvre une fenêtre et crée son contexte OpenGl
  GLFWwindow * window; // (Dans le code source qui accompagne, cette variable est globale)
  window = glfwCreateWindow( width, height, "Tutorial 03", NULL, NULL );
  if ( window == NULL )
  {
    fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent( window ); // Initialise GLEW
  glewExperimental = true; // Nécessaire dans le profil de base
  if ( glewInit() != GLEW_OK )
  {
    fprintf( stderr, "Failed to initialize GLEW\n" );
    return -1;
  }

  // Assure que l'on peut capturer la touche d'échappement enfoncée ci-dessous
  glfwSetInputMode( window, GLFW_STICKY_KEYS, GL_TRUE );
  glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );

  // Set the mouse at the center of the screen
  glfwPollEvents();

  // Enable depth test
  glEnable( GL_DEPTH_TEST );
  // Accept fragment if it closer to the camera than the former one
  glDepthFunc( GL_LESS );
  // Cull triangles which normal is not towards the camera
    glDisable( GL_CULL_FACE );
//    glEnable( GL_CULL_FACE );

  // IMGUI SETUP
  {
    static const char * glsl_version = "#version 330";

    // Setup Dear ImGui binding
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO & io = ImGui::GetIO();
    ( void )io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

    ImGui_ImplGlfw_InitForOpenGL( window, true );
    ImGui_ImplOpenGL3_Init( glsl_version );

    // Setup style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();
  }


  ///////////////////////////////////////////////////////
  GLuint VertexArrayID;
  glGenVertexArrays( 1, &VertexArrayID );
  glBindVertexArray( VertexArrayID );

  GLuint programID = LoadShaders( "Shader/SimpleVertexShader5-chris.glsl", "Shader/SimpleFragmentShader7.glsl" );

  // Get a handle for our "MVP" uniform
  GLuint MatrixID = glGetUniformLocation( programID, "MVP" );
  GLuint ViewMatrixID = glGetUniformLocation( programID, "V" );
  GLuint ModelMatrixID = glGetUniformLocation( programID, "M" );
  
    float color_decal = 0.f;
    GLuint uniform_color_decal = glGetUniformLocation( programID, "uniform_color_decal" );
    GLuint colorMaxHeight = glGetUniformLocation( programID, "colorMaxHeight" );
    GLuint objectColor = glGetUniformLocation( programID, "objectColor" );


    uint terrainSize = 65;

  // Read our .obj file
  std::vector<glm::vec3> vertices;
  std::vector<uint> indices;
//    generateTerrain( terrainSize, vertices, indices );
    generatePlanet( terrainSize, terrainSize, vertices, indices );

  GLuint vertexbuffer;
  glGenBuffers( 1, &vertexbuffer );
  GLuint elementbuffer;
  glGenBuffers( 1, &elementbuffer );

  const auto send_terrain( [&]()
  {

    glBindBuffer( GL_ARRAY_BUFFER, vertexbuffer );
    glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( glm::vec3 ), vertices.data(), GL_DYNAMIC_DRAW );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, elementbuffer );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof( unsigned int ), indices.data(), GL_DYNAMIC_DRAW );


  } );

  send_terrain();

  // Create and compile our GLSL program from the shaders

  ///////////////////////////////////////////////////////////////
  // For speed computation
  double lastTime = glfwGetTime();
  int nbFrames = 0;

  //
  bool gui_mode( true );
  bool gl_points_mode( false );


  // ImGui test
  bool show_demo_window = false;
  bool show_another_window = false;

  // Dark blue background
  ImVec4 clear_color = ImVec4( 0.45f, 0.55f, 0.60f, 1.00f );
  glClearColor( 0.0f, 0.0f, 0.4f, clear_color.w );

  do
  {

    // Measure speed
    double currentTime = glfwGetTime();
    nbFrames++;
    if ( currentTime - lastTime >= 1.0 )  // If last prinf() was more than 1sec ago
    {
      // printf and reset
      printf( "%f ms/frame\n", 1000.0 / double( nbFrames ) );
      nbFrames = 0;
      lastTime += 1.0;
    }

    // Clear the screen
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // Gui Mode ? (disable mouse)
    if ( glfwGetKey( window, GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS || glfwGetKey( window, GLFW_KEY_RIGHT_SHIFT ) == GLFW_PRESS )
    {
      glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
      if ( gui_mode ) glfwSetCursorPos( window, width / 2, height / 2 ); // prevent mouse force
      gui_mode = false;
    }
    else
    {
      glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
      gui_mode = true;
    }


    // IMGUI HERE
    {
      ImGui_ImplOpenGL3_NewFrame();
      ImGui_ImplGlfw_NewFrame();
      ImGui::NewFrame();

      // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
      if ( show_demo_window )
        ImGui::ShowDemoWindow( &show_demo_window );

      // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
      {
        static float f = 0.0f;
        static int counter = 0;

        ImGui::Begin( "Hello, world!" );                        // Create a window called "Hello, world!" and append into it.

        ImGui::Text( "This is some useful text." );             // Display some text (you can use a format strings too)
        ImGui::Checkbox( "Demo Window", &show_demo_window );    // Edit bools storing our window open/close state
        ImGui::Checkbox( "Another Window", &show_another_window );

//        if ( ImGui::SliderInt( "TerrainSize", reinterpret_cast<int *>( &terrainSize ), 3, 1024 ) )
//        {
//          if ( terrainSize >= 3 )
//          {
//            generateTerrain( terrainSize, vertices, indices );
//            send_terrain();
//          }
//        }

        ImGui::SliderFloat( "DecalColor", &color_decal, -10.f, 10.f );

        if ( ImGui::Button( "RegenTerrain" ) )
        {
            generatePlanet( terrainSize, terrainSize, vertices, indices );
//            generateTerrain( terrainSize, vertices, indices );
          send_terrain();
        }

        ImGui::Checkbox( "GL_POINTS ?", &gl_points_mode );


        if ( ImGui::ColorEdit3( "clear color", ( float * )&clear_color ) ) // Edit 3 floats representing a color
          glClearColor( clear_color.x, clear_color.y, clear_color.z, clear_color.w );

        if ( ImGui::Button( "Button" ) )                        // Buttons return true when clicked (most widgets return true when edited/activated)
          counter++;
        ImGui::SameLine();
        ImGui::Text( "counter = %d", counter );

        ImGui::Text( "Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate );
        ImGui::End();
      }

      // 3. Show another simple window.
      if ( show_another_window )
      {
        ImGui::Begin( "Another Window", &show_another_window ); // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
        ImGui::Text( "Hello from another window!" );
        if ( ImGui::Button( "Close Me" ) )
          show_another_window = false;
        ImGui::End();
      }

      ImGui::Render();
    }

    // Draw Model here :
    {
      // Use our shader
      glUseProgram( programID );

      // Compute the MVP matrix from keyboard and mouse input
      computeMatricesFromInputs( window, !gui_mode );
      glm::mat4 ProjectionMatrix = getProjectionMatrix();
      glm::mat4 ViewMatrix = getViewMatrix();
      glm::mat4 ModelMatrix = glm::mat4( 1.0 );
      glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

      // Send our transformation to the currently bound shader,
      // in the "MVP" uniform
      glUniformMatrix4fv( MatrixID, 1, GL_FALSE, &MVP[0][0] );
      glUniformMatrix4fv( ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0] );
      glUniformMatrix4fv( ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0] );

        glUniform1f( uniform_color_decal, color_decal );
        glUniform1i( colorMaxHeight, terrainSize );

//        glUniformMatrix3fv("objectColor", glm::vec3(1.0f, 0.5f, 0.31f));


        // 1rst attribute buffer : vertices
      glEnableVertexAttribArray( 0 );
      glBindBuffer( GL_ARRAY_BUFFER, vertexbuffer );
      glVertexAttribPointer(
        0,                  // attribute
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        nullptr             // array buffer offset
      );


      if ( !gl_points_mode ) // draw triangles
      {
        // Index buffer
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, elementbuffer );

        // Draw the triangles !
        glDrawElements(
          GL_TRIANGLES,          // mode
          GLsizei( indices.size() ),  // count
          GL_UNSIGNED_INT,            // type
          nullptr                     // element array buffer offset
        );
      }
      else // draw points
      {
        //
        glDrawArrays( GL_POINTS, 0, vertices.size() );
      }


      glDisableVertexAttribArray( 0 );
      //    glDisableVertexAttribArray( 1 );
      //    glDisableVertexAttribArray( 2 );
    }

    ImGui_ImplOpenGL3_RenderDrawData( ImGui::GetDrawData() );

    // Swap buffers
    glfwSwapBuffers( window );
    glfwPollEvents();

  } // Check if the ESC key was pressed or the window was closed
  while ( glfwGetKey( window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
          glfwWindowShouldClose( window ) == 0 );

  // Cleanup VBO and shader
  glDeleteBuffers( 1, &vertexbuffer );
  //  glDeleteBuffers( 1, &uvbuffer );
  //  glDeleteBuffers( 1, &colorBuffer );
  glDeleteBuffers( 1, &elementbuffer );
  glDeleteProgram( programID );
  glDeleteVertexArrays( 1, &VertexArrayID );

  // Cleanup IMGUI
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();

  // Close OpenGL window and terminate GLFW
  glfwTerminate();

  return 0;
}
