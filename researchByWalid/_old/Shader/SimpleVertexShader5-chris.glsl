#version 330 core

// Input vertex data, different for all executions of this shader.
layout( location = 0 ) in vec3 vertex_from_buffer;

// Uniforms :
uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;

// Output data :
out vec3 vertex;

flat out int coucou;

// Main
void main()
{
  vertex = vertex_from_buffer;

  // Output position of the vertex, in clip space : MVP * position
  gl_Position =  MVP * vec4( vertex_from_buffer, 1 );
  coucou = gl_VertexID;
}
