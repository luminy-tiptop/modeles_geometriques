#version 330 core

//
uniform float uniform_color_decal = 0.f;
uniform int colorMaxHeight;

// Interpolated values from the vertex shaders
in vec3 vertex;
flat in int coucou;

// Ouput data
out vec3 color;

//
#define Hex2Vex3(hex) vec3((float((hex/256/256)%256)/256.f),(float((hex/256)%256)/256.f),(float((hex)%256)/256.f))


// https://coolors.co/app/5ed6ff-32c5ff-07caed-2f9ec6-0083b7
// https://coolors.co/690611-daad2a-b4753f-422b17-110b06
// bf8156-58991d-54b534-4c4c4c-ffffff
const vec3 colors[10] = vec3[](
  // Water :
  Hex2Vex3(0x0083b7), Hex2Vex3(0x2f9ec6), Hex2Vex3(0x07caed), Hex2Vex3(0x32c5ff), Hex2Vex3(0x5ed6ff),
  // Terrain
  Hex2Vex3(0xbf8156), Hex2Vex3(0x58991d), Hex2Vex3(0x54b534), Hex2Vex3(0x4c4c4c), Hex2Vex3(0xffffff)
);
const float nb_colors = 10.f;



vec3 do_terrain_plot( float normal_height )
{
    float color_index = ((uniform_color_decal+normal_height - 10)/2.f) * nb_colors;
    if( color_index < 0.f ) color_index = 0.f;
    else if( (color_index) >= nb_colors-1.f ) color_index = nb_colors-1.f;

    float color_mix = (1.f-(ceil(color_index) - color_index));

    vec3 c1 = colors[int(floor(color_index))];
    vec3 c2 = colors[int(ceil(color_index))];
    return mix(c1,c2,color_mix);
}

float length(vec3 vertex){
    return sqrt((vertex.x * vertex.x) + (vertex.y * vertex.y) + (vertex.z * vertex.z));
}

void main()
{
    color = do_terrain_plot(length(vertex));

    if(coucou <= 0){
        color.r = 1.0f;
    }
}