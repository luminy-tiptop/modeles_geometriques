//
// Created by DoubleVV on 17/10/2018.
//

#ifndef OPENGLTUTO1_UTILITY_HPP
#define OPENGLTUTO1_UTILITY_HPP

#include <vector>

// Inclut GLEW. Toujours l'inclure avant gl.h et glfw.h, car c'est un peu magique.
#include <GL/glew.h>

// Inclut GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

GLuint LoadShaders( const char * vertex_file_path, const char * fragment_file_path );
GLuint loadBMP_custom( const char * imagepath );
void computeMatricesFromInputs( GLFWwindow * window, bool compute_mouse = true );
glm::mat4 getProjectionMatrix();
glm::mat4 getViewMatrix();
void indexVBO(
  std::vector<glm::vec3> & in_vertices,
  std::vector<glm::vec2> & in_uvs,
  std::vector<glm::vec3> & in_normals,

  std::vector<unsigned short> & out_indices,
  std::vector<glm::vec3> & out_vertices,
  std::vector<glm::vec2> & out_uvs,
  std::vector<glm::vec3> & out_normals
);


void indexVBO_TBN(
  std::vector<glm::vec3> & in_vertices,
  std::vector<glm::vec2> & in_uvs,
  std::vector<glm::vec3> & in_normals,
  std::vector<glm::vec3> & in_tangents,
  std::vector<glm::vec3> & in_bitangents,

  std::vector<unsigned short> & out_indices,
  std::vector<glm::vec3> & out_vertices,
  std::vector<glm::vec2> & out_uvs,
  std::vector<glm::vec3> & out_normals,
  std::vector<glm::vec3> & out_tangents,
  std::vector<glm::vec3> & out_bitangents
);

void indexVBO_slow(
  std::vector<glm::vec3> & in_vertices,
  std::vector<glm::vec2> & in_uvs,
  std::vector<glm::vec3> & in_normals,

  std::vector<unsigned short> & out_indices,
  std::vector<glm::vec3> & out_vertices,
  std::vector<glm::vec2> & out_uvs,
  std::vector<glm::vec3> & out_normals
);

GLuint loadDDS( const char * imagepath );

bool loadOBJ(
  const char * path,
  std::vector<glm::vec3> & out_vertices,
  std::vector<glm::vec2> & out_uvs,
  std::vector<glm::vec3> & out_normals
);

#endif //OPENGLTUTO1_UTILITY_HPP
