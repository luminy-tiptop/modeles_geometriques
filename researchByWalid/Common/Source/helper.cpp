//
// Created by DoubleVV on 18/12/2018.
//

#include "helper.hpp"

double distance2Points(glm::vec3 p1, glm::vec3 p2){
    return sqrt(((p1.x - p2.x) * (p1.x - p2.x)) + ((p1.y - p2.y) * (p1.y - p2.y)) + ((p1.z - p2.z) * (p1.z - p2.z)));
}

bool isInEllipse(float coord1, float coord2, float coord3, float axis1, float axis2, float axis3){
    return (((coord1 * coord1)/ (axis1 * axis1)) + (((coord2 * coord2)/ (axis2 * axis2))) + (((coord3 * coord3)/ (axis3 * axis3)))) <= 1;
}

int solvePoly2(double a, double b, double c, double &x1, double &x2)
{
    double delta = b * b - 4 * a * c;

    if (delta < 0) return 0;

    double inv_2a = 0.5 / a;

    if (delta == 0) {
        x1 = -b * inv_2a;
        x2 = x1;
        return 1;
    }

    double sqrt_delta = sqrt(delta);
    x1 = (-b + sqrt_delta) * inv_2a;
    x2 = (-b - sqrt_delta) * inv_2a;
    return 2;
}

int solvePoly3(double a, double b, double c, double d,
               double &x0, double &x1, double &x2)
{
    if (a == 0) {
        if (b == 0)	{
            if (c == 0)
                return 0;

            x0 = -d / c;
            return 1;
        }

        x2 = 0;
        return solvePoly2(b, c, d, x0, x1);
    }

    double inv_a = 1. / a;
    double b_a = inv_a * b, b_a2 = b_a * b_a;
    double c_a = inv_a * c;
    double d_a = inv_a * d;

    double Q = (3 * c_a - b_a2) / 9;
    double R = (9 * b_a * c_a - 27 * d_a - 2 * b_a * b_a2) / 54;
    double Q3 = Q * Q * Q;
    double D = Q3 + R * R;
    double b_a_3 = (1. / 3.) * b_a;

    if (Q == 0) {
        if(R == 0) {
            x0 = x1 = x2 = - b_a_3;
            return 3;
        }
        else {
            x0 = pow(2 * R, 1 / 3.0) - b_a_3;
            return 1;
        }
    }

    if (D <= 0) {
        double theta = acos(R / sqrt(-Q3));
        double sqrt_Q = sqrt(-Q);
        x0 = 2 * sqrt_Q * cos(theta             / 3.0) - b_a_3;
        x1 = 2 * sqrt_Q * cos((theta + 2 * M_PI)/ 3.0) - b_a_3;
        x2 = 2 * sqrt_Q * cos((theta + 4 * M_PI)/ 3.0) - b_a_3;

        return 3;
    }

    double AD = pow(fabs(R) + sqrt(D), 1.0 / 3.0) * (R > 0 ? 1 : (R < 0 ? -1 : 0));
    double BD = (AD == 0) ? 0 : -Q / AD;

    x0 = AD + BD - b_a_3;

    return 1;
}

float Remap(float x, float t1, float t2, float s1, float s2)
{
    // "Yellow" is a "normalized" value between 0 and 1
    float yellow = (x - t1)/(t2 - t1);

    // "Green" is the result!
    float green = yellow*(s2 - s1) + s1;

    return green;
}

float RemapClamp(float x, float t1, float t2, float s1, float s2)
{
    if (x < t1)
        return s1;
    if (x > t2)
        return s2;

    return Remap(x, t1, t2, s1, s2);
}
