//
// Created by DoubleVV on 16/12/2018.
//

#define GLM_ENABLE_EXPERIMENTAL

#include <random>

#include "RandomPointSphere.hpp"

#include "glm/gtx/string_cast.hpp"

#include <helper.hpp>
#include <iostream>

typedef glm::mat3 mat3;

RandomPointSphere::RandomPointSphere(float rayon, RandomPointSphere::Point center, int pointCount, float minimalDistance)
        : mRayonOx(rayon),
          mCenter(center),
          mListPoint(),
          mBarytcentre(0) {
    generateRandomPointsSphere(pointCount, minimalDistance);
}

RandomPointSphere::RandomPointSphere(float rayonOx, float rayonOy, float rayonOz, RandomPointSphere::Point center, float alpha, float beta, float gamma,
                                     int pointCount, float minimalDistance)
        : mRayonOx(rayonOx),
          mRayonOy(rayonOy),
          mRayonOz(rayonOz),
          mCenter(center),
          mAlpha(alpha),
          mBeta(beta),
          mGamma(gamma),
          mBarytcentre(0),
          mListPoint() {
    generateRandomPointsEllipse(pointCount, minimalDistance);
    rotate(0);
//    translate(Vector3f(1.0f, 0.2f, -0.5f));
    calculListPointBarycentre();
    calculMatInertia();
    diagonalizeMatInertia();
//    rotateFromQuaternion(Quaternion(Vector3f(1.f, 0.8f, 0.1f), 180));

    std::cout << glm::to_string(mMatInertia) <<  std::endl;
    std::cout << glm::to_string(mBarytcentre) <<  std::endl;

}



void RandomPointSphere::generateRandomPointsSphere(int pointCount, float minimalDistance) {
    mListPoint.clear();

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-mRayonOx, mRayonOx);

    bool isInMinimalDistance = true;

    for(int i = 0; i < pointCount; i++){
        Point p(dis(gen), dis(gen), dis(gen));

        if(distance2Points(p, mCenter) < mRayonOx){
            isInMinimalDistance = true;

            for(auto pointInSphere : mListPoint){
                if(distance2Points(p, pointInSphere) < minimalDistance){
                    isInMinimalDistance = false;
                    break;
                }
            }

            if(isInMinimalDistance){
                mListPoint.emplace_back(p);
            }
        }
    }

    calculBarycentre();
}

void RandomPointSphere::generateRandomPointsEllipse(int pointCount, float minimalDistance) {
    mListPoint.clear();
    mListPointTransformed.clear();

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> disOx(-mRayonOx, mRayonOx);
    std::uniform_real_distribution<> disOy(-mRayonOy, mRayonOy);
    std::uniform_real_distribution<> disOz(-mRayonOz, mRayonOz);

    bool isInMinimalDistance = true;

    for(int i = 0; i < pointCount; i++){
        Point p(disOx(gen), disOy(gen), disOz(gen));

        if(!isInEllipse(p.x, p.y, p.z, mRayonOx, mRayonOy, mRayonOz)){
            continue;
        }

        isInMinimalDistance = true;

        for(auto pointInEllipse : mListPoint){
            if(distance2Points(p, pointInEllipse) < minimalDistance){
                isInMinimalDistance = false;
                break;
            }
        }

        if(isInMinimalDistance){
            mListPoint.emplace_back(p);
            mListPointTransformed.emplace_back(p);
        }
    }

    calculBarycentre();
}

void RandomPointSphere::calculBarycentre() {
    mBarytcentre = Point(0.f);

    for(auto p : mListPoint){
        mBarytcentre += p;
    }

    mBarytcentre /= mListPoint.size();

//    mListPoint.push_back(mBarytcentre);
}

void RandomPointSphere::calculListPointBarycentre() {
    mListPointBarycentre.clear();

    for(auto p : mListPoint){
        mListPointBarycentre.emplace_back(p - mBarytcentre);
    }
}

void RandomPointSphere::calculMatInertia() {
    double xSquared = 0;
    double ySquared = 0;
    double zSquared = 0;
    double xy = 0;
    double xz = 0;
    double yz = 0;


    for(auto p : mListPointBarycentre){
        xSquared += p.x*p.x;
        ySquared += p.y*p.y;
        zSquared += p.z*p.z;
        xy += p.x*p.y;
        yz += p.y*p.z;
        xz += p.x*p.z;
    }
    mMatInertia = mat3(xSquared, xy, xz, xy, ySquared, yz, xz, yz, zSquared);
}

void RandomPointSphere::diagonalizeMatInertia() {

    mMatInertia[0][0] = 4;
    mMatInertia[0][1] = 6;
    mMatInertia[0][2] = 10;
    mMatInertia[1][0] = 3;
    mMatInertia[1][1] = 10;
    mMatInertia[1][2] = 13;
    mMatInertia[2][0] = -2;
    mMatInertia[2][1] = -6;
    mMatInertia[2][2] = -8;

    double a = -1;
    double b = mMatInertia[0][0] + mMatInertia[1][1] + mMatInertia[2][2];
    double c = -(mMatInertia[0][0] * mMatInertia[1][1]) - (mMatInertia[0][0] * mMatInertia[2][2]) - (mMatInertia[1][1] * mMatInertia[2][2]) + (mMatInertia[1][2] * mMatInertia[2][1]) + (mMatInertia[0][1] * mMatInertia[1][0]) + (mMatInertia[0][2] * mMatInertia[2][0]);
    double d = (mMatInertia[0][0] * mMatInertia[1][1] * mMatInertia[2][2]) - (mMatInertia[0][0] * mMatInertia[1][2] * mMatInertia[2][1]) - (mMatInertia[0][1] * mMatInertia[1][0] * mMatInertia[2][2]) + (mMatInertia[1][0] * mMatInertia[0][2] * mMatInertia[2][1]) + (mMatInertia[2][0] * mMatInertia[0][1] * mMatInertia[1][2]) - (mMatInertia[2][0] * mMatInertia[0][2] * mMatInertia[1][1]);

    double x0, x1, x2;

    int nbRoot = solvePoly3(a, b, c, d, x0, x1, x2);

    glm::vec3 eigenValues(x0, x1, x2);

    std::cout << glm::to_string(eigenValues) <<  std::endl;
}

void RandomPointSphere::rotateFromQuaternion(Quaternion q) {

    Vector3f ax;
    float angle;
    q.ToAxisAngle(ax, angle);

    mAlpha = ax.x;
    mBeta = ax.y;
    mGamma = ax.z;

    rotate(angle);
}

void RandomPointSphere::translate(Vector3f t) {
    glm::mat4 translationMatrix(1.f, 0.f, 0.f, t.x, 0.f, 1.f, 0.f, t.y, 0.f, 0.f, 1.f, t.z, 0.f, 0.f, 0.f, 1.f);
    for(auto& p : mListPoint){
        p.x = translationMatrix[0][0] * p.x + translationMatrix[0][1] * p.y + translationMatrix[0][2] * p.z + translationMatrix[0][3];
        p.y = translationMatrix[1][0] * p.x + translationMatrix[1][1] * p.y + translationMatrix[1][2] * p.z + translationMatrix[1][3];
        p.z = translationMatrix[2][0] * p.x + translationMatrix[2][1] * p.y + translationMatrix[2][2] * p.z + translationMatrix[2][3];
    }
}

void RandomPointSphere::rotate(float angle) {

    glm::vec3 n(mAlpha, mBeta, mGamma);

    if(n.x + n.y + n.z == 0)
        return;

    n = glm::normalize(n);

//    glm::mat4 rotationMatrix(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, , , , 1);
    mat3 rotationMatrix(cos(mGamma) * cos(mAlpha) - sin(mGamma) * cos(mBeta) * sin(mAlpha),
                             -cos(mGamma) * sin(mAlpha) - sin(mGamma) * cos(mBeta) * cos(mAlpha),
                             sin(mGamma) * sin(mBeta),
                             sin(mGamma) * cos(mAlpha) + cos(mGamma) * cos(mBeta) * sin(mAlpha),
                             -sin(mGamma) * sin(mAlpha) + cos(mGamma) * cos(mBeta) * cos(mAlpha),
                             -cos(mGamma) * sin(mBeta),
                             sin(mBeta) * sin(mAlpha),
                             sin(mBeta) * cos(mAlpha),
                             cos(mBeta));

//    mat3 rotationMatrixAlpha(1, 0, 0,
//                                  0, cos(mAlpha), -sin(mAlpha),
//                                  0, sin(mAlpha), cos(mAlpha));
//
//    mat3 rotationMatrixBeta(cos(mBeta), 0, sin(mBeta),
//                                 0, 1, 0,
//                                 -sin(mBeta), 0, cos(mBeta));
//
//    mat3 rotationMatrixGamma(cos(mGamma), -sin(mGamma), 0,
//                                 sin(mGamma), cos(mGamma), 0,
//                                 0, 0, 1);

//    mat3 r = rotationMatrixAlpha * rotationMatrixBeta * rotationMatrixGamma;
    mListPointTransformed.clear();

    for(auto& p : mListPoint){
//        mListPointTransformed.emplace_back(p*cos(angle) + (glm::dot(p, n) * n * (1-cos(angle))) + (glm::cross(n, p) * sin(angle)));


        Point pr(p);

        pr.x = rotationMatrix[0][0] * p.x + rotationMatrix[0][1] * p.y + rotationMatrix[0][2] * p.z;
        pr.y = rotationMatrix[1][0] * p.x + rotationMatrix[1][1] * p.y + rotationMatrix[1][2] * p.z;
        pr.z = rotationMatrix[2][0] * p.x + rotationMatrix[2][1] * p.y + rotationMatrix[2][2] * p.z;

//        pr.x = r[0][0] * p.x + r[0][1] * p.y + r[0][2] * p.z;
//        pr.y = r[1][0] * p.x + r[1][1] * p.y + r[1][2] * p.z;
//        pr.z = r[2][0] * p.x + r[2][1] * p.y + r[2][2] * p.z;

        mListPointTransformed.emplace_back(pr);


//        pr.x = rotationMatrixAlpha[0][0] * p.x + rotationMatrixAlpha[0][1] * p.y + rotationMatrixAlpha[0][2] * p.z;
//        pr.x = rotationMatrixBeta[0][0] * p.x + rotationMatrixBeta[0][1] * p.y + rotationMatrixBeta[0][2] * p.z;
//        pr.x = rotationMatrixGamma[0][0] * p.x + rotationMatrixGamma[0][1] * p.y + rotationMatrixGamma[0][2] * p.z;
//
//        pr.y = rotationMatrixAlpha[1][0] * p.x + rotationMatrixAlpha[1][1] * p.y + rotationMatrixAlpha[1][2] * p.z;
//        pr.y = rotationMatrixBeta[1][0] * p.x + rotationMatrixBeta[1][1] * p.y + rotationMatrixBeta[1][2] * p.z;
//        pr.y = rotationMatrixGamma[1][0] * p.x + rotationMatrixGamma[1][1] * p.y + rotationMatrixGamma[1][2] * p.z;
//
//        pr.z = rotationMatrixAlpha[2][0] * p.x + rotationMatrixAlpha[2][1] * p.y + rotationMatrixAlpha[2][2] * p.z;
//        pr.z = rotationMatrixBeta[2][0] * p.x + rotationMatrixBeta[2][1] * p.y + rotationMatrixBeta[2][2] * p.z;
//        pr.z = rotationMatrixGamma[2][0] * p.x + rotationMatrixGamma[2][1] * p.y + rotationMatrixGamma[2][2] * p.z;

//        p = pr;
    }
}

void RandomPointSphere::setRayon(float rayon) {
    mRayonOx = rayon;
}

void RandomPointSphere::setCenter(const RandomPointSphere::Point &center) {
    mCenter = center;
}

float RandomPointSphere::getRayon() const {
    return mRayonOx;
}

const RandomPointSphere::Point &RandomPointSphere::getCenter() const {
    return mCenter;
}

const std::vector<RandomPointSphere::Point> &RandomPointSphere::getListPoint() const {
    return mListPoint;
}

const RandomPointSphere::Point &RandomPointSphere::getBarytcentre() const {
    return mBarytcentre;
}

float RandomPointSphere::getAlpha() const {
    return mAlpha;
}

float RandomPointSphere::getBeta() const {
    return mBeta;
}

float RandomPointSphere::getGamma() const {
    return mGamma;
}

void RandomPointSphere::setAlpha(float mAlpha) {
//    RandomPointSphere::mAlpha = mAlpha/360 * (float)M_PI * 2;
    RandomPointSphere::mAlpha = mAlpha;
}

void RandomPointSphere::setBeta(float mBeta) {
//    RandomPointSphere::mBeta = mBeta/360 * (float)M_PI * 2;
    RandomPointSphere::mBeta = mBeta;
}

void RandomPointSphere::setGamma(float mGamma) {
//    RandomPointSphere::mGamma = mGamma/360 * (float)M_PI * 2;
    RandomPointSphere::mGamma = mGamma;
}

const vector<RandomPointSphere::Point> &RandomPointSphere::getListPointTransformed() const {
    return mListPointTransformed;
}

void RandomPointSphere::setRayonOx(float mRayonOx) {
    RandomPointSphere::mRayonOx = mRayonOx;
}

void RandomPointSphere::setRayonOy(float mRayonOy) {
    RandomPointSphere::mRayonOy = mRayonOy;
}

void RandomPointSphere::setRayonOz(float mRayonOz) {
    RandomPointSphere::mRayonOz = mRayonOz;
}



