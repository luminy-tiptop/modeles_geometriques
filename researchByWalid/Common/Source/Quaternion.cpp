//
// Created by DoubleVV on 18/12/2018.
//

#include <cmath>
#include <cassert>

#include "../Include/Quaternion.hpp"

Quaternion::Quaternion(const Vector3f &n, float a) {
    a = a/360 * (float)M_PI * 2;

    w = cos(a/2);

    v.x = n.x*sin(a/2);
    v.y = n.y*sin(a/2);
    v.z = n.z*sin(a/2);
}

Quaternion::Quaternion() {
    v = Vector3f();
    w = cos(0);
}

const Quaternion Quaternion::Inverted() const {
    Quaternion q;

    q.w = w;
    q.v = v * -1.f;

    return q;
}

const Quaternion Quaternion::operator*(const Quaternion &q) const {
    Quaternion r;

    r.w = w*q.w - (v%q.v);
    r.v = v*q.w + q.v*w + (v ^ q.v);

    return r;
}

const Vector3f Quaternion::operator*(const Vector3f &V) const {
    Quaternion p;
    p.w = 0;
    p.v = V;

    const Quaternion& q = *this;
    return (q * p * q.Inverted()).v;
}

const Quaternion Quaternion::operator^(float t) const
{
    float a;
    Vector3f n;

    ToAxisAngle(n, a);

    float at = a*t;

    return Quaternion(n, at);
}

const Quaternion Quaternion::Slerp(const Quaternion& other, float t) const
{
    const Quaternion& q = *this;
    Quaternion r = other;

    return ((r * q.Inverted()) ^ t) * q;
}

void Quaternion::ToAxisAngle(Vector3f &vecAxis, float &flAngle) const{
    if (v.length() < 0.0001f)
        vecAxis = Vector3f(1, 0, 0);
    else
        vecAxis = v;

    vecAxis.normalize();

    assert(fabs(vecAxis.length() - 1) < 0.000001f);

    flAngle = acos(w)*2;

    flAngle *= 360 / ((float)M_PI * 2);
}


