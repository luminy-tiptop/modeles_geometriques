//
// Created by DoubleVV on 18/12/2018.
//

#ifndef OPENGLTUTO1_QUATERNION_HPP
#define OPENGLTUTO1_QUATERNION_HPP

#include "vector3f.h"

class Quaternion {

public:
    explicit Quaternion();

    explicit Quaternion(const Vector3f& n, float a);

    const Quaternion Inverted() const;
    const Quaternion Slerp(const Quaternion& r, float t) const;

    const Quaternion operator*(const Quaternion& q) const;
    const Vector3f operator*(const Vector3f& V) const;
    const Quaternion operator^(float t) const;

    void ToAxisAngle(Vector3f& vecAxis, float& flAngle) const;

public:
    float w;

    Vector3f v;
};


#endif //OPENGLTUTO1_QUATERNION_HPP
