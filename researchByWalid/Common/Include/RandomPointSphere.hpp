//
// Created by DoubleVV on 16/12/2018.
//

#ifndef OPENGLTUTO1_RANDOMPOINTSPHERE_HPP
#define OPENGLTUTO1_RANDOMPOINTSPHERE_HPP

#include <vector>
#include <glm/glm.hpp>
#include <Quaternion.hpp>


class RandomPointSphere {

public:
    typedef glm::vec3 Point;

    RandomPointSphere(float rayon, Point center, int pointCount, float minimalDistance);
    RandomPointSphere(float rayonOx, float rayonOy, float rayonOz, Point center, float alpha, float beta, float gamma, int pointCount, float minimalDistance);

    void generateRandomPointsSphere(int pointCount, float minimalDistance);
    void generateRandomPointsEllipse(int pointCount, float minimalDistance);

    void translate(Vector3f t);
    void rotate(float angle);

    void rotateFromQuaternion(Quaternion q);


public:
    float getRayon() const;
    const Point &getCenter() const;
    const std::vector<Point> &getListPoint() const;

    void setRayon(float mRayon);
    void setCenter(const Point &mCenter);

private:
    void calculBarycentre();
    void calculListPointBarycentre();
    void calculMatInertia();
    void diagonalizeMatInertia();

private:
    float mRayonOx;
public:
    void setRayonOx(float mRayonOx);

    void setRayonOy(float mRayonOy);

    void setRayonOz(float mRayonOz);

private:
    float mRayonOy;
    float mRayonOz;
    Point mCenter;
    float mAlpha;
public:
    void setAlpha(float mAlpha);
    void setBeta(float mBeta);
    void setGamma(float mGamma);

public:
    float getAlpha() const;
    float getBeta() const;
    float getGamma() const;

private:
    float mBeta;
    float mGamma;
    Point mBarytcentre;
    glm::dmat3 mMatInertia;
    glm::dmat3 mEigenVector;

public:
    const Point &getBarytcentre() const;

private:

    std::vector<Point> mListPoint;
    std::vector<Point> mListPointTransformed;
public:
    const vector<Point> &getListPointTransformed() const;

private:
    std::vector<Point> mListPointBarycentre;
};


#endif //OPENGLTUTO1_RANDOMPOINTSPHERE_HPP
