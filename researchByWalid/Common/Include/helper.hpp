//
// Created by DoubleVV on 16/12/2018.
//

#ifndef OPENGLTUTO1_HELPER_HPP
#define OPENGLTUTO1_HELPER_HPP


// Macros
#define SQR(x)      ((x)*(x))                        // x^2

#include <glm/glm.hpp>

#include <glm/vec3.hpp>
#include <glm/mat3x3.hpp>
#include <cmath>
#include <iostream>
#include <ostream>

double distance2Points(glm::vec3 p1, glm::vec3 p2);
bool isInEllipse(float coord1, float coord2, float coord3, float axis1, float axis2, float axis3);
int solvePoly2(double a, double b, double c, double &x1, double &x2);
int solvePoly3(double a, double b, double c, double d, double &x0, double &x1, double &x2);
float Remap(float x, float t1, float t2, float s1, float s2);
float RemapClamp(float x, float t1, float t2, float s1, float s2);
// Returns a triangle wave on the interval [0, 1]
inline float TriangleWave(float flTime, float flLength)
{
    // flTime is a value in the interval [0, infinity].

    // flMod is in the interval [0, flLength]
    float flMod = (float)fmod(flTime, flLength);

    // flRemapped is in the interval [-1, 1]
    float flRemapped = Remap(flMod, 0, flLength, -1, 1);

    // The negative values are flipped to positive, so that you have a triangle wave on [0, 1]: /\/\/\/\/
    return fabs(flRemapped);
}

#endif //OPENGLTUTO1_HELPER_HPP
